/* Copyright 2012 Institut Pluridisciplinaire Hubert Curien
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

   Programmer : 	Christian Bonnin
   Version : 		1.0
   Date of creation:01/08/2012
   Support : 		mail to : christian.bonnin@iphc.cnrs.fr
   FileName : 		GlibStreamer.h
   Content : 		GlibStreamer module
   Used in : 		CBCDAQ
   Support : 		mail to : christian.bonnin@iphc.cnrs.fr
*/

#ifndef _GlibStreamer_h_
#define _GlibStreamer_h_


/*
#include <sys/time.h>
*/

#include "xdaq/Application.h"
#include "xdaq/ApplicationGroup.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationStub.h"
#include "xdaq/exception/Exception.h"
#include "xdaq/NamespaceURI.h"
#include "xdaq/WebApplication.h"


#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPElement.h"
#include "xoap/Method.h"
#include "xoap/domutils.h"


#include "xgi/Utils.h"
#include "xgi/Method.h"
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"

#include "toolbox/fsm/FiniteStateMachine.h"
#include "FsmWithStateName.h"
#include "xdata/String.h"
#include "xdata/UnsignedLong.h"
#include "xdata/Integer.h"
#include "xdata/Vector.h"
#include "xdata/Boolean.h"

#include "FRLA.h"

//#include "GlibStreamerV.h"
#include "Acquisition.h"

namespace Ph2_HwInterface{ class BeBoardInterface;}
namespace Ph2_HwDescription{ class BeBoard;}

namespace BoardSupervisor{

class Supervisor;
///Streamer main class. Retrieve acquisition data from GLIB.
class Streamer
{

public:

        /* define factory method for instantion of this application */
        //XDAQ_INSTANTIATOR();

        Streamer(Supervisor *pApp);
	~Streamer();
	void actionPerformed (xdata::Event& e);

        /* Actions disponibles depuis la page web */
        ///Default Hyperdaq method. Show parameter set values in an HTML table
        void Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
        ///Automatic web page refresh during acquisition
		void AutoRefresh(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
		///Parameter values validation
		void ValidParam(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
		void FsmDiagram(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
		///Send a spurious frame to the board (by writing a flag)
		void SendSpuriousFrame(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
		///Send a Trigger One Shot to the board (by writing a flag)
		void SendTriggerOneShot(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
		/// Read status flags from board
		void ReadFlags(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
		/// Enter manually simulated data
		void EnterData(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
		void EnterConditionData(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
		/// Validation of manually entered simulated data
		void ValidSimulationData(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
		/// Validation of selected condition data
		void ValidConditionData(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
		/// Force start by setting force_BG0 signal
		void ForceStartBg0(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
		void PauseAcquisition(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
		/// Load HTML parameter values from a file
		void LoadFromFileHtml(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
		/// Save HTML parameter values into a file
		void SaveToFileHtml(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
		/// Load HTML Condition data from a file
		void LoadFromFileHtmlCondition(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
		/// Save HTML Condition Data into a file
		void SaveToFileHtmlCondition(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
		void EnterCommissioningLoop(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
		void ValidCommissioningLoop(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
		void LoadFromFileCommissioningLoop(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
  		void SaveToFileCommissioningLoop(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
  		void AddCommissioningParameter(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
  		void RemoveLastCommissioningParameter(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
  		void ToggleDataVisuSize(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
  		void ResultCommissioningLoop(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	///XOAP callback for start transition
	xoap::MessageReference start (xoap::MessageReference msg) throw (xoap::exception::Exception);
	///XOAP callback for stop transition
	xoap::MessageReference stop (xoap::MessageReference msg) throw (xoap::exception::Exception);
	///XOAP halt transition
	xoap::MessageReference halt (xoap::MessageReference msg) throw (xoap::exception::Exception);
	///XOAP callback for reset transition
	xoap::MessageReference reset (xoap::MessageReference msg) throw (xoap::exception::Exception);
	///XOAP callback for configure transition
	xoap::MessageReference configure (xoap::MessageReference msg) throw (xoap::exception::Exception);

	///XOAP force start by python script (LGADDED)
	xoap::MessageReference forceStart (xoap::MessageReference msg) throw (xoap::exception::Exception);


	///Action called by FSM when entering Running state
	void startAction()	throw (toolbox::fsm::exception::Exception);
	///Action called by FSM when entering Halted state
	void stopAction()	throw (toolbox::fsm::exception::Exception);
	void destroyAction()	throw (toolbox::fsm::exception::Exception);
	void initAction()	throw (toolbox::fsm::exception::Exception);
	///Method called by FSM when entering Failed state
	void failedTransition (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception);
	/** Set values into the condition data parameter set used to fill HTML form and text properties file from XDAQ SOAP variables*/
	void setConditionValuesFromVector();
	/** Set values into vector of XDAQ SOAP variables from the condition data parameter set */
	void setConditionVectorFromValues();

	xdata::String fileParamHtml, fileConditionHtml;
  	xdata::String strRuName, strDataFile, strDestFile, strNewDaqFile;//, str2ndDestFile;
	xdata::Integer intRuInstance, intShortPause, intLongPause, intAcfAcquisition, intDataVisu, intNbAcq, intAcqMode;//intDestMaxSize, int2ndDestRatio, 
	xdata::Boolean bHardwareCounter, bSharedMemory, bMemToFile, bBreakTrigger, bDisplayLog, bDisplayFlags, bDataFlags, bDisplayCounters, bZeroSuppressed, bConditionData, bCommissionningLoop, bReadNextFiles;
	xdata::Vector<xdata::UnsignedInteger> vecConditionKey, vecConditionValue;
private:
	FRLA frla_; 
	// Work loop//	toolbox::task::ActionSignature * acqJob_;
	
		
	Acquisition* pAcq;
	Board* pMainBoard;
	Supervisor *pWebApp;
	
	///GLIB uHal object
	//uhal::HwInterface *lBoard;
	///Web page auto refresh
	bool bRefresh, bBigSizeVisu;
	///Reset the Finite State Machine
	//void resetAction() throw (xcept::Exception);
	///Write Hyperdaq Html header into web page
	//void createHtmlHeader(xgi::Output *out);
	///Write acquisition status description into web page
	void insertDescription(xgi::Output *out);
	/// HTML description of status flags 
	std::string statusFlagToHtml(uint32_t uVal, const char *small);
	/// HTML table description of data flags
	std::string dataFlagToHtml(uint32_t uFlags);
	/// Acquisition data visualisation in an HTML 5 canvas
	std::string dataVisuToHtml(uint32_t *puData, uint32_t uNum, uint32_t nbEvt, const std::string& strLabel);
	/// HTML tooltip for visualisation canvas histogram
	std::string dataVisuTooltip();
	///Get binary transcription of a part of a 32 bits value
	std::string getBits(uint32_t uVal, short begin, short end);
	///Meaning of sTTC code value
	const char* sTTScodeMeaning(uint32_t uVal);
};
}
#endif


