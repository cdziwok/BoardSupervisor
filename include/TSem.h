#ifndef TSem_INCLUDE
#define TSem_INCLUDE
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/file.h>
#include <sys/sem.h>
#include <unistd.h>
#include <string>
/**
 \enum TSemType
 */
enum TSemType { 
  TSem_CREATE, ///< Create the Share Memory
  TSem_OPEN    ///< Open the Share Memory 
};

/**
   \class TSem
   \author L.Mirabito
   \date March 2001
   \version 1.0
   \brief <b> Essential </b>. It is the main communication class

   <h2> Usage </h2>
   The TSem class implements both a Share memory definition and access and
   a semaphore to access it.
   <p> The memory is create by the following code:
   <pre>
   TSem *ms = new TSem("MyShare",TSem_CREATE,1000);
   </pre>
   <p> It then can be access and locked:
   <pre>
    // Mapp the meory to a given class
    MyClass* mc = (MyClass*) ms->Attach();
    // Lock the access with a semaphore
    ms->Lock();
    // access data (fantaisy)
    My_Histo_Fill(mc->getData());
    mc->setDataStatus(0);
    // Unlock access for other user
    ms->Unlock();
   </pre>
   <p> Finally access to the Share memory should be released
   <pre>
   ms->Detach(mc);
   </pre>
   <h5>note</h5> 
   Two empty files are created in /tmp by TSem: \\
    name.sem and name.shm 
   <h5>Warning</h5> 

   The Share memory and the semaphore are still registered in the system even 
   when the Object is destroyed. This is wanted to avoid problem with forked
   processes. Please clean all share memories and semaphores with script 
   clearshm you find in Daq/Acquisition/bin/clearshm
   <pre>
   #!/bin/csh
   ipcs -s | grep `whoami` | awk '{if ($6 != "0") print "ipcrm sem " $2}' >/tmp/tt.
   $$
   ipcs -m | grep `whoami` | awk '{if ($6 == "0") print "ipcrm shm " $2}' >>/tmp/tt
   .$$
   source /tmp/tt.$$
   rm /tmp/tt.$$
   ipcs -a

   </pre>

 */
#include <pthread.h>
 
 class PTLock {
 public:
 PTLock() { pthread_mutex_init(&plock, NULL); }
 ~PTLock() { pthread_mutex_unlock(&plock); }
 
 void lock() { pthread_mutex_lock(&plock); }
 void unlock() { pthread_mutex_unlock(&plock); }
 
 private:
 pthread_mutex_t plock;
 };


class TSem {
private:
  char fName[256]; ///< Name
  char fSemName[256]; ///< Semaphore  file name
  key_t fSemkey; ///< Semaphore key
  int fSemid; ///< Semaphore id

  struct sembuf fSembuf[2]; 
  PTLock* ptlock_;
 protected:
  bool isServer; ///< True if open in create mode
public:
  /**
     Create a TSem object:
     @param name Share memory name
     @param mode TSem_CREATE or TSem_OPEN (see TSem.h)
     @param size Share memory size
   */
  TSem(char* name,int mode) throw (std::string);
  ~TSem() throw (std::string);
  /** Lock the Semaphore to access safely the Share memory */
  void Lock();
  /** Unlock the Semaphore  */
  void Unlock();
  /** Return Share memory name */
  inline const char* GetName() { return fName;}
};








#endif





