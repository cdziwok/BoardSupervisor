/* Copyright 2012 Institut Pluridisciplinaire Hubert Curien
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

   Programmer : 	Christian Bonnin
   Version : 		1.0
   Date of creation:01/08/2012
   Support : 		mail to : christian.bonnin@iphc.cnrs.fr
   FileName : 		BoardSupervisor.h
   Content : 		Supervisor module
   Used in : 		BoardSupervisor
   Support : 		mail to : christian.bonnin@iphc.cnrs.fr
*/

#ifndef _BoardSupervisor_h_
#define _BoardSupervisor_h_


/*
#include <sys/time.h>
*/
#include <mutex>

#include "xdaq/Application.h"
#include "xdaq/ApplicationGroup.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationStub.h"
#include "xdaq/exception/Exception.h"
#include "xdaq/NamespaceURI.h"
#include "xdaq/WebApplication.h"

/*
#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPElement.h"
#include "xoap/Method.h"
#include "xoap/domutils.h"
*/


#include "xgi/Utils.h"
#include "xgi/Method.h"
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"


#include "xdata/String.h"
#include "xdata/UnsignedLong.h"
#include "xdata/Integer.h"

#include "FedStateMachine.hh"

#include "BoardSupervisorV.h"
#include "Acquisition.h"
#include "I2cCommunication.h"
#include "Board.h"
#include "Streamer.h"

#define VIEW_TYPE_MAIN		1	
#define VIEW_TYPE_PARAM		2
#define VIEW_TYPE_I2C		3
#define VIEW_TYPE_FPGA		4
#define VIEW_TYPE_ACQUISITION	5
#define VIEW_TYPE_DIAGRAM	6

///Supervisor main class. Generates XDAQ HTML pages and performs actions from the GUI.
namespace BoardSupervisor{
class Supervisor: public xdaq::WebApplication, xdata::ActionListener
{


public:

        /* define factory method for instantion of this application */
        XDAQ_INSTANTIATOR();

	StripTrackerFedFSM::FedStateMachine fsm_; // application state machine

        Supervisor(xdaq::ApplicationStub * s) throw (xdaq::exception::Exception);
	~Supervisor();
///xdata::ActionListener method called when configuration values are set
	void actionPerformed (xdata::Event& e);

        /* Actions disponibles depuis la page web */
        ///Default Hyperdaq method. Show parameter set values in an HTML table
        void Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
        void MainPage(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
        ///Force Finite State Machine transition
	void FsmTransition(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	///Display FSM diagram
	void FsmDiagram(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	///Show configuration parameters page
	void AcquisitionParam(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	void FpgaConfig(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	void StreamerAcq(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	///Write parameter values into board
	void SaveParam(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	///Load parameter values from board
	void LoadParam(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	void SaveFeCbcEnabling(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	void SaveSetup(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	void LoadSetup(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	/// Write configuration into board (same as FSM configure transition)
	void ConfigureBoard(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	/// Reset I2C bus
	void I2cReset(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	void CbcHardReset(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	void CbcReset101(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	void Test(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
		/// Write 3 fixed values on I2C bus
		void I2cWrite(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
		/// Read values from I2C bus and compare them with default values from I2C File
		void I2cRead(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
		/// Write values from I2C file last column into I2C bus
		void I2cWriteFileValues(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
		void I2cWriteOneValue(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
		void I2cDefaultToMem(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
		void I2cCheckAllValues(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
		void I2cSaveToFile(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
		/// Load HTML parameter values from a file
		void LoadFromFileHtml(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
		/// Save HTML parameter values into a file
		void SaveToFileHtml(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
		void LoadSetupFromFile(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
		void SaveSetupToFile(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
		void RebootBoard(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
		void JumpToGoldenImage(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
		void JumpToUserImage(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
		void UploadImage(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
		void AutoRefresh(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)			{streamer.AutoRefresh(in, out);}
		void ValidParam(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)			{streamer.ValidParam(in, out);}
		void SendSpuriousFrame(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)		{streamer.SendSpuriousFrame(in, out);}
		void ReadFlags(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)			{streamer.ReadFlags(in, out);}
		void EnterData(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)			{streamer.EnterData(in, out);}
		void ValidSimulationData(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)		{streamer.ValidSimulationData(in, out);}
		void ForceStartBg0(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)		{streamer.ForceStartBg0(in, out);}
		void PauseAcquisition(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)		{streamer.PauseAcquisition(in, out);}
		void LoadFromStreamerFileHtml(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)	{streamer.LoadFromFileHtml(in, out);}
		void SaveToStreamerFileHtml(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)	{streamer.SaveToFileHtml(in, out);}
		void EnterConditionData(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)		{streamer.EnterConditionData(in, out);}
		void ValidConditionData(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)		{streamer.ValidConditionData(in, out);}
		void LoadFromFileHtmlCondition(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)	{streamer.LoadFromFileHtmlCondition(in, out);}
		void SaveToFileHtmlCondition(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)	{streamer.SaveToFileHtmlCondition(in, out);}
		void EnterCommissioningLoop(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)	{streamer.EnterCommissioningLoop(in, out);}
		void ValidCommissioningLoop(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)	{streamer.ValidCommissioningLoop(in, out);}
		void LoadFromFileCommissioningLoop(xgi::Input * in, xgi::Output * out )throw(xgi::exception::Exception)	{streamer.LoadFromFileCommissioningLoop(in, out);}
		void SaveToFileCommissioningLoop(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)	{streamer.SaveToFileCommissioningLoop(in, out);}
		void AddCommissioningParameter(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)	{streamer.AddCommissioningParameter(in, out);}
		void RemoveLastCommissioningParameter(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)	{streamer.RemoveLastCommissioningParameter(in, out);}
		void ToggleDataVisuSize(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)		{streamer.ToggleDataVisuSize(in, out);}
		void ResultCommissioningLoop(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)	{streamer.ResultCommissioningLoop(in, out);}
		///Perform initialise transition
	  ///Set reset value to CBC reset address then reset it after a pause
	  void setHardResetValue(int iVal, xgi::Input * in, xgi::Output * out);
	  bool initialising(toolbox::task::WorkLoop* wl);
	  ///Perform configure transition
	  bool configuring(toolbox::task::WorkLoop* wl);
	  ///Perform enable transition
	  bool enabling(toolbox::task::WorkLoop* wl);
	  ///Perform halt transition
	  bool halting(toolbox::task::WorkLoop* wl);
	  ///perform pause transition
	  bool pausing(toolbox::task::WorkLoop* wl);
	  ///Perform resume transition
	  bool resuming(toolbox::task::WorkLoop* wl);
	  ///Perform stop transition
	  bool stopping(toolbox::task::WorkLoop* wl);
	  ///Perform destroy transition
	  bool destroying(toolbox::task::WorkLoop* wl);
	  /** FSM call back   */
	  xoap::MessageReference fsmCallback(xoap::MessageReference msg) throw (xoap::exception::Exception) {    
	    return fsm_.commandCallback(msg);
	  }
	  ///Show FSM status and available transition in an HTML table
	  void WebShowStateMachine(xgi::Output * out) throw (xgi::exception::Exception);

	bool playbackMode() const {return bPlaybackMode.value_;}
	///HTML header for Hyperdaq interface
	void createHtmlHeader(xgi::Output *out, int iViewType, const std::string& strDest=".");
	int getAutoRefresh() const {return iAutoRefresh;}
	void setAutoRefresh(int iRefresh);
	Board* getBoardPointer() {return &mainBoard;}
	Acquisition* getAcquisitionPointer() {return &acquisition;}
private:
	int iAutoRefresh, iCurrentPage;
	Acquisition acquisition;
	I2cCommunication i2cComm;
	Board mainBoard, *pI2cBoard;
	Streamer streamer;
	std::string gFedDebugString_;
	xdata::String fileUhalConfig, fileParamHtml, fileSetupHtml, fileFpgaHtml;
	xdata::String fileHwDescription, fileHwDescriptionI2c, strBoard, strI2cDirectory, strInitSequence;
	xdata::Boolean bPlaybackMode;
	std::mutex fBoardMutex;         /*!< Mutex to avoid conflict btw threads on shared resources*/
	xdata::Integer intDataSize, intCommissionningDelayAfterFastReset, intCommissionningDelayAfterL1a, intCommissionningDelayAfterTestPulse, intCommissionningRequest, intStubLatencyAdjust, intStubLatencyMode, intI2cClkPrescaler, intClkMuxSel, intBackpressureOut50Ohms, intBackpressureOutPolar, intClkIn50Ohms, intClkOut50Ohms, intLemo2SigSel, intThresholdClkIn, intThresholdTrigIn, intTrigIn50Ohms, intTrigInEdge, intTrigOut50Ohms, intExternalData, intTriggerFreq, intTriggerFromTtc, intReadoutDataType, intFe0Masked, intFe1Masked, intReadoutReleaseDelay, intReadoutRelease;
	//xdata::Boolean pbConditionEnabled[NB_CONDITION_DATA];
	//xdata::Integer pintConditionFeId[NB_CONDITION_DATA], pintConditionCbc[NB_CONDITION_DATA], pintConditionPage[NB_CONDITION_DATA], pintConditionRegister[NB_CONDITION_DATA], pintConditionType[NB_CONDITION_DATA], pintConditionValue[NB_CONDITION_DATA];

	/// Insert content of Hyperdaq page
	void insertDescription(xgi::Output * out);
//	void insertFeCbcEnabling(xgi::Output * out);
	void insertI2cDescription(xgi::Output * out, bool bRead);
};
}
#endif


