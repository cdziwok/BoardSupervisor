/* Copyright 2014 Institut Pluridisciplinaire Hubert Curien
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

   FileName : 		FpgaConfig.h
   Content : 		FPGA configuration
   Programmer : 	Christian Bonnin
   Version : 		
   Date of creation : 2014-07-10
   Support : 		mail to : christian.bonnin@iphc.cnrs.fr
*/
#ifndef _FpgaConfig_h_
#define _FpgaConfig_h_

#include "uhal/uhal.hpp"
/**
 * Upload MCS files into Flash EPROM as FPGA configuration
 * @author cbonnin
 */
class FpgaConfig{
public:
	FpgaConfig();
	void setBoard(uhal::HwInterface *board);
	void runUpload(bool bGolden, const std::string& strFile) throw (std::string);
	uint32_t getUploadingFpga() const {return numUploadingFpga;}
	std::string htmlProgressBar() const;
private:
	uhal::HwInterface *lBoard;
	timeval timStart, timEnd;
	uint32_t progressValue, numUploadingFpga;
	std::string progressString;
	
	void confAsyncRead() throw (std::string);
	void blockLockOrUnlock(uint32_t block_number, char operation) throw (std::string);
	void blockErase(uint32_t block_number) throw (std::string);
	void bufferProgram(uint32_t block_number, uint32_t data_address, std::vector<uint32_t>& write_buffer, uint32_t words) throw (std::string);
	void dumpFromFileIntoFlash(bool bGolden, const std::string& strFile) throw (std::string);
};

#endif
