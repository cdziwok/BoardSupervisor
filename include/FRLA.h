#ifndef _FRLA_h_
#define _FRLA_h_

//#include "i2oimfltmsg.h"
#include <unistd.h>
#include "toolbox/rlist.h"
//#include "i2oevbim.h"
#include "i2o/Method.h"
#include "i2o/Listener.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/lang/Class.h"

#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/mem/CommittedHeapAllocator.h"
#include "toolbox/exception/Handler.h"
#include "i2o/utils/AddressMap.h"
#include "i2o/i2oDdmLib.h"


#include "interface/evb/i2oEVBMsgs.h"
#include "interface/shared/i2oXFunctionCodes.h"

#include "xcept/tools.h"


/**
   \class FRLA
   \author L.Mirabito
   \date November 2002
   \version 0.1
   \brief Pool of RU buffer managment class

   <h2> Usage </h2>

   Each FRLA class can receive buffer ready to be send to any
   RU<br>
   One can get a usable buffer by calling usebuffers(),and can forward this 
   buffer with forwardbuffer() calls.<br>


*/
class FRLA  : public virtual toolbox::lang::Class
{
 public:
  /** Default constructor, no Buffer is allowed*/
  FRLA (int defaultSize) 
    {
      toolbox::net::URN urn("toolbox-mem-pool", "FRLAPool");
      try
	{

	  bpool_ = toolbox::mem::getMemoryPoolFactory()->findPool(urn);



	}
      catch (toolbox::mem::exception::Exception& e)
	{
	  std::string errorHistory = xcept::stdformat_exception_history(e);
	  std::cout << "No pool Found "<<std::endl;
	  try
	    {
	      //toolbox::mem::CommittedHeapAllocator* c = new toolbox::mem::CommittedHeapAllocator(0x40000000);

	      toolbox::mem::HeapAllocator* c = new toolbox::mem::HeapAllocator();

	      bpool_ = toolbox::mem::getMemoryPoolFactory()->createPool(urn, c);

	    }
	  catch (toolbox::mem::exception::Exception& xe)
	    {
	      std::cout << "pool error "<<std::endl;
	      std::string errorHistory = xcept::stdformat_exception_history(xe);
	      std::cout << "Cannot create pool "<<std::endl;
	    }
	}
      i2o::bind (this,&FRLA::dataBufferHandler,
		 I2O_RU_DATA_READY, XDAQ_ORGANIZATION_ID);

      buffers_.resize(8192);
      defaultSize_= defaultSize; // buffer sizes

    }

  virtual ~FRLA(){}

  /** Buffer listener: it pushes the buffer in the queue and calls onBuffer() methods */
#undef DEBUGR
  void dataBufferHandler (toolbox::mem::Reference* ref)
    {

#ifdef DEBUGR
      I2O_EVENT_DATA_BLOCK_MESSAGE_FRAME* block  = (I2O_EVENT_DATA_BLOCK_MESSAGE_FRAME*) ref->getDataLocation();

      std::cout << "DEBUG Receive " << block->nbBlocksInSuperFragment <<hex<< ref << dec << std::endl;

#endif


      buffers_.push_back(ref);
      onBuffer ();
    }
  /** True if the list isn't empty*/
  inline bool hasBuffers ()
    {
      return (buffers_.elements()>0);
    }
  /** returns total number of buffers available	*/
  inline int getBuffers ()
    {
      return buffers_.elements();
    }
  /** Returns first buffer in the queue*/
  toolbox::mem::Reference* useBuffer (toolbox::mem::Reference* tail=0 )
    {


      if (buffers_.elements()>0)
	{
	  toolbox::mem::Reference* head=buffers_.front();
	  buffers_.pop_front();

	  //std::cout << head << " head:tail "<< tail <<std::endl;
	  if (tail == 0) return head;

	  // Fill in the fields of the fragment block / message
	  I2O_EVENT_DATA_BLOCK_MESSAGE_FRAME* block  = (I2O_EVENT_DATA_BLOCK_MESSAGE_FRAME*) head->getDataLocation();
	  I2O_EVENT_DATA_BLOCK_MESSAGE_FRAME* newblock = (I2O_EVENT_DATA_BLOCK_MESSAGE_FRAME*) tail->getDataLocation();

	  //std::cout << "Blocks "<< block->nbBlocksInSuperFragment << std::endl;
	  if (block->nbBlocksInSuperFragment == 0)
	    {

	      // Replace the head by the tail




	      newblock->eventNumber = block->eventNumber;
	      newblock->nbBlocksInSuperFragment = 1;
	      newblock->blockNb=0;

	      // Drop the buffer
	      head->release();
	      //std::cout <<" The head is released " << hex << tail <<dec <<std::endl;
	      return tail;

	    }
	  else
	    {
	      //std::cout << "Not the first " <<std::endl;
	      int ninsup = block->nbBlocksInSuperFragment+1;
	      newblock->blockNb = ninsup-1;
	      newblock->nbBlocksInSuperFragment = ninsup;
	      toolbox::mem::Reference* r = head;
	      toolbox::mem::Reference* n = 0;
	      do
		{
		  I2O_EVENT_DATA_BLOCK_MESSAGE_FRAME* rblock = (I2O_EVENT_DATA_BLOCK_MESSAGE_FRAME*) r->getDataLocation();
		  rblock->nbBlocksInSuperFragment = ninsup;
		  n =  r->getNextReference();
		  if ( n == 0)
		    {
		      r->setNextReference(tail);
		      tail->setNextReference(0);
		    }
		  r=n;
		} while (r!=0);

	    }





	  return head;
	}
      else
	return NULL;
    }
toolbox::mem::Reference* appendBuffer (toolbox::mem::Reference* head,toolbox::mem::Reference* tail=0 )
    {


      

	  //std::cout << head << " head:tail "<< tail <<std::endl;
	  if (tail == 0) return head;

	  // Fill in the fields of the fragment block / message
	  I2O_EVENT_DATA_BLOCK_MESSAGE_FRAME* block  = (I2O_EVENT_DATA_BLOCK_MESSAGE_FRAME*) head->getDataLocation();
	  I2O_EVENT_DATA_BLOCK_MESSAGE_FRAME* newblock = (I2O_EVENT_DATA_BLOCK_MESSAGE_FRAME*) tail->getDataLocation();

	  //std::cout << "Blocks "<< block->nbBlocksInSuperFragment << std::endl;
	  if (block->nbBlocksInSuperFragment == 0)
	    {

	      // Replace the head by the tail




	      newblock->eventNumber = block->eventNumber;
	      newblock->nbBlocksInSuperFragment = 1;
	      newblock->blockNb=0;

	      // Drop the buffer
	      head->release();
	      //std::cout <<" The head is released " << hex << tail <<dec <<std::endl;
	      return tail;

	    }
	  else
	    {
	      //std::cout << "Not the first " <<std::endl;
	      int ninsup = block->nbBlocksInSuperFragment+1;
	      newblock->blockNb = ninsup-1;
	      newblock->nbBlocksInSuperFragment = ninsup;
	      toolbox::mem::Reference* r = head;
	      toolbox::mem::Reference* n = 0;
	      do
		{
		  I2O_EVENT_DATA_BLOCK_MESSAGE_FRAME* rblock = (I2O_EVENT_DATA_BLOCK_MESSAGE_FRAME*) r->getDataLocation();
		  rblock->nbBlocksInSuperFragment = ninsup;
		  n =  r->getNextReference();
		  if ( n == 0)
		    {
		      r->setNextReference(tail);
		      tail->setNextReference(0);
		    }
		  r=n;
		} while (r!=0);

	    }





	  return head;
	
    }

  /** Update Event Number on th list */
  /** Returns first buffer in the queue*/
  void updateEventNumber (toolbox::mem::Reference* ref, unsigned int evn )
    {



      toolbox::mem::Reference* r = ref;
      do
	{
	  I2O_EVENT_DATA_BLOCK_MESSAGE_FRAME* rblock = (I2O_EVENT_DATA_BLOCK_MESSAGE_FRAME*) r->getDataLocation();
	  rblock->eventNumber = evn;
	  r =  r->getNextReference();
	} while (r!=0);

    }







  /** forwards to application destination the buffer ref */

 void forwardBuffer (xdaq::Application* app,toolbox::mem::Reference* ref, xdaq::ApplicationDescriptor* destination)
    {
      xdaq::ApplicationDescriptor* originator = app->getApplicationDescriptor();
      toolbox::mem::Reference* refc=ref;
      I2O_MESSAGE_FRAME               *stdMsg = 0;
      do
	{
	  stdMsg = (I2O_MESSAGE_FRAME*)refc->getDataLocation();


	  stdMsg->TargetAddress = i2o::utils::getAddressMap()->getTid(destination);
	  stdMsg->InitiatorAddress = i2o::utils::getAddressMap()->getTid(originator);
      refc= refc->getNextReference();
      //std::cout << "reference " << refc <<std::endl;
	} while(refc!=0);

      //std::cout <<" Sending TO " << stdMsg->TargetAddress << " from " <<  stdMsg->InitiatorAddress << std::endl;
      try
	{
	  app->getApplicationContext()->postFrame(ref, originator,destination);

	}
      catch (xdaq::exception::Exception& xe)
	{

	  std::string errorHistory = xcept::stdformat_exception_history(xe);
	}

    }


  /** return the user-space address in buffer ref where one can write */
  unsigned long getUserAddress (toolbox::mem::Reference* ref)
    {
      I2O_MESSAGE_FRAME               *stdMsg = 0;
      I2O_EVENT_DATA_BLOCK_MESSAGE_FRAME *block  = 0;

      size_t msgHeaderSize       = sizeof(I2O_EVENT_DATA_BLOCK_MESSAGE_FRAME);
      stdMsg = (I2O_MESSAGE_FRAME*)ref->getDataLocation();
      block  = (I2O_EVENT_DATA_BLOCK_MESSAGE_FRAME*)stdMsg;
      unsigned char *cbuf = (unsigned char*) block+msgHeaderSize;
      // std::cout << "User Address " <<dec << (unsigned long) stdMsg << " " << (unsigned long) cbuf << "Header size " <<msgHeaderSize <<   dec <<std::endl;

      return (unsigned long) cbuf;

    }
  /** return the physical address of buffer ref where one can write */
  unsigned long getPhysicalAddress (toolbox::mem::Reference* ref)
    {
      return getUserAddress(ref);


    }
  /** update the current size of data written in buffer ref */
  unsigned long updateSize (toolbox::mem::Reference* ref, U32 siz)
    {

      I2O_MESSAGE_FRAME               *stdMsg = 0;
      //I2O_EVENT_DATA_BLOCK_MESSAGE_FRAME *block  = 0;


      size_t msgHeaderSize       = sizeof(I2O_EVENT_DATA_BLOCK_MESSAGE_FRAME);
      size_t realsize    = siz ;
      size_t dataBufSize = realsize + msgHeaderSize;



      // Fill in the fields of the fragment block / message
      stdMsg = (I2O_MESSAGE_FRAME*)ref->getDataLocation();

      //block  = (I2O_EVENT_DATA_BLOCK_MESSAGE_FRAME*)stdMsg;

      stdMsg->MessageSize    = (dataBufSize) >> 2;
      // Fill in payload check markers for debugging

      ref->setDataSize(dataBufSize);


      return realsize;
    }

  /** default action on buffer reception */
  virtual void onBuffer ()
    {
      return;
    }

  /** Clear all buffers */
  void clearBuffers ()
    {
      //    printf("Size>>>>>>>>>>>>>>>>%d \n",buffers_.size());
      if (buffers_.elements()==0) return;
      while (hasBuffers ())
	{

	  toolbox::mem::Reference* ref = useBuffer ();
	  ref->release();
	}

    }

  /** Number of Buffers in the stack */
  inline int getNumberOfBuffers(){return buffers_.elements();}



  /** Allocate a new buffer with dat size of reqsize */
  toolbox::mem::Reference* allocateDataBuffer(int reqsize=0)  throw (xcept::Exception)
    {

      if (reqsize == 0) reqsize =defaultSize_;
      I2O_MESSAGE_FRAME               *stdMsg = 0;
      I2O_PRIVATE_MESSAGE_FRAME       *pvtMsg = 0;
      I2O_EVENT_DATA_BLOCK_MESSAGE_FRAME *block  = 0;


      size_t msgHeaderSize       = sizeof(I2O_EVENT_DATA_BLOCK_MESSAGE_FRAME);
      size_t fullBlockPayload    = reqsize;
      size_t dataBufSize = fullBlockPayload + msgHeaderSize;

      toolbox::mem::Reference* bufRef=NULL;
      // Allocate memory for a fragment block / message
      try
	{
	  bufRef = toolbox::mem::getMemoryPoolFactory()->getFrame(bpool_,dataBufSize);
	}
      catch(xcept::Exception e)
	{
	  XCEPT_RETHROW(xcept::Exception,
			"Failed to allocate memory for a fragment block",
			e);
	}

      bufRef->setDataSize(dataBufSize);

      // Fill in the fields of the fragment block / message
      stdMsg = (I2O_MESSAGE_FRAME*)bufRef->getDataLocation();
      pvtMsg = (I2O_PRIVATE_MESSAGE_FRAME*)stdMsg;
      block  = (I2O_EVENT_DATA_BLOCK_MESSAGE_FRAME*)stdMsg;

      memset(block, 0, sizeof(I2O_EVENT_DATA_BLOCK_MESSAGE_FRAME));

      pvtMsg->XFunctionCode  = I2O_RU_DATA_READY;
      pvtMsg->OrganizationID = XDAQ_ORGANIZATION_ID;

      stdMsg->MessageSize    = (dataBufSize) >> 2;
      stdMsg->TargetAddress  = 0;
      stdMsg->Function       = I2O_PRIVATE_MESSAGE;
      stdMsg->VersionOffset  = 0;
      stdMsg->MsgFlags       = 0;  // Point-to-point

      block->eventNumber             = 0;
      block->blockNb                 = 0;
      block->nbBlocksInSuperFragment = 0;

      // Fill in payload check markers for debugging
    
      return bufRef;

    }


 protected:
  /** List of buffers */
  toolbox::rlist < toolbox::mem::Reference* >buffers_;
  toolbox::mem::Pool* bpool_;
  int defaultSize_;
};
#endif
