/* Copyright 2013 Institut Pluridisciplinaire Hubert Curien
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

   FileName : 		ParameterSet.h
   Content : 		Set of parameters written into board
   Programmer : 	Christian Bonnin
   Version : 		1.0
   Date of creation : 13/06/2013
   Support : 		mail to : christian.bonnin@iphc.cnrs.fr
*/
#ifndef __PARAMETERSET_H__
#define __PARAMETERSET_H__

#include <map> 
#include <string> 
#include <cgicc/Cgicc.h>
#include "xdaq/Application.h"
#include "xdata/Serializable.h"
#include "Tracker/ParamSet.h"

#define DEFAULT_VALUES_EXTENSION ".defaultvalues.txt"

namespace Ph2_HwInterface{ class BeBoardInterface;}
namespace Ph2_HwDescription{ class BeBoard;}

enum HTML_CONTROL_TYPE {CHECKBOX, COMBOBOX, TEXTFIELD, HEXA, INTEGER, CHARS};
/** Set of parameters whose values will be read from and written into a board and displayed and set by the GUI.
 * Parameter names are (nearly) the same in board address table, in HTML components and in internal values map.  */ 
class ParameterSetUhal:public ParamSet{
public:
	ParameterSetUhal();
	/* /// get a parameter value
	uint32_t getValue(const std::string& strName);
	std::string getStrValue(const std::string& strName);
	bool containsIntValue(const std::string& strName);
	bool containsStrValue(const std::string& strName);
	void removeIntValue(const std::string& strName);
	///Set a parameter value
	uint32_t setValue(const std::string& strName, uint32_t uVal);
	void setValue(const std::string& strName, const std::string& strVal);
	/// Return parameter names and their value in a string with format <parameter 1>=<value 1>\n<parameter 2>=<value 2>\n...
	std::string nameAndValuePairs();
	/// Read parameter and value pairs from a text file and write them into the board
	bool loadParamValuePairsFromFile(const std::string& strFile);
	void clearValues();
*/
	/**Set a parameter value according to an HTML component
	 * @param cgi CGI context
	 * @param strName Parameter name
	 * @param bBoolean The HTML component is a checkbox 
	 * @param bHexa The HTML component is a textfield containing a hexadecimal value
	 * @return Integer value found in the component or 0 if the value is a string */
	uint32_t setFromGui(cgicc::Cgicc& cgi, const std::string& strName, HTML_CONTROL_TYPE type);//bool bBoolean, bool bHexa=false);
	/** Write a parameter value into the board.
	 * @param strName parameter name 
	 * @param bDispatch IPBUS commands are really sent to the board (no need to manually dispatch */
	void writeIntoBoard(const std::string& strName);//, bool bDispatch);
	///Set the uhal hardware interface (e.g. GLIB board) running IPBus.
	void setBoard(Ph2_HwInterface::BeBoardInterface* pbbi, Ph2_HwDescription::BeBoard* pbb);
	std::string htmlDescription(const std::string& strName, HTML_CONTROL_TYPE ctrlType, const std::string& strArg="");
	/// read and set all parameter values from the board
	void readAllFromBoard();
	///Read all HTML component names from the form file, add them to the maps, and read the values from the board.
	void readHtmlFormAndValues(const std::string& strPath);
	std::string transformHtmlFileDescription(const std::string& strPath, const std::string& strLoadSave);
	std::string transformHtmlFormDescription(std::istream& isForm, const std::string& strPath, const std::string& strLoadSave);
	///Write all parameter values into the board
	void readAllValuesAndWriteIntoBoard(cgicc::Cgicc* pcgi, bool bWrite);
	/** Insert the option tags of an HTML select (choices of a combobox)
	 * @param ostream output stream
	 * @param strName Component and parameter name
	 */ 
	void insertOptionsForNode(std::ostringstream& ostream, const std::string& strName);
	void mapToSerializable(xdaq::Application* pApp, const std::string& strParam, xdata::Serializable* pData);
	uint32_t setValue(const std::string& strName, uint32_t uVal);
	///Set a parameter value (string)
	void setValue(const std::string& strName, const std::string& strVal);
	void setValuesFromSerializable();
private:
	std::map <std::string, xdata::Serializable*> mapSerializable;
	
	Ph2_HwInterface::BeBoardInterface* beBoardInterface;                     /*!< Interface to the BeBoard */
	Ph2_HwDescription::BeBoard* beBoard;
	/** Search the next HTML component name (name=) in a string and return its position 
 * @param strLig HTML line
 * @param strName string modified to contain the HTML component hame
 * @param piPos position in the line to begin the search. Will be modified
 * @param piEnd Will be modified to the end position of the string name='...'
 */
	size_t nextHtmlComponentName(const std::string& strLig, std::string& strName, size_t *piPos, size_t* piEnd);
	/**Insert an HTML select (combobox) component into an output stream
	 * @param ostream output stream
	 * @param strLig Original HTML line to be transformed
	 * @param strName HTML component (and parameter) name
	 * @param piPos Position in the line to search for the select tag end
	 * @param piEnd End position in the line of the transformed substring
	 */ 
	void insertHtmlSelectOptions(std::ostringstream& ostream, const std::string& strLig, const std::string& strName, size_t *piPos, size_t *piEnd);
	std::string componentTypeFromName(const std::string& strName);
	std::string descriptionFromName(const std::string& strName);
	void setOneValueFromSerializable(const std::string& strName, xdata::Serializable* pSer);
};

#endif
