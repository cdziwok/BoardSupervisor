/* Copyright 2014 Institut Pluridisciplinaire Hubert Curien
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

   Programmer : 	Christian Bonnin
   Version : 		1.0
   Date of creation:5/05/2014
   Support : 		mail to : christian.bonnin@iphc.cnrs.fr
   FileName : 		CommissioningLoop.h
   Content : 		Performs acquisition loops with varying I2C parameter values
   Used in : 		CBCDAQ
*/
#ifndef COMMISSIONINGLOOP_H_
#define COMMISSIONINGLOOP_H_

#include "xgi/Utils.h"
#include "xgi/Method.h"
#include "ParameterSet.h"

namespace Ph2_HwDescription{ class BeBoard;}
namespace Ph2_HwInterface{ class CbcInterface;}
class Board;
///Commissioning loops configuration and use. They are used to program acquisitions with varying I2C parameter values
class CommissioningLoop
{
public:
	CommissioningLoop();
	virtual ~CommissioningLoop();
	void setI2cDirectory(const std::string& strDir);
	void insertHtmlForm(xgi::Output *out, Board* pBoard, ParameterSetUhal& pSetFeCbc);
	void insertCbcTable(xgi::Output *out, int iLig);
	void addOneParameter();
	void removeLastParameter();
	///get parameter set
	ParameterSetUhal& getParameterSet() {return pSet;}
	void setValuesFromGui(cgicc::Cgicc& cgi);
	void initAcq();
	void nextAcq(Ph2_HwInterface::CbcInterface *cbcInterface);
	std::string addI2cRequestsForParam(Ph2_HwInterface::CbcInterface *cbcInterface, int iLig, uint32_t uWrite);
	const std::string& getLog() const {return strLog;}
	const std::string& getCurrentValuesString() const {return strCurrentValues;}
	const uint32_t getNbIterations() const { return nbIterations;}
	void storeVisuData(uint32_t* arrData, uint32_t event_counter);
	std::string getDataVisuCommLoopString(uint32_t iCom);
	uint32_t* getDataVisuCommLoop(uint32_t iCom);
	uint32_t getCommLoopNbEvent(uint32_t iCom);
	uint32_t* createSumArray();
private:
	uint32_t calcNbIterations();

	ParameterSetUhal pSet;
	ParameterSetUhal  *ppSetFeCbc;
	Ph2_HwDescription::BeBoard* beBoard;
	std::string strChoices, strI2cDirectory, strLog, strCurrentValues;
	void readI2cParameters();
	uint32_t numLoop, nbIterations, lastEvtCounter;
	uint32_t  *arrCommLoopVisu;
	std::map < uint16_t, std::string > mapI2cNames;
	std::vector< std::string > vecCommLoopString;
	std::vector< uint32_t > vecNbEvent;
};

#endif /*COMMISSIONINGLOOP_H_*/
