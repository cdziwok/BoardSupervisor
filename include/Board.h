/* Copyright 2013 Institut Pluridisciplinaire Hubert Curien
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

   Programmer : 	Christian Bonnin
   Version : 		1.0
   Date of creation:18/09/2013
   Support : 		mail to : christian.bonnin@iphc.cnrs.fr
   FileName : 		Board.h
   Content : 		Generic board management (GLIB, FC7)
   Used in : 		CBCDAQ
*/

#ifndef _Board_h_
#define _Board_h_

#include <string>
#include "xgi/Utils.h"
#include "xgi/Method.h"
#include "ParameterSet.h"
#include "HWInterface/BeBoardInterface.h"

#define RESET_HARD			1
#define RESET_FAST			2

//namespace Ph2_HwInterface{ class BeBoardInterface;}
namespace Ph2_HwInterface{ class CbcInterface;}
namespace Ph2_HwDescription{ class BeBoard;}
 
///Generic board management (GLIB, FC7) that sends commands, retrieve information and make descriptions of the board status.
class Board
{
	public:
		Board();
		~Board();
		//uhal::HwInterface* getHardwareInterface() const{return lBoard;}
		Ph2_HwInterface::BeBoardInterface* getBeBoardInterface() const {return beBoardInterface;}
		Ph2_HwInterface::CbcInterface* getCbcInterface() const {return cbcInterface;}
		Ph2_HwDescription::BeBoard* getBeBoard() const {return beBoard;}
		//void setHardwareInterface(uhal::HwInterface* pBoard);
		/// Load GLIB status flags and construct HTML description
		void loadStatus();
		std::string launchTest(int iNb);
		void sendResetCommand(int iVal);
		uint32_t getFirmwareVersion() const{return uFirmwareVersion;}
//		std::string getFmc2Description();
//		std::string getFmc1Description();
//		std::string getGbtDescription();
//		std::string getFeAndCbcDescription();
//		std::string getCbcDescription(uint32_t uFE);
		void insertFeCbcEnabling(xgi::Output *out, ParameterSetUhal& pSetConfig, ParameterSetUhal& pSetFeCbc);
		void insertFeCbcLinks(xgi::Output *out, ParameterSetUhal& pSetConfig, ParameterSetUhal& pSetFeCbc);
		void insertFpgaImages(xgi::Output *out);
		void initCbcFilename();
		std::string nextCbcFilename( ParameterSetUhal& pSetFeCbc);
		void reboot();
		void jumpToImage(const std::string& strImage);
		void uploadFpgaConfig(const std::string& strImage, const std::string& strFile) throw (std::string);
		uint32_t isUploadingFpga() const; 
		std::string htmlFpgaProgressBar() const;
		/// get Firmware information parameter set
		ParameterSetUhal& getParameterSetInfo() { return pSetInfo;}
		void initialize(const std::string& strFile, ParameterSetUhal* pSetSuper);
		const std::string& getI2cDirectory() const { return strI2cDirectory;}
		void destroy();
		
		uint32_t nbFrontEnd(ParameterSetUhal& pSetConfig);
		void loadFpgaImages(); 
		static std::string expandEnvironmentVariables( std::string s );
	private:
		//uhal::HwInterface *lBoard;
		Ph2_HwInterface::BeBoardInterface* beBoardInterface;                     /*!< Interface to the BeBoard */
		Ph2_HwInterface::CbcInterface* cbcInterface;                     /*!< Interface to the CBC */
		Ph2_HwDescription::BeBoard *beBoard;
                Ph2_System::SystemController *systemCtrl;

		std::string strTtcVer, strI2cDirectory;
		//uhal::ValWord< uint32_t > lFmc1, lFmc2, lGbt, lCbc;
		uint32_t uCbcIterator, uFirmwareVersion;
		ParameterSetUhal pSetInfo;
		std::vector<std::string> vecImages;
};

#endif
