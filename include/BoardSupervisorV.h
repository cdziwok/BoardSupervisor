/* Copyright 2012 Institut Pluridisciplinaire Hubert Curien
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

   Programmer : 	Christian Bonnin
   Version : 		1.0
   Date of creation:01/08/2012
   Support : 		mail to : christian.bonnin@iphc.cnrs.fr
*/

#ifndef _BoardSupervisorV_h_
#define _BoardSupervisorV_h_



#define SUPERVISOR_PACKAGE_VERSION "1.0"


#include "config/PackageInfo.h"

namespace BoardSupervisor 
{

    const std::string package  =  "BoardSupervisor";
    const std::string versions =  SUPERVISOR_PACKAGE_VERSION;
    const std::string description = "BoardSupervisor: CBC data acquisition for october 2012 beam test";
    const std::string link = "https://sbgtrac.in2p3.fr/projects/cms";
    const std::string authors  =  "Christian BONNIN - christian.bonnin@iphc.cnrs.fr";
    const std::string summary  =  "";
    config::PackageInfo getPackageInfo();
    void checkPackageDependencies() throw (config::PackageInfo::VersionException);
    std::set<std::string, std::less<std::string> > getPackageDependencies();
    
}

#endif




