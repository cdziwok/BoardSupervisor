/* Copyright 2012 Institut Pluridisciplinaire Hubert Curien
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

   Programmer : 	Christian Bonnin
   Version : 		1.0
   Date of creation:01/08/2012
   Support : 		mail to : christian.bonnin@iphc.cnrs.fr
   FileName : 		I2cCommunication.h
   Content : 		CDR Controller module
   Used in : 		CBCDAQ
*/

#ifndef _I2cCommunication_h_
#define _I2cCommunication_h_

#include <string>
namespace Ph2_HwInterface{ class BeBoardInterface;}
namespace Ph2_HwInterface{ class CbcInterface;}
namespace Ph2_HwDescription{ class BeBoard;}
///Communication on I2C bus (via IPBus)
class I2cCommunication
{
	public:
	///Constructor with settings, command and reply uHal addresses
		I2cCommunication(const std::string& strSet, const std::string& strCmd, const std::string& strRep, uint32_t iSlave);
	///Initialisation
		void init();
	/// Send commands to reset the board
		bool reset( Ph2_HwInterface::BeBoardInterface* pBoardInterface, Ph2_HwDescription::BeBoard* pBoard);
	/// Write 3 values at 3 addresses on I2C bus 
		bool write3values(Ph2_HwInterface::BeBoardInterface* pBoardInterface, Ph2_HwDescription::BeBoard* pBoard);
	/// Get log string
		const std::string& getLog() const {return strLog;} 
		/// Read I2C values from board and default values from I2C file and compare them in an HTML table  
		std::string showValuesFromI2cAndFile(Ph2_HwInterface::CbcInterface *cbcInterface,  Ph2_HwDescription::BeBoard *beBoard, bool bRead);
		/// write I2C values stored in memory into the board
		bool writeFileValues(Ph2_HwInterface::CbcInterface *cbcInterface,  Ph2_HwDescription::BeBoard *beBoard, bool bCheck);
		/// Write one hexadecimal I2C value into the selected CBCs or the current one if none is selected
		void writeOneCbcValue(Ph2_HwInterface::CbcInterface *cbcInterface, Ph2_HwDescription::BeBoard *beBoard, const std::string& strName, const std::string& strValue, const std::string& strChecked);
                /// Set default value into memory value for all registers of the current CBC
                void setDefaultValuesIntoMem(Ph2_HwDescription::BeBoard *beBoard);
		/// I2C values file name
		const std::string& getValuesFile() const { return strValuesFile;}
		/// Set I2C values file name
		void setValuesFile(const std::string& strFilename);
		/// I2C values files directory
		const std::string& getValuesDirectory() const { return strValuesDir;}
		/// Set I2C values files dirctory
		void setValuesDirectory(const std::string& strDir);

		//void blockInit(uhal::HwInterface *lBoard);//, uint32_t uFE);		
		//void blockSendRequest(uhal::HwInterface *lBoard, std::vector<uint32_t>& vecReq, bool bWrite);
		//static void blockAdd(std::vector<uint32_t>& vecReq, uint32_t uFE, uint32_t uCBC, uint32_t uPage, uint32_t uAddr, uint32_t uWrite);
		//std::vector< uint32_t > blockWaitAndReadResult(uhal::HwInterface *lBoard, uint32_t uSize);
		bool valuesFromLine(std::string strLine, bool bWrite, std::string& strName,
				uint32_t *puPage, uint32_t *puAddr, uint32_t *puDef, uint32_t *puWrite);
		std::string getValuesAsText(Ph2_HwInterface::CbcInterface *cbcInterface, Ph2_HwDescription::BeBoard *beBoard, const std::string& strCbc);
		void checkAllValues(Ph2_HwInterface::CbcInterface *cbcInterface, Ph2_HwDescription::BeBoard *beBoard, const std::string& strChecked);
		void checkAllValuesForOneCbc(Ph2_HwInterface::CbcInterface *cbcInterface, Ph2_HwDescription::BeBoard *beBoard, uint32_t uFE, uint32_t uCBC);
		
	private:
		std::string strLog, strValuesFile, strValuesDir;
		std::string strSettings, strCommand, strReply;
		std::string strSram, strSramUserLogic;//strOtherSram;
		uint32_t iI2cSlave;
	/// Send a command (read or write) on the bus
		uint32_t send(Ph2_HwInterface::BeBoardInterface* pBoardInterface, Ph2_HwDescription::BeBoard* pBoard, uint16_t addr, uint16_t data, bool bWrite);
	/// Read a value at a given address on I2C bus
		uint32_t read(Ph2_HwInterface::BeBoardInterface* pBoardInterface, Ph2_HwDescription::BeBoard* pBoard, uint16_t addr);
	/// Write a data at a given address on I2C bus
		bool write(Ph2_HwInterface::BeBoardInterface* pBoardInterface, Ph2_HwDescription::BeBoard* pBoard, uint16_t addr, uint16_t data);
	/// Check that a data can be read at a fixed read address
		bool checkRead(Ph2_HwInterface::BeBoardInterface* pBoardInterface, Ph2_HwDescription::BeBoard* pBoard);
	/// Enable I2C bus before sending commands
		void enable( Ph2_HwInterface::BeBoardInterface* pBoardInterface, Ph2_HwDescription::BeBoard* pBoard);
	/// Discable I2C bus after sending commands
		void disable(Ph2_HwInterface::BeBoardInterface* pBoardInterface, Ph2_HwDescription::BeBoard* pBoard);
		bool extractFeAndCbcNumbers(const std::string& strCbcName, uint32_t *puFe, uint32_t *puCbc);
				
};
#endif
