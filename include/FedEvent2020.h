/* Copyright 2013 Institut Pluridisciplinaire Hubert Curien
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

   Programmer : 	Christian Bonnin
   Version : 		1.0
   Date of creation:01/10/2013
   Support : 		mail to : christian.bonnin@iphc.cnrs.fr
*/
#ifndef FEDEVENT2020_H_
#define FEDEVENT2020_H_

#include "xdaq/Application.h"
#include "FedEvent.h"

class ParameterSetUhal;
/**Front End Driver (FED) event data with new data format for CBC-derived DAQ aimed to be integrated in CMS DAQ at the horizon 2020.
Format description shared document: https://docs.google.com/document/d/1fbvhCCfj3Z_3RFnfyVJCENTveIEjzlbHJN-ZfelgWCA/edit
 */
class FedEvent2020:public FedEvent
{
public:
 	FedEvent2020(uint32_t uAcqMode, bool bZeroSuppr, bool bConditionData, bool bFakeData,
							uint32_t uFE, uint32_t nbCBC, uint32_t uCBC, ParameterSetUhal* pPSet, const char * buffer, uint32_t index);
private:
	void fillTrackerHeader(const char * buffer, uint32_t idxBuf, uint64_t uFE, uint32_t nbCBC, uint32_t uAcqMode, 
							bool bZeroSuppr, bool bConditionData, bool bFakeData);
	void fillTrackerPayload(const char * buffer, uint32_t idxBuf, uint32_t nbFE, uint32_t nbCBC, uint32_t uCBC, 
							uint32_t nbBitsHeader, bool bZeroSuppr, bool bCondition, uint32_t nbCondition, ParameterSetUhal* pPSetCondition);
	void fillTrackerConditionData(const char* buffer, uint32_t idxBuf, uint32_t idxPayload, 
							uint32_t nbCBC, uint32_t nbCondition, ParameterSetUhal* pPSetCondition);
	uint32_t calcBitsForFE(const char* buffer, uint32_t idxBuf, char* dest, uint32_t idxDest, uint32_t nbCBC);
	void setValueBits(char* arrDest, uint32_t bitDest, uint8_t width, uint8_t uVal);
	uint32_t countConditionData(ParameterSetUhal* ppSetCondition);
};
#endif
