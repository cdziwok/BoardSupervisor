/* Copyright 2012 Institut Pluridisciplinaire Hubert Curien
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

   Programmer : 	Christian Bonnin
   Version : 		1.0
   Date of creation:01/08/2012
   Support : 		mail to : christian.bonnin@iphc.cnrs.fr
*/
#ifndef FEDEVENT_H_
#define FEDEVENT_H_

#include "xdaq/Application.h"
#include "FRLA.h"
#include "toolbox/mem/Reference.h"

#define NB_STRIPS					128
#define NB_STRIPS_CBC2				256
#define DAQ_TRAILER_SIZE	8
#define DAQ_HEADER_SIZE		8
//one CBC data size in memory from board;
#define CBC_SIZE	(NB_STRIPS_CBC2/8+4)

///Front End Driver (FED) event data
class FedEvent
{
public:
	FedEvent();
	virtual ~FedEvent();
	///Pointer of FED event data 
	virtual const char* getDataAddress() const {return data_;}
	///Event size in bytes
	virtual uint32_t getSize() const {return size_;}

protected:
	char *data_;
	uint32_t size_;
	/**Calculate byte index in data payload from 'normal' index. Bytes are not in the same order in destination buffer as they are constructed.
	 * They are put in 64 bits words (8 bytes) in little endian order (from right to left) : 7 6 5 4 3 2 1 0 8 9 ...
	 * @return index in destination buffer
	 */  	
	uint32_t littleEndian8(uint32_t n);
	void reverseByte(char & b);
};

///Event read from a file. It contains the DAQ header and trailer
class SimulatedEvent:public FedEvent{
public:
        SimulatedEvent(const char *buffer, uint32_t size);
};
/** Construct one event from GLIB data. One fiber out of 12 is filled with data and the others are empty.
 * @param buffer GLIB data buffer
 * @param index index of the event data in buffer 
 */ 
class OneCbcEvent:public FedEvent{
 public:
	OneCbcEvent(const char * buffer, uint32_t index);
	
private:
	/**Data buffer construction and data size computing for one fibre. Two pass are necessary: <ul>
	 * <li> one to compute size with NULL as destination buffer</li>
	 * <li> one to construct destination buffer</li></ul>
	 * @return Data size in bytes
	 * @param buffer GLIB data buffer
	 * @param index index of event data in buffer
	 * @param dest destination buffer. Set to NULL to just compute size.
	 * @param idxDest index to put data for this fiber into destination buffer
	 */
	uint32_t calcBytesForFiber(const char* buffer, uint32_t index, char* dest, uint32_t idxDest);
	/** Fill FED data of all fibers. 
	 * @param buffer GLIB data buffer
	 * @param index index of event data in buffer
	 */
	void fillDataForFibers(const char *buffer, uint32_t index);

};
/* ************************************************* */
/// Data with DAQ header and trailer
class DAQData{
public:
	static int              crc_tab16_init;
	static unsigned short   crc_tab16[256];

	DAQData(FedEvent *pEvent, uint32_t nbL1A, uint32_t nbBunchCrossing);
	DAQData(SimulatedEvent *pEvent, const char* buffer);
	virtual ~DAQData();
	///Payload data buffer
	const char* getDataAddress() const;
	///Header buffer
	const char* getHeaderAddress() const;
	///Trailer buffer
	const char* getTrailerAddress() const;
	///Data size in bytes including header and trailer
	uint32_t	getDaqSize() const;
	///Compute CRC16 of a file content
	static std::string calcCrc16FromFile(const std::string& strFile);
private:
	/** Compute Cyclic Redundancy Check code 
	 * @param Adresse_tab data address
	 * @param Taille_max data size
	 * @param Crc previous code (cyclic)
	 */
	static uint16_t Crc16(const char *Adresse_tab , uint32_t Taille_max, uint16_t Crc) ;
	
	char *header_, *trailer_;
	FedEvent *event_;

	static unsigned short update_crc_16( unsigned short crc, char c ) ;
	static void init_crc16_tab( void ) ;
	//CRC16 computing using method given by VHDL source at http://cmsdoc.cern.ch/cms/TRIDAS/horizontal/RUWG/DAQ_IF_guide/DAQ_IF_guide.html
	static uint16_t crcVhdl(char *pBuf, uint16_t uCrc);
};
/* ********************************************** */
/// Data to be sent to RU-BU-FU chain via shared memory with FRL header
class FRLData{
public:
	FRLData(DAQData *pDaq);
	FRLData(const char *pChar, uint32_t uSize);
	/** Allocate memory and fill data buffer and FRL header.
	 * @param sharedMem FRLA object in shared memory
	 * @param uEvt Event number
	 * @return Reference to be sent to shared memory
	 */
	toolbox::mem::Reference* generateReference(FRLA* sharedMem, uint32_t uEvt);
	///Data size in bytes
	uint32_t getFrlSize() const;
private:
	///Data dump for debug
	void dumpData(FRLA* sharedMem, toolbox::mem::Reference* tail) const;

	DAQData *daqData_;
	const char* ptrData;
	uint32_t uDataSize;
};

#endif /*FEDEVENT_H_*/
