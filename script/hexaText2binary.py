#!/usr/bin/python

import optparse 
import sys

parser = optparse.OptionParser(usage = "%prog [options] -o <output file> <text file>\n"
   "Generate a binary file from its content in hexadecimal format"
)
parser.add_option("-o", "--output", action="store", dest="output", help="Output binary file", metavar="FILE")
parser.add_option("-l", "--little", action="store_true", dest="little", help="Little endian order (LSB first)", default=False)
(options, args) = parser.parse_args()
if len(args)==0 or options.output==None: 
    parser.print_help()
    sys.exit()

filOutput = open(options.output, 'w')
filContent= open(args[0],'r')
line=filContent.readline()
while len(line)>0:#Read format file
        line = line.strip()
        nbOct = len(line)/2
        for iOct in range(0, nbOct):
                if options.little:
                        idx=(iOct ^ (nbOct-1))*2
                else:
                        idx=iOct*2

                strHex=line[idx:idx+2]
                filOutput.write("%c" % int(strHex,16))

        line=filContent.readline()

filOutput.close()
filContent.close()
