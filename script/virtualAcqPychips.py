#! /usr/bin/env python
import sys
import os
from PyChipsUser import *
from time import sleep
#from optparse import OptionParser

ipaddr = "192.168.0.80"
glibAddrTable = AddressTable(os.environ['CBCDAQ']+"/BoardSupervisor/config/glibAddrTable.dat")
board = ChipsBusUdp(glibAddrTable, ipaddr, 50001)
print "Board Id:",hex(board.read("board_id"))

#parser = OptionParser(usage = "%prog [options]\n\n"
              #"Send CDR acquisition data to an IPBus dummy hardware at localhost to simulate an acquisition."
              #"--------------------------------")
#parser.add_option("-c", "--channel", action="store", dest="channel", default=1, help="Channel number")
#(options, args) = parser.parse_args()
nbPack = board.read("CBC_DATA_PACKET_NUMBER")+1;
packetSize = 10
print "Nb of packets: ",nbPack;
for iEn in range(0,32768):
	board.blockWrite("sram1", range(iEn*2*nbPack,nbPack*(iEn*2+packetSize)))
	board.write("SRAM1_full", 1)
	sleep(0.25);
	board.write("SRAM1_full", 0)
	board.blockWrite("sram2", range(nbPack*(iEn*2+1),nbPack*(iEn*2+1+packetSize)))
	board.write("SRAM2_full", 1)
	sleep(0.25);
	board.write("SRAM2_full", 0)
	sys.stdout.write(str(iEn)+'\r')
	sys.stdout.flush()

