import sys

nb=10
if len(sys.argv)>1:
	nb=int(sys.argv[-1])

for i in range(1,nb+1):
  print "{0:020b}{1:020b}{2:020b}{3:020b}{4:020b}{5:020b}{6:08b}".format(i,i,i,i,i,i,i%256)

