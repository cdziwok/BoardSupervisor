#!/usr/bin/python

import sys
import optparse 
import uhal
#if sys.version_info[0:2] != (2, 4): raise Exception('requires python 2.4')
parser = optparse.OptionParser(usage = "%prog [options] \n\nExample of pycoHAL usage (uHAL reading and writing in python")
parser.add_option("-i", "--ip", action="store", dest="ip", help="IP address of IPbus board", default="192.168.007.221")
parser.add_option("-a", "--addresses", action="store", dest="addresses", help="uHAL address XML file", default="${CBCDAQ}/BoardSupervisor/xml/cta/address_table.xml")
parser.add_option("-p", "--param", action="store", dest="param", help="parameter uHAL identifier", default="board_id")
(options, args) = parser.parse_args()

uhal.disableLogging()
#manager = uhal.ConnectionManager("file://${CBCDAQ}/BoardSupervisor/xml/cta/connections.xml")
#hw = manager.getDevice("board0")
hw = uhal.getDevice( "board0" , "ipbusudp-2.0://"+options.ip+":50001",  "file://"+options.addresses) 
#hw.getNode(options.param).write(12)
#hw.dispatch()
l=hw.getNode(options.param).read()
hw.dispatch()
print options.param,'=',hex(l.value())
