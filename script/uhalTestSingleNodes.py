#!/usr/bin/python
#Read / write test of all nodes of a uhal XML address file. Usage : uhalTestSingleNodes.py [board name] [connections file]

#Test read and write access of all parameters of an uHAL XML address file
#Usage: python2.4 uhalTestSingleNodes.py <connections file> <address file>
import sys
if sys.version_info[0:2] != (2, 4): raise Exception('requires python 2.4')
import uhal

board="localhost"
if len(sys.argv)>1:
  board=sys.argv[1]

path="file:///home/xtaldaq/trackerDAQ-3.1/CBCDAQ/BoardSupervisor/xml/connections.xml"
if len(sys.argv)>2:
  path=sys.argv[2]

manager = uhal.ConnectionManager(path)
hw = manager.getDevice(board)
for strNode in hw.getNodes():
	node=hw.getNode(strNode)
	if node.getPermission()==uhal._core.NodePermission.READ:
		print "Read from "+strNode+": ",
		l=node.read()
		hw.dispatch()
		print l.value()
	else:
		value=node.getMask()
		while value%2==0:
			value=value>>1
		
		print "Write into "+strNode+": "+str(value),
		node.write(value)
		if node.getPermission()==uhal._core.NodePermission.READWRITE:
			l=node.read()
			hw.dispatch()
			if value==l.value():
				print " read: "+str(value)
			else:
				print " ERROR! read value: "+str(l.value())
		else:
			hw.dispatch()
			print " ok"
			
