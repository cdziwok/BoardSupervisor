#!/usr/bin/python
# Performance test for IPBus latency. Usage : perfTest <board name> <repetition number>

import timeit
import sys

board="localhost"
if len(sys.argv)>1:
  board=sys.argv[1]

nb=10000
if len(sys.argv)>2:
  nb=int(sys.argv[2])

t=timeit.Timer(setup='import uhal;manager = uhal.ConnectionManager("file:///opt/testing/trackerDAQ-3.2/CBCDAQ/BoardSupervisor/xml/connections.xml");hw = manager.getDevice("'+board+'")', 
	stmt='hw.getNode("user_wb_ttc_fmc_regs.i2c_command").read();hw.dispatch()')

output= str(nb)+" one word reading: "+str(t.timeit(number=nb))+" s\n"

t=timeit.Timer(setup='import uhal;manager = uhal.ConnectionManager("file:///opt/testing/trackerDAQ-3.2/CBCDAQ/BoardSupervisor/xml/connections.xml");hw = manager.getDevice("'+board+'")', 
	stmt='hw.getNode("user_wb_ttc_fmc_regs.i2c_command").write(999999);hw.dispatch()')

output+=str(nb)+" one word writing: "+str(t.timeit(number=nb))+" s\n"

t=timeit.Timer(setup='import uhal;manager = uhal.ConnectionManager("file:///opt/testing/trackerDAQ-3.2/CBCDAQ/BoardSupervisor/xml/connections.xml");hw = manager.getDevice("'+board+'")', 
	stmt='hw.getNode("user_wb_ttc_fmc_regs.i2c_command").write(999999);hw.dispatch();'+
		'val=hw.getNode("user_wb_ttc_fmc_regs.i2c_command").read();hw.dispatch();')

output+=str(nb)+" one word reading and writing: "+str(t.timeit(number=nb))+" s\n"

print output
