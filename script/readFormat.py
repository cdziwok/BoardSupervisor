#!/usr/bin/python

import optparse 
import sys

parser = optparse.OptionParser(usage = "%prog [options] -f <format file> <data file>\n\n"
   "Read a binary data file according to a format file\n"
   "--------------------------------\n"
   "The format file contains the following text fields separated by tabulations:\n"
   "Name   Byte	bitBeg	nbBits	Format	Repeat	Size	Since	Variable\n"
   "Each line represents a field in the data file\n"
   "Name: Field name. If it contains the sequence '%@', it will be replaced by the address of the Least Significant Byte of the field\n"
   "Byte: Byte number from the beginning of the file. If the field covers several bytes, must be the Least Significant Byte. Can be an hexadecimal if preceded by 0x. Can be followed by '^' and a number N to use little endian order over N+1 bytes\n"
   "bitBegin: bit number of the field beginning\n"
   "nbBits: Field length in bits. Can be negative (start with '-') to tell that the Most Significant byte is located before the Least Significant (Big endian). It may contain an expression and variable names\n"
   "Format: String format using Python string formatting syntax. Sequences '\\n' will be replaced by carriage returns. Can start with '%s' to print strings and '%b' to print binary values\n"
   "Repeat: The sequence of fields will be repeated N times since the field named by the Since field. Can contain a '*' to take the number from the command line -n option \n"
   "Size: Size in bits of the repeated sequence of fields in the data file\n"
   "Since: Name of the beginning field of the repeated sequence\n"
   "Variable: Name of a variable that will contain the value to be used in an nbBits expression for fields with variable length\n"
)
parser.add_option("-f", "--format", action="store", dest="format", help="Format file", metavar="FILE")
parser.add_option("-d", "--debug", action="store_true", dest="debug", help="print out debug information", default=False)
parser.add_option("-n", "--nb_records", action="store", dest="number", help="Number of times a loop is repeated if repeat fields contains '*'", default=10)
(options, args) = parser.parse_args()
if len(args)==0: 
    parser.print_help()
    sys.exit()

class Field:#Each line in file format represents a field
    def __init__(self, words):
        self.name=words[0]
        self.xor=words[1].find('^')
        if self.xor>=0:
            self.littleEnd=int(words[1][self.xor+1:])
            byte=str((int(words[1][0:self.xor]))^self.littleEnd)
        else:
            byte=words[1]
            self.littleEnd=0

        self.bitBegin='('+byte+')*8+'+words[2];
        self.nbBits=words[3]
        if self.nbBits[:1]=='-':
            self.nbBits=self.nbBits[1:]
            self.MSB=-1
        else:
            self.MSB=1

        self.fmt=words[4].strip()
        self.repeat='0'
        self.size='0'
        self.since=''
	self.variable=''
        if len(words)>7:
            if words[5]=='*':
                self.repeat=str(options.number)
            else: 
                self.repeat=words[5]
    
            self.size=words[6]
            self.since=words[7].strip()
	if len(words)>8:
            self.variable=words[8].strip() 

    def __eq__(self, other):#allows to use index to find a field in a list
        return self.name.strip()==other.name.strip()
#Repeat a sequence N times at the end of the list. 
#No need of a recursive function since nested sequences have already been repeated
def appendRepeat(lastField):
   repeat = eval(lastField.repeat, {}, mapVariable)
   for iRep in range(1, repeat):
        idxSince=len(fields)-fields[::-1].index(Field([lastField.since,"0","0","0",""]))-1#last index of Since
        iLen=len(fields)
        for idx in range(idxSince, iLen):
            record=fields[idx]
	    readFieldValue(record)
            newBegin=eval(lastField.size+'+'+record.bitBegin,{},mapVariable)
            bits=record.nbBits
            if record.MSB<=0: 
                bits='-'+bits

            if record.xor>=0:
                addedRec=Field([record.name, str((newBegin/8)^record.littleEnd)+"^"+str(record.littleEnd), 
                    str(newBegin%8), bits, record.fmt, '0', record.size, record.since, record.variable])
            else:
                addedRec=Field([record.name, str(newBegin/8), str(newBegin%8), bits, record.fmt, '0', record.size, record.since, record.variable])

            fields.append(addedRec)

        lastField=fields[-1]

def readFieldValue(record):
    iData=0    
    nbBits=eval(record.nbBits, {}, mapVariable)
    nbRemain=nbBits
    bitBegin=eval(record.bitBegin, {}, mapVariable)
    byte=bitBegin/8
    bitData=byte*8
    filData.seek(byte);
    data=filData.read(1)
    while bitData<bitBegin+nbBits and data!='':
        if options.debug and record.xor>=0: print "^",
        if bitData<=bitBegin:#read data before field
            iData = (ord(data)>>(bitBegin-bitData))&((1<<nbRemain)-1)
            nbRemain -= (bitData+8-bitBegin)
            if options.debug: print "Before: ",
        elif bitData+7<bitBegin+nbBits:#read data in field
            iData |= ord(data)<<(bitData-bitBegin)
            nbRemain-=8
            if options.debug: print "In: ",
        else:#read data begins in and ends after field
            iData |= (ord(data)&((1<<nbRemain)-1))<<(bitData-bitBegin)
            nbRemain=0
            if options.debug: print "Ends after: ",
 
        if options.debug: 
            print "data="+hex(ord(data))+", bitData="+str(bitData/8)+"*8, begin="+str(bitBegin/8)+"*8+"+str(bitBegin%8)+", nbBits="+str(nbBits)+"("+str(nbRemain)+"). iData="+(record.fmt%iData)

        if bitData+7<bitBegin+nbBits:
            if bitBegin/8<(bitBegin+nbBits)/8 and record.xor>=0:
                byte=((byte^record.littleEnd)-record.MSB)^record.littleEnd
                if options.debug: print "^ seek("+hex(byte)+")"
                filData.seek(byte)
            elif record.MSB<0:
                byte+=record.MSB
                if options.debug: print "^ seek("+str(byte)+")"
                filData.seek(max(0,byte))

            data=filData.read(1)
            bitData+=8
        else:
            break

    if len(record.variable)>0:
	mapVariable[record.variable]=iData

    return (iData, data, byte)


fields=[]
filData= open(args[0],'r')
mapVariable={}
if options.format==None:#default format
    record=Field(['%@:','0','0','32','%08X',str(options.number),'32','%@:'])
    fields.append(record)
    appendRepeat(record)
else:
    filFormat = open(options.format, 'r')
    line=filFormat.readline()
    numLineFmt=0
    #Name   Byte	bitBeg	nbBits	Format	Repeat	size	Since
    while len(line)>0:#Read format file
        numLineFmt+=1
        if line[0]=='#' or line[0]=='\n':
            line=filFormat.readline()
            continue

        words=line.split("\t")
        if len(words)<5:
            raise Exception("Incorrect number of fields at line "+str(numLineFmt)+": "+line)
        else:
	    for iWrd in range(5): 	
		if words[iWrd]=='': raise Exception("The five first fields cannot be empty at line "+str(numLineFmt))

            record=Field(words)
            fields.append(record)
	    if len(record.variable)>0:
		readFieldValue(record)

            if eval(record.repeat, {}, mapVariable)>0:
                appendRepeat(record)

        line=filFormat.readline()

    filFormat.close()

if options.debug:#debug: print out fields
    for record in fields: 
        bits=record.nbBits
        if record.MSB<=0: 
            bits='-'+bits
	if record.xor>0:
        	bitBegin=eval(record.bitBegin, {}, mapVariable)
	        print "%s\t%d (^%d)\t%d\t%s\t%s\t%s\t%s"%(record.name,bitBegin/8,record.littleEnd,bitBegin%8, bits, record.fmt, record.repeat, record.variable)
	else:
	        print "%s\t%s\t%s\t%s\t%s\t%s"%(record.name,record.bitBegin, bits, record.fmt, record.repeat, record.variable)
	
for record in fields:#Main loop: read value for each field
    (iData, data, byte)=readFieldValue(record)

    if record.MSB<0:    
            byte+=1
    ########### Print out value ###################
    strName=record.name.replace('%@','%07X'%byte)
    print strName+"\t",
    if record.fmt[0:2]=='%:':#Enumerated values
        arrMap=[]
        for enum in record.fmt[2:].split(','):
            keyVal=enum.split(':')
            arrMap.append((keyVal[0], keyVal[1]))

        strKey="%d"%iData
        dicVal=dict(arrMap)
        if strKey in dicVal:#Enum key found
            print dicVal[strKey]
        else:#No key found
            print strKey
    elif record.fmt[0:2]=='%s':#String value
	strVal=""
	iCar=eval(record.nbBits, {}, mapVariable)/8
	while iCar>0:
		strVal+=chr(iData&255)
		iData>>=8
		iCar-=1

	print record.fmt.replace('\\n','\n')%strVal
    elif '%b' in record.fmt:#Binary value
	strVal=bin(iData)[2:] #remove "0b"
	nbBits=eval(record.nbBits, {}, mapVariable)
	for iCar in range(len(strVal), nbBits): strVal="0"+strVal #Left padding with 0s
	print record.fmt.replace("%b","%s").replace('\\n','\n')%("0b"+strVal)
    else:#Print value using format
        print record.fmt.replace('\\n','\n')%iData

    if data=='': 
       break

filData.close()
