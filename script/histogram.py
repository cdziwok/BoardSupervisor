#!/usr/bin/python

import sys
import optparse 
parser = optparse.OptionParser(usage = "%prog [options] <input file>\n\nGenerate an HTML 5 histogram from file or standard input if file is '-'")
parser.add_option("-m", "--min", action="store", dest="min", help="minimum value (inclusive)", default="0")
parser.add_option("-x", "--max", action="store", dest="max", help="maximum value (exclusive)", default="255")
parser.add_option("-n", "--number", action="store", dest="number", help="number of bins", default="256")
(options, args) = parser.parse_args()
if len(args)==0: 
    parser.print_help()
    sys.exit()

filContent= open(args[0],'r') if args[0]!='-' else sys.stdin
line=filContent.readline().strip()
xMin= int(options.min, 16 if options.min[0:2]=='0x' else 10) 
xMax= int(options.max, 16 if options.max[0:2]=='0x' else 10)
nbBin=int(options.number, 16 if options.number[0:2]=='0x' else 10)
data=[0]*nbBin
while len(line)>0:#Read file
	val = int(line, 16 if line[0:2]=='0x' else 10)
	data[int((val-xMin) * nbBin / (xMax - xMin))]+=1
        line=filContent.readline().strip()

yMax=0
for uVal in range(nbBin):
	if yMax<data[uVal]:
		yMax=data[uVal]

print '<!DOCTYPE html>'
print '<html><body>';
print "<table><tr><td valign='top'>", yMax, "</td><td>"
print '<canvas id="cnvDataVisu" width="1024" height="682" title="" style="border:1px solid black;" onclick=""> Your browser does not support the HTML5 canvas tag.</canvas>'
print '<script> var data=['
for uVal in range(nbBin):
	print data[uVal],",",

print "];"
print "displayData();"
print "function displayData(){ var c = document.getElementById('cnvDataVisu');"
print "var ctx = c.getContext('2d');"
print "ctx.fillStyle='blue';" 
print "for (col=0; col<data.length; col++)"
print "ctx.fillRect(col*c.width/data.length,(",yMax,"-data[col])*c.height/",yMax,", c.width/data.length-1,c.height);"
print "}</script>"
print "</td></tr>"
print "<tr><td align='right'>",xMin,"</td><td align='right'>",xMax-1,"</td></tr>"
print "</table></body></html>"

filContent.close()
