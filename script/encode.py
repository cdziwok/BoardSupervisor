#!/usr/bin/python
# coding: utf8 
import optparse 
import sys
import encodings
import codecs

parser = optparse.OptionParser(usage = "%prog [options] <input file>\n\n"
   "Convert from one encoding into another\n"
   "--------------------------------\n"
   "Under Linux you can also try iconv\n"
)
parser.add_option("-l", "--list", action="store_true", dest="list", help="Print the list of all encodings then exit", default=False)
parser.add_option("-i", "--input", action="store", dest="input", help="Input file encoding", default="utf8")
parser.add_option("-o", "--output", action="store", dest="output", help="Encoding of the output", default="utf8")
parser.add_option("-a", "--all", action="store_true", dest="tryAll", help="Try opening the file with all encodings", default=False)
parser.add_option("-c", "--chars", action="store", dest="chars", help="special or accentuated characters to be present in file when trying all encodings", default="")

(options, args) = parser.parse_args()
setEncodings=sorted(set(encodings.aliases.aliases.values()))
if options.list:
	print ''.join('- ' + e + '\n' for e in setEncodings)	
	sys.exit()

if len(args)==0: 
    parser.print_help()
    sys.exit()

if options.tryAll:
	for e in setEncodings:
                try:
                    fh = codecs.open(args[0], 'r', encoding=e)
                    lst=fh.readlines()
		    if len(options.chars)>0:
			found=False
			for line in lst:
				for ch in options.chars.decode('utf8'):
					if line.find(ch)>=0:
						found=ch
						break

				if found: break
				

                except Exception:
			pass
                else:
		    if len(options.chars)==0:
			print('File successfully opened with encoding: %s ' % e)
		    elif found: 	
			print('File successfully opened and char %s found with encoding: %s' %(found, e) )
						
		finally:
                    fh.close()

	sys.exit()

f=codecs.open(args[0],encoding=options.input)
for line in f:
	print line.encode(options.output),

f.close()

