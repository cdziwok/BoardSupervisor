#!/usr/bin/python
import sys
#if sys.version_info[0:2] != (2, 4): raise Exception('requires python 2.4')
import uhal
from time import sleep

manager = uhal.ConnectionManager("file://${CBCDAQ}/BoardSupervisor/xml/connections.xml")
board = manager.getDevice("board0") 
board.getNode("cbc_i2c_cmd_rq").write(0)
for iEn in range(0,1000000):
	lVal = board.getNode("cbc_i2c_cmd_rq").read()#user_wb_ttc_fmc_regs.cbc_reg_i2c_settings
	board.dispatch()
	if lVal.value()!=0:
		print "READ request" if lVal.value()==1 else "WRITE request"
		board.getNode("cbc_i2c_cmd_ack").write(1)
		arrReq=board.getNode("sram1").readBlock(512)
		board.dispatch()
		while True:
			lVal = board.getNode("cbc_i2c_cmd_rq").read()
			board.dispatch() 
			if lVal.value()==0: break

		print lVal.value()
		print "\t".join(map(hex, arrReq.value()))
		board.getNode("cbc_i2c_cmd_ack").write(0)
		board.dispatch()

	sleep(0.01)
	sys.stdout.write(str(iEn)+'\r')
	sys.stdout.flush()


'''
		SelectFeSRAM( pFeId );
		EnableI2c( 1 );

		try
		{
			SendBlockCbcI2cRequest( pVecReq, false );
			WriteReg( fStrSramUserLogic, 1 );

			pVecReq.push_back( 0xFFFFFFFF );

			WriteReg( fStrSramUserLogic, 0 );

			WriteBlockReg( fStrSram, pVecReq );
			WriteReg( fStrOtherSram, 0xFFFFFFFF );

			WriteReg( fStrSramUserLogic, 1 );

			WriteReg( CBC_HARD_RESET, 0 );

		//r/w request
			WriteReg( CBC_I2C_CMD_RQ, pWrite ? 3 : 1 );

			pVecReq.pop_back();

			if ( I2cCmdAckWait( ( uint32_t )1, pVecReq.size() ) == 0 ) throw Exception( "CbcInterface: I2cCmdAckWait 1 failed." );

			WriteReg( CBC_I2C_CMD_RQ, 0 );

			if ( I2cCmdAckWait( ( uint32_t )0, pVecReq.size() ) == 0 ) throw Exception( "CbcInterface: I2cCmdAckWait 0 failed." );
		}

		catch ( Exception& e )
		{
			throw e;
		}

		ReadI2cBlockValuesInSRAM( pVecReq );

		EnableI2c( 0 );
'''
