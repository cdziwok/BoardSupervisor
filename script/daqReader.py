#!/usr/bin/python

import optparse 
import sys

parser = optparse.OptionParser(usage = "%prog [options] <new daq file>\n\n"
   "Read a binary Phase-2 tracker data file written by the GlibStreamer XDAQ package\n"
   "--------------------------------\n"
   "The file format consists of the event size over 4 bytes followed by the event data whose format is described at:\n"
   "https://cms-docdb.cern.ch/cgi-bin/DocDB/ShowDocument?docid=12091\n"
)
parser.add_option("-c", "--condition", action="store_true", dest="conditionData", help="print out condition data", default=False)
parser.add_option("-m", "--memdump", action="store_true", dest="memoryDump", help="memory dump from GlibStreamer", default=False)
(options, args) = parser.parse_args()
if len(args)==0: 
    parser.print_help()
    sys.exit()


condType=range(256)
condType[1]="I2C";condType[3]="TDC";condType[4]="Angle";condType[5]="HV";condType[6]="Error bits";condType[7]="Status bits";condType[9]="Stub latency";
condType[10]="Bunch counter";condType[11]="Orbit counter";condType[12]="Lumisection";
regName=["FrontEndControl", "TriggerLatency", "HitDetectSLVS", "Ipre1", "Ipre2", "Ipsf", "Ipa", "Ipaos", "Vpafb", "Icomp", "Vpc", "Vplus", "VCth", "TestPulsePot", "SelTestPulseDel&ChanGroup", "MiscTestPulseCtrl&AnalogMux", "TestPulseChargePumpCurrent", "TestPulseChargeMirrCascodeVolt", "", "", "", "", "", "", "CwdWindow&Coincid", "MiscStubLogic", "", "", "", "", "", "", "MaskChannelFrom008downto001", "MaskChannelFrom016downto009", "MaskChannelFrom024downto017", "MaskChannelFrom032downto025", "MaskChannelFrom040downto033", "MaskChannelFrom048downto041", "MaskChannelFrom056downto049", "MaskChannelFrom064downto057", "MaskChannelFrom072downto065", "MaskChannelFrom080downto073", "MaskChannelFrom088downto081", "MaskChannelFrom096downto089", "MaskChannelFrom104downto097", "MaskChannelFrom112downto105", "MaskChannelFrom120downto113", "MaskChannelFrom128downto121", "MaskChannelFrom136downto129", "MaskChannelFrom144downto137", "MaskChannelFrom152downto145", "MaskChannelFrom160downto153", "MaskChannelFrom168downto161", "MaskChannelFrom176downto169", "MaskChannelFrom184downto177", "MaskChannelFrom192downto185", "MaskChannelFrom200downto193", "MaskChannelFrom208downto201", "MaskChannelFrom216downto209", "MaskChannelFrom224downto217", "MaskChannelFrom232downto225", "MaskChannelFrom240downto233", "MaskChannelFrom248downto241", "MaskChannelFrom254downto249", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "Channel001", "Channel002", "Channel003", "Channel004", "Channel005", "Channel006", "Channel007", "Channel008", "Channel009", "Channel010", "Channel011", "Channel012", "Channel013", "Channel014", "Channel015", "Channel016", "Channel017", "Channel018", "Channel019", "Channel020", "Channel021", "Channel022", "Channel023", "Channel024", "Channel025", "Channel026", "Channel027", "Channel028", "Channel029", "Channel030", "Channel031", "Channel032", "Channel033", "Channel034", "Channel035", "Channel036", "Channel037", "Channel038", "Channel039", "Channel040", "Channel041", "Channel042", "Channel043", "Channel044", "Channel045", "Channel046", "Channel047", "Channel048", "Channel049", "Channel050", "Channel051", "Channel052", "Channel053", "Channel054", "Channel055", "Channel056", "Channel057", "Channel058", "Channel059", "Channel060", "Channel061", "Channel062", "Channel063", "Channel064", "Channel065", "Channel066", "Channel067", "Channel068", "Channel069", "Channel070", "Channel071", "Channel072", "Channel073", "Channel074", "Channel075", "Channel076", "Channel077", "Channel078", "Channel079", "Channel080", "Channel081", "Channel082", "Channel083", "Channel084", "Channel085", "Channel086", "Channel087", "Channel088", "Channel089", "Channel090", "Channel091", "Channel092", "Channel093", "Channel094", "Channel095", "Channel096", "Channel097", "Channel098", "Channel099", "Channel100", "Channel101", "Channel102", "Channel103", "Channel104", "Channel105", "Channel106", "Channel107", "Channel108", "Channel109", "Channel110", "Channel111", "Channel112", "Channel113", "Channel114", "Channel115", "Channel116", "Channel117", "Channel118", "Channel119", "Channel120", "Channel121", "Channel122", "Channel123", "Channel124", "Channel125", "Channel126", "Channel127", "Channel128", "Channel129", "Channel130", "Channel131", "Channel132", "Channel133", "Channel134", "Channel135", "Channel136", "Channel137", "Channel138", "Channel139", "Channel140", "Channel141", "Channel142", "Channel143", "Channel144", "Channel145", "Channel146", "Channel147", "Channel148", "Channel149", "Channel150", "Channel151", "Channel152", "Channel153", "Channel154", "Channel155", "Channel156", "Channel157", "Channel158", "Channel159", "Channel160", "Channel161", "Channel162", "Channel163", "Channel164", "Channel165", "Channel166", "Channel167", "Channel168", "Channel169", "Channel170", "Channel171", "Channel172", "Channel173", "Channel174", "Channel175", "Channel176", "Channel177", "Channel178", "Channel179", "Channel180", "Channel181", "Channel182", "Channel183", "Channel184", "Channel185", "Channel186", "Channel187", "Channel188", "Channel189", "Channel190", "Channel191", "Channel192", "Channel193", "Channel194", "Channel195", "Channel196", "Channel197", "Channel198", "Channel199", "Channel200", "Channel201", "Channel202", "Channel203", "Channel204", "Channel205", "Channel206", "Channel207", "Channel208", "Channel209", "Channel210", "Channel211", "Channel212", "Channel213", "Channel214", "Channel215", "Channel216", "Channel217", "Channel218", "Channel219", "Channel220", "Channel221", "Channel222", "Channel223", "Channel224", "Channel225", "Channel226", "Channel227", "Channel228", "Channel229", "Channel230", "Channel231", "Channel232", "Channel233", "Channel234", "Channel235", "Channel236", "Channel237", "Channel238", "Channel239", "Channel240", "Channel241", "Channel242", "Channel243", "Channel244", "Channel245", "Channel246", "Channel247", "Channel248", "Channel249", "Channel250", "Channel251", "Channel252", "Channel253", "Channel254", "ChannelDummy"]
filData= open(args[0],'r')
if options.memoryDump:
	filData.seek(16)
	strSize=filData.read(2)+chr(0)+chr(0)
	index=24
else:
	strSize=filData.read(4)
	index=4

event=0
while len(strSize)==4:
	size=0
	nbCbcReturn=0
	event+=1
	msgError=""
	for iByte in range(4):#event size
		size+= ord(strSize[iByte])<<(iByte*8)

	print "------------------ Index:",index,"\tEvent size:",size,"\tEvent #",event,
	if options.memoryDump:
		msgEvt=0
		filData.seek(index-20)
		strNum=filData.read(4)
		for iByte in range(4):#Message event number from memory dump
			msgEvt+= ord(strNum[iByte])<<(iByte*8)

		print "\tMessage event #", msgEvt
		if msgEvt!=event:
			msgError+="Message event # is "+str(msgEvt)+" but expected "+str(event)+"! "
	else:
		print
	############ Tracker header ####################
	filData.seek(index+8)#FE status MSB
	val=ord(filData.read(1))
	feStatusMsb=""
	for iBit in range(7,-1,-1):
		feStatusMsb= str((val>>iBit) & 1) + feStatusMsb

	nbCbc=ord(filData.read(1)) + (ord(filData.read(1))<<8)
	filData.seek(index+8+6)
	val0=ord(filData.read(1))
	val1=ord(filData.read(1))
	evtType=(val0 & 0xC0)>>6 | (val1 & 0x03)<<2
	mode= (val1 & 0x0C)>>2
	fmtVersion= (val1 & 0xF0)>>4
	print "Format version:",fmtVersion,", Mode:",["Summary","Full debug","Error","?"][mode],
	print ", Event type:",["Sparsified","Unsparsified"][(evtType&0b1000)>>3],["Fake","Real"][evtType&1],["without","with"][(evtType&0b10)>>1],"condition data, nb CBC=",nbCbc
	feStatus=""
	for iByte in range(8): #FE status
		val=ord(filData.read(1))
		for iBit in range(8):
			feStatus=str((val>>iBit) & 1) + feStatus

	feStatus=feStatusMsb+feStatus
	print "FE status: "+feStatus	
	headerSize=16#Summary mode, Tracker header minimum size
	if mode==1:#Full debug mode
		headerSize+=(10*nbCbc+7)/8
	elif mode==2:#Error mode
		headerSize+=(2*nbCbc+7)/8

	headerSize=(headerSize+7)/8 * 8#padding over 8 bytes
	nbFE=0
	for cFE in feStatus:#Count number of FEs
		if cFE=='1': nbFE+=1
	##################### Payload ##########################
	if (evtType & 0b1000)>0:#Unsparsified
		indexFE=0
		payloadSize=2*nbFE+32*nbCbc #CBC status: 16 bits = 2 Bytes, CBC strips: 256 bits = 32 Bytes
		for iFE in range(nbFE):#for each FE data in payload
			filData.seek(index+headerSize+indexFE+8+6)#DAQ header = 8, 2 first bytes in little endian 8 = 6
			cbcStatus=ord(filData.read(1))+ (ord(filData.read(1))<<8)
			nbCbcFE=0
			for iCbc in range(16):#Count the numer of CBCs returning data
				nbCbcFE+= (cbcStatus>>iCbc)&1

			indexFE+= 2+32*nbCbcFE 
			nbCbcReturn+= nbCbcFE
		
	else:	#Sparsified: <type 0=2S, 1=PS><#P cluster 6b><#S cluster 6b> FEDCBA9876543210
		bitFE=0
		for iFE in range(nbFE):#for each FE data in payload
			filData.seek(index+((8+headerSize+bitFE/8)^7))#DAQ header = 8
			clusterHdr=ord(filData.read(1))<<8
			filData.seek(index+((8+headerSize+bitFE/8 + 1)^7))#DAQ header = 8
			clusterHdr+= ord(filData.read(1))
			nbCluster= (clusterHdr>>(16-bitFE%8-1-6) & 0x3F)

			bitFE+= 7 + nbCluster *15 #Module 2S: cluster header=7, cluster size=15
			print "FE",iFE,":",nbCluster,"cl-",

		payloadSize=(bitFE+7)/8

	payloadSize+=4*8#Stub data
	payloadSize=(payloadSize+7)/8 * 8#padding over 8 bytes
	################## Condition data #####################
	conditionSize=0
	nbCondition=0
	if (evtType & 0b0010)>0:#Condition data present
		filData.seek(index+headerSize+payloadSize+8)
		for iByte in range(8):#Number of condition data
			nbCondition += ord(filData.read(1))<<(iByte*8)

		conditionSize=8+nbCondition*8

	print nbFE,"FE, header size:",headerSize,", payload size:",payloadSize,",",
	if conditionSize==0:
		print "No condition data",
	else:
		print conditionSize/8-1, "condition data",
	################# Size verification ###################
	computedSize=headerSize+payloadSize+conditionSize+16
	print "(size=",conditionSize,") computed size:",computedSize
	if (evtType & 0b1000)>0 and nbCbcReturn!=nbCbc: #Number of CBC returning data must match the total number of CBCs (Unsparsified)
		msgError+=str(nbCbc)+" CBC but "+str(nbCbcReturn)+" returning data! "

	if size!=computedSize: 	msgError+="Event size is "+str(size)+" but computed size is "+str(computedSize)+"! " 
	if mode==3: 		msgError+="Unexpected mode: "+str(mode)+"! "
	if fmtVersion!=2: 	msgError+="Unexpected format version: "+str(fmtVersion)+"! "

	if options.conditionData and nbCondition>0:
		filData.seek(index+headerSize+payloadSize+8+8)
		for iCond in range(nbCondition):
			arrCond=filData.read(8)
			#print "Condition data", iCond+1,": FE",ord(arrCond[0^7]),"CBC",(ord(arrCond[1^7])&0x0F),"page",(ord(arrCond[1^7])>>4),"register",hex(ord(arrCond[2^7])),
			print "Condition data", iCond+1,
			if ord(arrCond[3^7])==1:#I2C
				print ": FE",ord(arrCond[0^7]),"CBC",(ord(arrCond[1^7])&0x0F),"register",regName[(ord(arrCond[1^7])>>4)*256+ord(arrCond[2^7])],
			print "type",condType[ord(arrCond[3^7])],"value:",hex(ord(arrCond[7^7])+(ord(arrCond[6^7])<<8)+(ord(arrCond[5^7])<<16)+(ord(arrCond[4^7])<<24)),
			print "key:",hex(ord(arrCond[0^7])&255 | (ord(arrCond[1^7])&255)<<8 | (ord(arrCond[2^7])&255)<<16 | (ord(arrCond[3^7])&255)<<24)

	if len(msgError)>0:
		print "ERROR:",msgError

	if options.memoryDump:
		index+=size+64+16
		filData.seek(index)
		strSize=filData.read(2)+chr(0)+chr(0)
		index+=8
	else:
		index+=size
		filData.seek(index)
		strSize=filData.read(4)
		index+=4
	
filData.close()
