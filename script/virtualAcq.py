#!/usr/bin/python
import sys
import os
#if sys.version_info[0:2] != (2, 4): raise Exception('requires python 2.4')
import uhal #requires python2.4
from time import sleep

manager = uhal.ConnectionManager("file://"+os.environ['CBCDAQ']+"/BoardSupervisor/xml/connections.xml")
board = manager.getDevice("board2") 

lVal = board.getNode("pc_commands.CBC_DATA_PACKET_NUMBER").read()
board.getNode("status_flags.CMD_START_VALID").write(1);
board.dispatch()
nbPack = lVal.value()+1;
packetSize = 42
print "Nb of packets: ",nbPack;
#board.getNode("sram1").writeBlock([0,0,0,0,0,
#				0xAAAA, 0xAAAA, 0xAAAA, 0xAAAA, 0xAAAA, 0xAAAA, 0xAAAA, 0xAAAA, 0xAAAA,
#				0x5555, 0x5555, 0x5555, 0x5555, 0x5555, 0x5555, 0x5555, 0x5555, 0x5555, 
#				0xAAAA, 0xAAAA, 0xAAAA, 0xAAAA, 0xAAAA, 0xAAAA, 0xAAAA, 0xAAAA, 0xAAAA,
#				0x5555, 0x5555, 0x5555, 0x5555, 0x5555, 0x5555, 0x5555, 0x5555, 0x5555, 
#				0x06])
#board.getNode("sram2").writeBlock([0,0,0,0,1,
#				0xAAAA, 0xAAAA, 0xAAAA, 0xAAAA, 0xAAAA, 0xAAAA, 0xAAAA, 0xAAAA, 0xAAAA,
#				0x5555, 0x5555, 0x5555, 0x5555, 0x5555, 0x5555, 0x5555, 0x5555, 0x5555, 
#				0xAAAA, 0xAAAA, 0xAAAA, 0xAAAA, 0xAAAA, 0xAAAA, 0xAAAA, 0xAAAA, 0xAAAA,
#				0x5555, 0x5555, 0x5555, 0x5555, 0x5555, 0x5555, 0x5555, 0x5555, 0x5555, 
#				0x0F])
for iEn in range(0,32768):
#	board.getNode("sram1").writeBlock( range(iEn*2*nbPack,nbPack*(iEn*2+packetSize)) )
#		print str(iEn)+": "+str(iEn*2*nbPack)+"->"+str(nbPack*(iEn*2+packetSize))+"\n"
	board.getNode("flags.SRAM1_full").write(0)
	board.dispatch();
	sleep(0.09);
	board.getNode("flags.SRAM1_full").write(1)
#	board.getNode("sram2").writeBlock( range(nbPack*(iEn*2+1),nbPack*(iEn*2+1+packetSize)) )
#		print str(iEn)+": "+str((iEn*2+1)*nbPack)+"->"+str(nbPack*(iEn*2+1+packetSize))+"\n"
	board.getNode("flags.SRAM2_full").write(0)
	board.dispatch();
	sleep(0.09);
	board.getNode("flags.SRAM2_full").write(1)
	board.dispatch();
	sys.stdout.write(str(iEn)+'\r')
	sys.stdout.flush()

