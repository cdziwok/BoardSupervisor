/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "BoardSupervisorV.h"
#include "config/version.h"
#include "xcept/version.h"
#include "xdaq/version.h"


GETPACKAGEINFO(BoardSupervisor)

void BoardSupervisor::checkPackageDependencies() throw (config::PackageInfo::VersionException)
{
    CHECKDEPENDENCY(config)
    CHECKDEPENDENCY(xcept)
    CHECKDEPENDENCY(xdaq)
}

std::set<std::string, std::less<std::string> > BoardSupervisor::getPackageDependencies()
{
    std::set<std::string, std::less<std::string> > dependencies;
    ADDDEPENDENCY(dependencies,config);
    ADDDEPENDENCY(dependencies,xcept);
    ADDDEPENDENCY(dependencies,xdaq);
    return dependencies;
}	

