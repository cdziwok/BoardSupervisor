#include "ShmCollector.h"
#include "toolbox/fsm/FailedEvent.h"
//#include "xdaqTrackerDefs.h"
#include "interface/shared/frl_header.h"
#include "interface/shared/fed_header.h"
#include "interface/shared/fed_trailer.h"
#include "LDAShare.h"
typedef unsigned int u32; 
using namespace xdaq;

XDAQ_INSTANTIATE(ShmCollector)
ShmCollector::ShmCollector (xdaq::ApplicationStub* stub) : TrackerCommandSender(stub), FRLA(8),JsInterface(stub),logger_(getApplicationLogger())
{

  getApplicationInfoSpace()->fireItemAvailable("stateName",&fsm_.stateName_);
  nPurged_=0;

  ruDescriptor_=NULL;
  ruName_ = "rubuilder::ru::Application";
  ruInstance_=-1;
  count_ = 0;
  preAllocated_ = 5;
  continuous_=true;
  theEventDriven_=false;
  theNumberOfDIF_=1;
  nbOfBufferedTrigger_=500;
  getApplicationInfoSpace()->fireItemAvailable("nbOfBufferedTrigger",&nbOfBufferedTrigger_);
  getApplicationInfoSpace()->fireItemAvailable("theNumberOfDIF",&theNumberOfDIF_);

 
  usePolling_ = false;
  getApplicationInfoSpace()->fireItemAvailable("UsePolling",&usePolling_);
  getApplicationInfoSpace()->fireItemAvailable("RU",&ruInstance_);
  getApplicationInfoSpace()->fireItemAvailable("RUName",&ruName_);

  getApplicationInfoSpace()->fireItemAvailable("count",&count_);
  getApplicationInfoSpace()->fireItemAvailable("preAllocated",&preAllocated_);
  getApplicationInfoSpace()->fireItemAvailable("continuePushing",&continuous_);
  getApplicationInfoSpace()->fireItemAvailable("EventDriven",&theEventDriven_);


  triggerPaused_ = false;
  getApplicationInfoSpace()->fireItemAvailable("triggerPause",&triggerPaused_);


  // Add infospace listeners for exporting data values
  getApplicationInfoSpace()->addItemRetrieveListener ("count", this);


  // Define FSM
  fsm_.addState ('H', "Halted");
  fsm_.addState ('C', "Configured");
  fsm_.addState ('E', "Enabled");
  fsm_.addStateTransition ('H','C', "Configure", this, &ShmCollector::ConfigureAction);
  fsm_.addStateTransition ('C','E', "Enable", this, &ShmCollector::EnableAction);
  fsm_.addStateTransition ('E','H', "Halt", this, &ShmCollector::HaltAction);
  fsm_.addStateTransition ('E','C', "Stop", this, &ShmCollector::HaltAction);
  fsm_.addStateTransition ('C','H', "Halt", this, &ShmCollector::HaltAction);
  fsm_.setFailedStateTransitionAction( this, &ShmCollector::failedTransition );

  fsm_.setInitialState('H');
  fsm_.reset();

 





  // Bind SOAP callbacks for control messages
  xoap::bind (this, &ShmCollector::fireEvent, "Configure", XDAQ_NS_URI);
  xoap::bind (this, &ShmCollector::fireEvent, "Enable", XDAQ_NS_URI);
  xoap::bind (this, &ShmCollector::fireEvent, "Halt", XDAQ_NS_URI);
  xoap::bind (this, &ShmCollector::fireEvent, "Stop", XDAQ_NS_URI);


  xoap::bind (this, &ShmCollector::fireEvent, "Reset", XDAQ_NS_URI);
 
  // Bind CGI callbacks
  lastpage_="StateMachine";
  xgi::bind(this, &ShmCollector::dispatch, "dispatch");
  xgi::bind(this, &ShmCollector::displayStateMachine, "displayStateMachine");
  xgi::bind(this, &ShmCollector::displayParameters, "displayParameters");

  lastGtc_=0;
 
  memset(difID_,0,128*sizeof(uint32_t));
  memset(DCCSlot_,0,128*sizeof(uint32_t));
  memset(LDASlot_,0,128*sizeof(uint32_t));
}

void ShmCollector::initDescriptors()
{

  // Set the RU instance if it is not yet set
  if (ruInstance_.value_<0)  ruInstance_ = getApplicationDescriptor()->getInstance();
  //diagService_->reportError(toolbox::toString("Ru Instance is %d\n",ruInstance_.value_),DIAGUSERINFO);
  if (usePolling_.value_) return;
}

void ShmCollector::createVectorOfDescriptors(std::vector<xdaq::ApplicationDescriptor*> &v,std::string name)
{
  std::set<xdaq::ApplicationDescriptor*> s;
  try 
    {

      s = getApplicationContext()->getDefaultZone()->getApplicationDescriptors(name);
      
      //diagService_->reportError(toolbox::toString("%d %s descriptors found \n",s.size(),name.c_str()),DIAGUSERINFO);
    }
  catch (xdaq::exception::Exception& e)
    {
      //diagService_->reportError(toolbox::toString("No %s descriptors found \n",name.c_str()),DIAGUSERINFO);
      return;
    }

  std::set<xdaq::ApplicationDescriptor*>::iterator i;
  for (i=s.begin();i!=s.end();i++)
    {
      xdaq::ApplicationDescriptor* a =dynamic_cast<xdaq::ApplicationDescriptor*> (*i);
      v.push_back(a);
      //      cout << "Pushing a new descriptor" << endl;
    }
  // cout << "Total size" << v.size() << endl;
  return;

}



void ShmCollector::ConfigureAction(toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception)
{


  memset(lastEvent_,0,128*sizeof(int));

  // Check descriptors
  initDescriptors();
  // prepare Fed Size

  // Get RU client descriptor
  if (ruInstance_.value_>=0)
    ruDescriptor_=getApplicationContext()->getDefaultZone()->getApplicationDescriptor(ruName_,ruInstance_.value_);
  else
    {

      LOG4CPLUS_FATAL(logger_,toolbox::toString(" %s :No RU Instance %d \n",__PRETTY_FUNCTION__ ,ruInstance_.value_));
    }

 
  nbook_=0;
  clearBuffers ();
  // Initialize event sent
  nsent_ =0;

  theFifoManager_ = new FifoManager(false,true,theNumberOfDIF_.value_);
  theFifoManager_->initialise();
  theFifoManager_->configure();
}


void ShmCollector::ConfigurePolling()
{

  std::cout<<"Polling workloop creation"<<std::endl;
 
  asPolling_   = toolbox::task::bind(this,&ShmCollector::serviceShmPolling,  "polling");
  // work loops
  workLoopPolling_ =
    toolbox::task::getWorkLoopFactory()->getWorkLoop("ShmCollectorShmPolling","polling");
  
  workLoopPolling_->submit(asPolling_);
  
  if (!workLoopPolling_->isActive()) workLoopPolling_->activate(); 
}
bool ShmCollector::serviceShmPolling(toolbox::task::WorkLoop* wl)
{
  if (fsm_.getStateName(fsm_.getCurrentState()).compare("Enabled") !=0 || theEventDriven_.value_)
    {
      usleep(10);
      // std::cout<<"Monitor Task is stopped " <<std::endl;
      return true;
    }

  
  this->flagSlots();

  



  // Now loop on all Share Memories

  //
  if (vGTC_.size()>0)
    std::cout <<  vGTC_.size() <<" Events are ready" <<std::endl;
  // Format the event and transfer it
  for (unsigned int idx=0;idx<vGTC_.size();idx++)
    {
      //std::cout<<" Pushing "<<vGTC_[idx]<<std::endl;
      this->pushEvent(vGTC_[idx]);
    }  // Transfer it
  this->purgeSlots();
  usleep(100);
  return true;
}
#undef DEBUG
void ShmCollector::EnableAction(toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception)
{
  eventNumber_=1; //LM patch 14 Nov 2007
  nsent_=0;
  if (usePolling_.value_)
    ConfigurePolling();
  theFifoManager_->start();
  std::cout<<"End of ShmCollector Enable"<<std::endl;
}



void ShmCollector::HaltAction(toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception)
{
  std::cout << "ShmCollector is halting"<<std::endl;


  std::cout << "Monitor is stopped "<<std::endl;
  clearBuffers();
  std::cout << "ShmCollector is halted "<<std::endl;

}
	




void ShmCollector::sendBuffer (toolbox::mem::Reference* channelRef)
{
  
  //diagService_->reportError(toolbox::toString(" J'envoie a la RU %d  l evt %d \n",ruInstance_.value_,eventNumber_),DIAGINFO);
  // Up
  updateEventNumber(channelRef,eventNumber_);
  forwardBuffer(this,channelRef,ruDescriptor_);
  nbook_--;
  eventNumber_++;
  nsent_++;
  return;
}





toolbox::mem::Reference* ShmCollector::senderTrackerCommandHandler(PI2O_TRACKER_COMMAND_MESSAGE_FRAME com)
{
  if ((com->System != SYST_TRACKER)  || (com->SubSystem != SUB_SYST_SENDER ) )  return NULL;
  //    printf("I GOT AMESSAGE in TSCSupervisor\n");
  U32 command = com->Command;
  U32 data = com->data[0];
  // U32 error=0;
  //   U32 ndata=1;
  // do something
  toolbox::mem::Reference* ackRef = allocateTrackerCommand(2);
  PI2O_TRACKER_COMMAND_MESSAGE_FRAME reply = (PI2O_TRACKER_COMMAND_MESSAGE_FRAME)ackRef->getDataLocation();
  reply->Error=0;
  switch (command)
    {
      
    case SENDER_GET_LIST_OF_BCID_READY:
      {
	ackRef->release();
	//	std::cout<<"I got a message SENDER_GET_LIST_OF_SLOTS_READY" <<std::endl; 


	this->flagSlots();
	
	uint32_t ntrg=vGTC_.size();
	
	theLastGTCSize_=ntrg;

	ackRef = allocateTrackerCommand(2+ntrg*2);
	reply = (PI2O_TRACKER_COMMAND_MESSAGE_FRAME)ackRef->getDataLocation();
	reply->Error=vGTC_.size();
	//std::cout<<ntrg<<" slot founds"<<std::endl;
	//if (ntrg>0) std::cout<< "request received" <<ntrg<< " "<<gtcFound[0]<<std::endl;
	reply->data[0]=ntrg;
	if (ntrg>0)
	  for (uint32_t i=0;i<ntrg;)
	    {
	      reply->data[i+1]=vGTC_[i] & BITS24;
	      reply->data[i+2]=(vGTC_[i]/SHIFT24)& BITS24;
	      i+=2;
	    }

      }
      break;

    case SENDER_PUSH_BCID:  
      {
	uint64_t abcidlow= com->data[0];
	uint64_t abcidhigh= com->data[1];
	uint64_t abcid= abcidlow+SHIFT24*abcidhigh;
	std::cout << " L emonsieur te demande "<<abcidlow<<" "<<abcidhigh<<" soit "<<abcid<<std::endl;
	lastGtc_=abcid;
	pushEvent(abcid);
	purgeSlot(abcid);
      }
      break;
    case SENDER_PURGE_SLOT:  
      {
	purgeSlots();
      }
      break;
    case SENDER_PREALLOCATE_BUFFER:
      {

	for (unsigned int ib = 0; ib < data; ib++)
	  {

	    toolbox::mem::Reference* ref = allocateDataBuffer();
	    forwardBuffer (this,ref, nextDescriptor_);
	  }
      }
      break;

    case SENDER_SEND_BUFFER:
      {
	int nbuff = data;
		  
		  
	while (hasBuffers () && nbuff > 0)
	  {
	    toolbox::mem::Reference* ref1 = useBuffer ();
	    sendBuffer (ref1);
	    nbuff--;
	  }
	
	clearBuffers();
	//diagService_->reportError(toolbox::toString("%d sent %d clear %d \n",data,getNumberOfBuffers(),nbook_),DIAGDEBUG);
	reply->data[0] = nbuff;

      }
      break;
    case SENDER_CLEAR_BUFFER:
      {
	nsent_=0;
	reply->data[0]=getNumberOfBuffers();
	clearBuffers();
      }
      break;
    case SENDER_GET_STORED:
      {

	eventTrigger_=data;
       	//std::cout << "ShmCollector::"<<data<<" "<<eventTrigger_<<std::endl;
	if (continuous_)
	  reply->data[0]=nsent_;
	else
	  reply->data[0]=nsent_;
      }
      break;

  
    case DBA_GET_NEXT_DBA_CMD:
      {
	reply->Error =1;
      }
      break;
    case DBA_SET_NEXT_DBA_CMD:
      {

	reply->Error =1;
      }
      break;

    default:
      break;


    }

  
 
  return ackRef;
	
	
}


// XGI Call back

void ShmCollector::Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{



  if (lastpage_ == "Parameters" )
    this->displayParameters(in,out);
  else 
    if (lastpage_ == "StateMachine" )
      this->displayStateMachine(in,out);


}


void ShmCollector::WebShowGenericLinks(xgi::Output * out) throw (xgi::exception::Exception)
{

  std::string statlink = toolbox::toString("/%s/displayStateMachine",getApplicationDescriptor()->getURN().c_str());
  std::string paralink = toolbox::toString("/%s/displayParameters",getApplicationDescriptor()->getURN().c_str());
  
  *out << "<p style=\"font-family:arial;font-size:10pt;color:#0000AA\">[<a href="
       << statlink << ">State machine</a>] [<a href="
       << paralink << ">Parameter Status</a>] "
       << "</p>";

}




void ShmCollector::displayParameters(xgi::Input * in, xgi::Output * out) 
{

  *out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
  *out << cgicc::html().set("lang", "en").set("dir","ltr") << std::endl;
  *out << cgicc::title("Data Sender") << std::endl;
  xgi::Utils::getPageHeader(*out, "Data Sender");

  this->WebShowGenericLinks(out);
  lastpage_="Parameters";
  this->WebShowParameters(out);
  xgi::Utils::getPageFooter(*out);


}


void ShmCollector::WebShowParameters(xgi::Output * out) throw (xgi::exception::Exception)
{

  try {
    *out << cgicc::h3("Data Sender (ShmCollector) Parameter Display ") << std::endl;

    //xgi::Utils::getPageHeader(*out, "Configure");
	

    *out << "<table border border=1 cellpadding=1 cellspacing=3>" << std::endl;
    
    
    *out << "<tr><td> RU name </td> <td>"<< ruName_.value_ <<"</td> </tr>" <<endl;
    *out << "<tr><td> RU instance </td> <td>"<< ruInstance_.value_ <<"</td> </tr>" <<endl;
    *out << "<tr><td> Continuous </td> <td>"<< continuous_.value_ <<"</td> </tr>" <<endl;
    *out << "<tr><td> Buffers </td> <td>"<< getNumberOfBuffers() <<"</td> </tr>" <<endl;
    *out << "<tr><td> Sent Buffers </td> <td>"<< nsent_ <<"</td> </tr>" <<endl;
    *out << "<tr><td> Purged Buffers </td> <td>"<< nPurged_ <<"</td> </tr>" <<endl;
    *out << "<tr><td> Current GTC completed </td> <td>"<< theLastGTCSize_ <<"</td> </tr>" <<endl;
    *out << "<tr><td> Current FIFO map size </td> <td>"<< theFifoSize_ <<"</td> </tr>" <<endl;
    

    *out << "</table>" << "<br><br> "<< std::endl;


    if (triggerPaused_.value_)
      *out << cgicc::h3("TRIGGER PAUSED ") << std::endl;
    else
      *out << cgicc::h3("TRIGGER ENABLED ") << std::endl;
    *out << cgicc::h3("Share memory summary ") << std::endl;
    *out << "<table border border=1 cellpadding=1 cellspacing=3>" << std::endl;
    *out << "<tr><td> Instance </td><td>DIF</td><td>LDA #</td><td>DCC #</td><td> Last Event </td> <td>Nfree </td></tr>" << endl;

    // for (int i=firstDifIndex_; i<=lastDifIndex_;i++)
    //   {
    // 	*out << "<tr>" ; 
    // 	*out <<" <td>" << i <<"</td>";
    // 	*out <<" <td>" << difID_[i] <<"</td>";
    // 	*out <<" <td>" << LDASlot_[i] <<"</td>";
    // 	*out <<" <td>" <<DCCSlot_[i] <<"</td>";
    // 	*out <<" <td>" <<lastEvent_[i]<< std::dec<<"</td>";
    // 	*out <<" <td>" <<fedShms_->getNumberOfFreeSlot(i)<< std::dec<<"</td>";
    // 	*out <<"</tr>" << endl;
	
    //   }
    *out << "</table>" << "<br><br> "<< std::endl;


  } catch(xgi::exception::Exception &e) {
    XCEPT_RETHROW(xgi::exception::Exception, "Exception caught in WebShowRun", e);
    
  }
}

void ShmCollector::displayStateMachine(xgi::Input * in, xgi::Output * out)
{

  //*out << cgicc::HTTPHTMLHeader();
  //*out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;

  *out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
  *out << cgicc::html().set("lang", "en").set("dir","ltr") << std::endl;
  *out << cgicc::title("Data Sender") << std::endl;
  xgi::Utils::getPageHeader(*out, "Data Sender");

  this->WebShowGenericLinks(out);
  lastpage_="StateMachine";
  this->WebShowStateMachine(out);

  xgi::Utils::getPageFooter(*out);

}


void ShmCollector::WebShowStateMachine(xgi::Output * out) throw (xgi::exception::Exception)
{
  try {

    std::string action = toolbox::toString("/%s/dispatch",getApplicationDescriptor()->getURN().c_str());
    
    
    // display FSM
    std::set<std::string> possibleInputs = fsm_.getInputs(fsm_.getCurrentState());
    std::set<std::string> allInputs = fsm_.getInputs();
  
  
    *out << cgicc::h3("Tracker Supervisor Finite State Machine").set("style", "font-family: arial") << std::endl;
    *out << "<table border cellpadding=10 cellspacing=0>" << std::endl;
    *out << "<tr>" << std::endl;
    *out << "<th>" << fsm_.getStateName(fsm_.getCurrentState()) << "</th>" << std::endl;
    *out << "</tr>" << std::endl;
    *out << "<tr>" << std::endl;
    std::set<std::string>::iterator i;
    for ( i = allInputs.begin(); i != allInputs.end(); i++)
      {
	*out << "<td>"; 
	*out << cgicc::form().set("method","get").set("action", action).set("enctype","multipart/form-data") << std::endl;
      
	if ( possibleInputs.find(*i) != possibleInputs.end() )
	  {
	    *out << cgicc::input().set("type", "submit").set("name", "StateInput").set("value", (*i) );
	  }
	else
	  {
	    *out << cgicc::input() .set("type", "submit").set("name", "StateInput").set("value", (*i) ).set("disabled", "true");
	  }
      
	*out << cgicc::form();
	*out << "</td>" << std::endl;
      }
  
    *out << "</tr>" << std::endl;
    *out << "</table>" << std::endl;
    
  
  

  } catch(xgi::exception::Exception &e) {
    XCEPT_RETHROW(xgi::exception::Exception, "Exception caught in WebShowRun", e);
  }

}


void ShmCollector::dispatch(xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{   
  try 
    {
      // Create a new Cgicc object containing all the CGI data
      cgicc::Cgicc cgi(in);
      cgicc::const_form_iterator stateInputElement = cgi.getElement("StateInput");
      std::string stateInput = (*stateInputElement).getValue();


      try 
	{
	  toolbox::Event::Reference e(new toolbox::Event(stateInput,this));
	  fsm_.fireEvent(e);
	}
      catch (toolbox::fsm::exception::Exception & e)
	{
	  XCEPT_RETHROW(xgi::exception::Exception, "invalid command", e);
	}



    }
  catch(const std::exception& e) 
    {
      XCEPT_RAISE(xgi::exception::Exception,  e.what());
    }


  this->displayStateMachine(in,out);
}






void ShmCollector::actionPerformed (xdata::Event& e)
{
  // update measurements monitors		
  if (e.type() == "ItemRetrieveEvent")
    {
      std::string item = dynamic_cast<xdata::ItemRetrieveEvent&>(e).itemName();
      if ( item == "count")
	{
	  // count_=....
	  //diagService_->reportError( toolbox::toString("count: %d",count_.value_),DIAGINFO);
	} 
    }	


 
}
	



//
// SOAP Callback trigger state change 
//
xoap::MessageReference ShmCollector::fireEvent (xoap::MessageReference msg) throw (xoap::exception::Exception)
{
  xoap::SOAPPart part = msg->getSOAPPart();
  xoap::SOAPEnvelope env = part.getEnvelope();
  xoap::SOAPBody body = env.getBody();
  DOMNode* node = body.getDOMNode();
  DOMNodeList* bodyList = node->getChildNodes();
  for (unsigned int i = 0; i < bodyList->getLength(); i++) 
    {
      DOMNode* command = bodyList->item(i);

      if (command->getNodeType() == DOMNode::ELEMENT_NODE)
	{
	  std::string commandName = xoap::XMLCh2String (command->getLocalName());
	  if (commandName == "Configure" || 
	      commandName == "Enable" ||
	      commandName == "Stop" ||
	      commandName == "Halt")
	    try 
	      {
		toolbox::Event::Reference e(new toolbox::Event(commandName, this));
		fsm_.fireEvent(e);
		xoap::MessageReference reply = xoap::createMessage();
		xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
		xoap::SOAPName responseName = envelope.createName( commandName +"Response", "xdaq", XDAQ_NS_URI);
		envelope.getBody().addBodyElement ( responseName );
		return reply;

	      }
	    catch (toolbox::fsm::exception::Exception & e)
	      {
		XCEPT_RETHROW(xcept::Exception, "invalid command", e);
	      }
	  else if  (commandName == "Reset")
	    {
	      triggerPaused_=false;
	      xoap::MessageReference reply = xoap::createMessage();
	      xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
	      xoap::SOAPName responseName = envelope.createName( commandName +"Response", "xdaq", XDAQ_NS_URI);
	      envelope.getBody().addBodyElement ( responseName );
	      return reply;
	    }				

	}
    }

  XCEPT_RAISE(xcept::Exception,"command not found");		
}




	


void ShmCollector::failedTransition (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception)
{
  toolbox::fsm::FailedEvent & fe = dynamic_cast<toolbox::fsm::FailedEvent&>(*e);

  std::ostringstream logMsg;
  logMsg << "Failure occurred when performing transition from: "  << fe.getFromState() <<  " to: " << fe.getToState() << " exception: " << fe.getException().what();
  //diagService_->reportError( logMsg.str(),DIAGERROR);
  lastKnownError_ =  xcept::htmlformat_exception_history(fe.getException());		
}


//DIAGREQUESTEDBEGIN




void ShmCollector::checkTrigger()
{
  theFifoSize_=theFifoManager_->getBufferMap().size();
  if (theFifoManager_->getBufferMap().size()>400) purgeCorrupted();
  if (eventNumber_-eventTrigger_>nbOfBufferedTrigger_.value_
      || theFifoManager_->getBufferMap().size()>2000 )
    {

      if (!triggerPaused_.value_)
	{
	  BasicSoapCommand("CCCManagerApplication", 0,"Pause","Pause");
	  triggerPaused_.value_=true;
	  std::cout<<" CheckTrigger Too many buffer is stopped " <<eventNumber_<<" "<<eventTrigger_<<std::endl;
	  
	}
     
      
    }
  else
    if (triggerPaused_.value_)
      {
	BasicSoapCommand("CCCManagerApplication", 0,"Resume","Resume");
	triggerPaused_.value_=false;
	std::cout<<"Trigger Resume " <<eventNumber_<<" "<<eventTrigger_<<std::endl;

	  
      }


 
}
void ShmCollector::flagSlots()
{
 
  this->checkTrigger();
  theFifoManager_->Lock();  
  // Find slots to be transfered
  std::map<uint64_t,std::vector<unsigned char*> >& bmap=theFifoManager_->getBufferMap();
  vGTC_.clear();
  for (std::map<uint64_t,std::vector<unsigned char*> >::iterator it=bmap.begin();it!=bmap.end();it++)
    {
      // std::cout<<it->first<<" Size of vector "<<it->second.size()<<std::endl;
      if (it->second.size()== (uint32_t) theNumberOfDIF_.value_)
	{
	  vGTC_.push_back(it->first);
	}
    }
  theFifoManager_->UnLock();
  if (vGTC_.size()==0 && bmap.size()>0)
    printf(" Map size %d \n",bmap.size());
  // Pause the trigger if not enough SLot free
  



}



void ShmCollector::purgeSlots()
{

  std::map<uint64_t,std::vector<unsigned char*> >& bmap=theFifoManager_->getBufferMap();
  theFifoManager_->Lock();
  for (std::map<uint64_t,std::vector<unsigned char*> >::iterator it=bmap.begin();it!=bmap.end();)
    {
      
      if (it->second.size()==(uint32_t)  theNumberOfDIF_.value_ || (it->first<lastGtc_))
	{
	  for (std::vector<unsigned char*>::iterator iv=it->second.begin();iv!=it->second.end();iv++) delete (*iv);
	  it->second.clear();
	  bmap.erase(it++);
	}
      else
	it++;
    }
  theFifoManager_->UnLock();
}

void ShmCollector::purgeSlot(uint64_t gtc)
{

  std::map<uint64_t,std::vector<unsigned char*> >& bmap=theFifoManager_->getBufferMap();
  theFifoManager_->Lock();
  for (std::map<uint64_t,std::vector<unsigned char*> >::iterator it=bmap.begin();it!=bmap.end();)
    {
      
      if ( (it->first==gtc)  || (it->first==(gtc-1)  ) || (it->first==(gtc+1)  ) )
	{
	  for (std::vector<unsigned char*>::iterator iv=it->second.begin();iv!=it->second.end();iv++) delete (*iv);
	  it->second.clear();
	  bmap.erase(it++);
	}
      else
	it++;
    }
  theFifoManager_->UnLock();
}

void ShmCollector::purgeCorrupted()
{

  std::map<uint64_t,std::vector<unsigned char*> >& bmap=theFifoManager_->getBufferMap();
  theFifoManager_->Lock();
  for (std::map<uint64_t,std::vector<unsigned char*> >::iterator it=bmap.begin();it!=bmap.end();)
    {
      
      if (it->first<(lastGtc_-150) && it->second.size()<theNumberOfDIF_.value_)
	{
	  std::cout<<it->first<<" has "<<it->second.size()<<" DIF and is lower than "<<lastGtc_<<" so it's purged "<<std::endl;
	  for (std::vector<unsigned char*>::iterator iv=it->second.begin();iv!=it->second.end();iv++) delete (*iv);
	  it->second.clear();
	  bmap.erase(it++);
	}
      else
	it++;
    }
  theFifoManager_->UnLock();
}





void ShmCollector::pushEvent(uint64_t gtc)
{
  //this->checkTrigger();
  std::cout<<"Pushing "<<gtc<<std::endl;
  std::map<uint64_t,std::vector<unsigned char*> >::iterator it_gtc=theFifoManager_->getBufferMap().find(gtc);
  if (it_gtc==theFifoManager_->getBufferMap().end())
    it_gtc=theFifoManager_->getBufferMap().find(gtc-1);
  if (it_gtc==theFifoManager_->getBufferMap().end())
    it_gtc=theFifoManager_->getBufferMap().find(gtc+1);
  if (it_gtc==theFifoManager_->getBufferMap().end())
    {
      std::cout<<"----------------------> ou c'est qu'il est le BCID ?"<<gtc<<std::endl;
      return;
    }
  std::vector<unsigned char*>& vb=it_gtc->second;
  uint32_t ifed=0,evtn=0;
  toolbox::mem::Reference* head=0;

  for (std::vector<unsigned char*>::iterator ib=vb.begin();ib!=vb.end();ib++)
    {
      unsigned char* cdata=(*ib);
      uint32_t* idata= (uint32_t*) cdata;
      
      int dataShift = sizeof(frlh_t)/sizeof(int);
      uint32_t returned_buffer_size = idata[FEDSHM_BUFFER_SIZE];
      //std::cout<<returned_buffer_size<<std::endl;
      uint32_t dif_buffer_size=returned_buffer_size-FEDSHM_EVENT_ADDRESS*sizeof(uint32_t);
      
      uint32_t allo_size= (dataShift+32)*sizeof(int)+returned_buffer_size;
      toolbox::mem::Reference* tail=NULL;
      while (tail==NULL)
	{
	  try 
	    {
	      //std::cout<<" allocating"<<allo_size<<std::endl;
	      tail=allocateDataBuffer(allo_size);
	    }
	  catch(...)
	    {
	      std::cout<<"Cannot allocate buffer"<<allo_size<<std::endl;
	      tail=NULL;
	      for (uint32_t jj=0;jj<200;jj++) usleep(100);	      
	    }
	}
      
      
      
      //	std::cout << " tail allocated "<<tail <<std::endl;
      int* data = (int*) getUserAddress(tail);
      //std::cout<<ifed<<" size" <<returned_buffer_size<<" DIF size"<<dif_buffer_size<< " tail size"<<allo_size<<std::endl;
      // for (uint32_t i =0;i<400;i++) printf("%02x",cdata[i]);
      // usleep((unsigned int) 100);
      memcpy(&data[dataShift],&cdata[FEDSHM_EVENT_ADDRESS*sizeof(uint32_t)],dif_buffer_size);
      evtn = idata[FEDSHM_EVENT_NUMBER];
      // Fill the FRL header
      frlh_t* frlh = (frlh_t*) &data[0];
      frlh->trigno =evtn;  
      frlh->segno = ifed;
      frlh->segsize=returned_buffer_size;
      if (ifed == (vb.size()-1)) 
	{
	  //std::cout<<ifed <<"Setting last segments"<<std::endl;
	  frlh->segsize  |= FRL_LAST_SEGM ;
	}
      uint32_t newsiz= dif_buffer_size+dataShift*sizeof(uint32_t)+8;
      //std::cout<<ifed<<" Update size"<<newsiz<<std::endl;
      //updateSize(tail,newsiz*sizeof(int));
      updateSize(tail,newsiz);
  
      // Now create and fill the buffer list
      if (ifed==0)
	{
	  
	  head=tail;
	  I2O_EVENT_DATA_BLOCK_MESSAGE_FRAME* block  = (I2O_EVENT_DATA_BLOCK_MESSAGE_FRAME*) head->getDataLocation();
	  block->nbBlocksInSuperFragment = 1;
	}
      else
	{
	  //  std::cout<<"Append buffer "<<std::endl;
	  head = appendBuffer(head,tail);
	}
      ifed++;
    }

  //  std::cout<<__PRETTY_FUNCTION__<<" Update event number "<<evtn<<std::endl;
  this->sendBuffer(head);
  lastGtc_=gtc;
}


