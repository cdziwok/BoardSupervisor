/* Copyright 2013 Institut Pluridisciplinaire Hubert Curien
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

   Programmer : 	Christian Bonnin
   Version : 		1.0
   Date of creation:01/10/2013
   Support : 		mail to : christian.bonnin@iphc.cnrs.fr
*/

#include <iostream>
#include <boost/format.hpp>
#include "FedEvent2020.h"
#include "Acquisition.h"


#define IDX_NUMBER_CBC			1
#define IDX_FORMAT				7
#define IDX_EVENT_TYPE			6
#define IDX_GLIB_STATUS 		3
#define IDX_FRONT_END_STATUS 	8
#define IDX_FRONT_END_STATUS_MSB 0
//littleEndian8 index
#define IDX_CBC_STATUS 			16

#define FORMAT_VERSION 			2
#define ZERO_SUPPRESSION		1
#define VIRGIN_RAW				2
#define GLIB_STATUS_REGISTERS	0

using namespace std;

//void dumpBuffer(const string& strName, const char* buffer, uint32_t begin, uint32_t end){
//	for (uint32_t idx=begin; idx<end; idx++){
//		if (idx%16==0) cout<<endl<<strName<<"["<<hex<<setw(2)<<setfill('0')<<idx<<"]: ";
//		if (idx%8==0) cout<<" ";
//		cout <<hex<<setw(2)<<setfill('0')<<int(buffer[idx]&255)<<" ";
//	}
//	cout<<endl;
//}

/** Construct one event from GLIB data. One fiber out of 12 is filled with data and the others are empty.
 * @param uAcqMode Acquisition mode (1:Full debug, 2:CBC error, 0:Summary error)
 * @param uFE Front Ends available (one bit per FE)
 * @param nbCBC Total number of CBC
 * @param uCBC Available CBCs (one bit per CBC) on each FE. temporary : only one CBC status
 * @param bZeroSuppr Sparsified mode
 * @param pPSetCondition pointer on the condition data parameter set
 * @param buffer GLIB data buffer
 * @param idxBuf index of the event data in buffer 
 */ 
FedEvent2020::FedEvent2020(uint32_t uAcqMode, bool bZeroSuppr, bool bConditionData, bool bFakeData,
							uint32_t uFE, uint32_t nbCBC, uint32_t uCBC, ParameterSetUhal* pPSetCondition, const char * buffer, uint32_t idxBuf){

	uint32_t nbBitsPayload=0, nbBitsCondition=0, nbBitsHeader = 128;//at least 2 64-bits words
	uint32_t nbCondition = 0;
	
	switch (uAcqMode){
		case STREAMER_ACQ_MODE_FULLDEBUG:
			nbBitsHeader+= 10 * nbCBC;
			break;
		case STREAMER_ACQ_MODE_CBCERROR:
			nbBitsHeader+= nbCBC*2;
			break;
	}//+=0 if STREAMER_ACQ_MODE_SUMMARYERROR
	//cout << "nbBitsHeader = " << nbBitsHeader << endl;
	uint32_t nbFE=0, uFront=uFE;
	while (uFront>0){//Count number of FE
		if (uFront%2>0){
			nbFE++;
		}
		uFront>>=1;
	}
	uint32_t nbCbcFe=nbCBC/nbFE;//nb of CBCs per FE
	//cout << "nbFE = " << nbFE << endl;

	if (bZeroSuppr){
		for (uFront=0; uFront<nbFE;uFront++)
			nbBitsPayload+=	calcBitsForFE(buffer, idxBuf+uFront*CBC_SIZE*nbCbcFe, NULL, 0, nbCbcFe);
				
	} else
		nbBitsPayload=16 * nbFE + nbCBC*NB_STRIPS_CBC2;//Payload size (bits)
		
	//cout << "bZeroSuppr = " << bZeroSuppr << ", nbBitsPayload = " << nbBitsPayload << endl;
	nbBitsPayload+=64*4;//Stub data
	//cout << "nbBitsPayload = " << nbBitsPayload << ", nbCBC = " << nbCBC << endl;

	//Condition data
	if (bConditionData){
		nbCondition = countConditionData(pPSetCondition);
		nbBitsCondition=(nbCondition+1)*64;
	}
	nbBitsHeader=nbBitsHeader/64*64 + (nbBitsHeader%64>0 ? 64 :0);//padded to 64 bits
	nbBitsPayload=nbBitsPayload/64*64 + (nbBitsPayload%64>0 ? 64 :0);//padded to 64 bits
	size_=(nbBitsHeader + nbBitsPayload + nbBitsCondition)/8;
	//cout<<"Tracker size (Bytes)= "<<nbBitsHeader/8<<"+"<<nbBitsPayload/8<<"= "<<size_<<endl;
	data_=new char[size_];
	memset (data_, 0, size_);
	fillTrackerHeader(buffer, idxBuf, uFE, nbCBC, uAcqMode, bZeroSuppr, bConditionData, bFakeData);
	fillTrackerPayload(buffer, idxBuf, nbFE, nbCBC, uCBC, nbBitsHeader, bZeroSuppr, 
						bConditionData, nbCondition, pPSetCondition);
	if (bConditionData)
		fillTrackerConditionData(buffer, idxBuf, (nbBitsHeader+nbBitsPayload)/8, nbCBC, nbCondition, pPSetCondition);
}
							
///Fill the tracker header
void FedEvent2020::fillTrackerHeader(const char * buffer, uint32_t idxBuf, uint64_t uFE, uint32_t nbCBC, uint32_t uAcqMode, 
				bool bZeroSuppr, bool bConditionData, bool bFakeData){
	data_[IDX_FORMAT] = FORMAT_VERSION<<4 | uAcqMode<<2 | (bZeroSuppr ? ZERO_SUPPRESSION : VIRGIN_RAW);//Data format version, Header format, 
	data_[IDX_EVENT_TYPE] = (bConditionData ? 1 : 0)<<7 | (bFakeData ? 0 : 1)<<6 | (GLIB_STATUS_REGISTERS&0x3F000000)>>24;//Event type, DTC status registers
	data_[IDX_GLIB_STATUS+2]= (GLIB_STATUS_REGISTERS&0xFF0000)>>16;
	data_[IDX_GLIB_STATUS+1]= (GLIB_STATUS_REGISTERS&0xFF00)>>8;
	data_[IDX_GLIB_STATUS]	= (GLIB_STATUS_REGISTERS&0xFF);
	data_[IDX_FRONT_END_STATUS_MSB] = 0;//(uFE>>64)&0xFF;//Front End status (72 bits but only 64 for now: temporary)
	for (uint32_t uIdx=0; uIdx<8; uIdx++)
		data_[IDX_FRONT_END_STATUS+uIdx]=(uFE>>(uIdx*8))&0xFF;
		
	//Total number of CBC chips
	data_[IDX_NUMBER_CBC+1]	= (nbCBC&0xFF00)>>8;
	data_[IDX_NUMBER_CBC]	= (nbCBC&0xFF);
	
	uint32_t uChip, uStatus;
	for (uChip = 0; uChip<nbCBC; uChip++)//CBC status in header
		switch (uAcqMode){
			case STREAMER_ACQ_MODE_FULLDEBUG://10 bits always over 2 bytes
				uStatus = (buffer[idxBuf+uChip*CBC_SIZE+1]&0xC0)>>6	| (buffer[idxBuf+uChip*CBC_SIZE+0]&0x3F)<<2;
				data_[littleEndian8(IDX_CBC_STATUS + uChip*10/8)] 		|= uStatus>>((uChip*2+2)%10) & 0xFF;
				data_[littleEndian8(IDX_CBC_STATUS + uChip*10/8 + 1)] 	|= uStatus<<(8-(uChip*2+2)%10) & 0xFF;
				break;
			case STREAMER_ACQ_MODE_CBCERROR:
				data_[littleEndian8(IDX_CBC_STATUS + uChip/4/* *2/8 */)] |= ((buffer[idxBuf+uChip*CBC_SIZE+0]>>6)&3)<<(8-(uChip*2)%8);
				break;
		}//nothing if STREAMER_ACQ_MODE_SUMMARYERROR
}

void FedEvent2020::fillTrackerPayload(const char * buffer, uint32_t idxBuf, uint32_t nbFE, uint32_t nbCBC, uint32_t uCBC, uint32_t nbBitsHeader,
										bool bZeroSuppr, bool bCondition, uint32_t nbCondition, ParameterSetUhal* pPSetCondition){
	// Fill the tracker payload 	
	uint32_t uIdxCbc=0, uOct, idxPayload, bitPayload=nbBitsHeader, uFront, uChip;
	uint32_t nbCbcFe=nbCBC/nbFE;//nb of CBCs per FE
	for (uFront=0; uFront<nbFE; uFront++){
		//cout<<"FE="<<uFront<<" @"<<idxPayload<<endl;
		if (bZeroSuppr){
			bitPayload+=(calcBitsForFE(buffer, idxBuf+uFront*CBC_SIZE*nbCbcFe, data_, bitPayload, nbCbcFe)+7)/8;
			idxPayload=(bitPayload+7)/8;
		} else {
			idxPayload=nbBitsHeader/8+uFront*(nbCbcFe*NB_STRIPS_CBC2/8 + 2);
			data_[littleEndian8(idxPayload++)]=(uCBC&0xFF00)>>8;
			data_[littleEndian8(idxPayload++)]=(uCBC&0x00FF);
			for (uChip=0; uChip<nbCbcFe; uChip++){
		//cout<<"FE,CBC: "<<uFront<<","<<uChip<<endl;
				for (uOct=0; uOct<NB_STRIPS_CBC2/8; uOct++){
					//cout << dec <<"["<<littleEndian8(idxPayload)<<"<-"<<idxBuf+ uIdxCbc*CBC_SIZE + uOct<<" <- " << (idxBuf+((uIdxCbc+1)*CBC_SIZE -4 - uOct) ^3) <<"] ";
					//data_[littleEndian8(idxPayload++)]=buffer[idxBuf+((uIdxCbc+1)*CBC_SIZE -4 - uOct)] & (uOct==NB_STRIPS_CBC2/8-1 ? 0x3F : 0xFF);
					
					// Jonni: so we need to skip 2 CBC error bits and then 8 Pipeline Address Bits then we need to reconstruct nibbles from half nibbles
					data_[littleEndian8(idxPayload)] =  
						(((buffer[idxBuf+ uIdxCbc*CBC_SIZE +1 + uOct] ) << 2) & 0xfc) |
						(((buffer[idxBuf+ uIdxCbc*CBC_SIZE +2 + uOct] ) >> 6) & 0x03);
					reverseByte(data_[littleEndian8(idxPayload++)]);
				}
				uIdxCbc++;
				//cout<<endl;
			}
		}//if zero suppressed
	}//for Front End
	//Stub Data
	idxPayload=(idxPayload+7)/8*8;
//	cout<<"idxStub="<<idxPayload<<endl;
	for (uChip = 0; uChip<nbCBC; uChip++){//Stub bits
		if ((buffer[idxBuf + uChip*CBC_SIZE+35]&0x01)!=0)
			data_[littleEndian8(idxPayload+7-uChip/8)] |= (1<<(uChip%8));
	}
//	idxPayload+=uChip/8 + 1;
}

void FedEvent2020::fillTrackerConditionData(const char* buffer, uint32_t idxBuf, uint32_t idxPayload, 
									uint32_t nbCBC, uint32_t nbCondition, ParameterSetUhal* pPSetCondition)
{//Condition data
//	cout<<"idxNbCondition="<<idxPayload<<endl;
	uint32_t uOct, uCond, uVal=0, uChip;
	idxPayload+=4;
	for (uOct=4; uOct<8; uOct++)//Nb of condition data in 64 bits
		data_[littleEndian8(idxPayload++)]= (nbCondition>>(56-uOct*8))&0xFF;
		
//	cout<<"idxCondition="<<idxPayload<<endl;
	for (uCond=0 ; uCond<NB_CONDITION_DATA; uCond++){//Key
		if (pPSetCondition->getValue((boost::format(CONDITION_DATA_ENABLED)%uCond).str())==1){
			data_[littleEndian8(idxPayload++)]=pPSetCondition->getValue((boost::format(CONDITION_DATA_FE_ID)%uCond).str())&0xFF;
			data_[littleEndian8(idxPayload++)]=((pPSetCondition->getValue((boost::format(CONDITION_DATA_CBC)%uCond).str())&0x0F)) 
				| ((pPSetCondition->getValue((boost::format(CONDITION_DATA_PAGE)%uCond).str())&0x0F)<<4);
			data_[littleEndian8(idxPayload++)]=pPSetCondition->getValue((boost::format(CONDITION_DATA_REGISTER)%uCond).str())&0xFF;
			data_[littleEndian8(idxPayload++)]=pPSetCondition->getValue((boost::format(CONDITION_DATA_TYPE)%uCond).str())&0xFF;
			switch(pPSetCondition->getValue((boost::format(CONDITION_DATA_TYPE)%uCond).str())){//Value
				case 3://Trigger phase (TDC)
		//				uFront = pPSetCondition->getValue((boost::format(CONDITION_DATA_FE_ID)%uCond).str());
		
					uVal=buffer[idxBuf+ CBC_SIZE*std::max(nbCBC,(uint32_t)4)+3];//Variable GLIB format
					break;
				case 6://Error bits
					uVal=0;
					for (uChip=0;uChip<nbCBC;uChip++){
						uVal |= ((buffer[idxBuf+ (CBC_SIZE*uChip)]&0xC0)>>6)<<(uChip*2);
					}
					break;
				case 7://CBC Status bits
					uVal=0;
					if (nbCBC<=4){
						for (uChip=0;uChip<nbCBC && uChip<4;uChip++)//Variable GLIB format ?
						{//temporary: All CBC status bits in one 32-bits value
							uVal |= (((buffer[idxBuf+ (CBC_SIZE*uChip)]&0x3F)<<2) 
									| ((buffer[idxBuf+ (CBC_SIZE*uChip) + 1]&0xC0)>>6)) << (uChip*8);
						}	
					} else {	
						uChip=pPSetCondition->getValue((boost::format(CONDITION_DATA_CBC)%uCond).str())&0x0F; 
						uVal = (((buffer[idxBuf+ (CBC_SIZE*uChip)]&0x3F)<<2) | ((buffer[idxBuf+ (CBC_SIZE*uChip) + 1]&0xC0)>>6));
					}
					break;
				case 10://Bunch counter
					uVal=(buffer[idxBuf-17]&255) | ((buffer[idxBuf-18]&255)<<8) | ((buffer[idxBuf-19]&255)<<16);
					break;
				case 11://Orbit counter
					uVal=(buffer[idxBuf-13]&255) | ((buffer[idxBuf-14]&255)<<8) | ((buffer[idxBuf-15]&255)<<16);
					break;
				case 12://Lumisection
					uVal=(buffer[idxBuf-9]&255) | ((buffer[idxBuf-10]&255)<<8) | ((buffer[idxBuf-11]&255)<<16);
					break;
				default://Configuration parameter (I2C), Angle, High Voltage, Other Value
					uVal=pPSetCondition->getValue((boost::format(CONDITION_DATA_VALUE)%uCond).str());
					break;
			}
			for (uOct=0; uOct<4; uOct++)
				data_[littleEndian8(idxPayload++)]= (uVal>>(24-uOct*8))&0xFF;
		}
	}
}

	/**Data buffer construction and data size computing for one FE in sparsified mode for 2S modules. Two pass are necessary: <ul>
	 * <li> one to compute size with NULL as destination buffer</li>
	 * <li> one to construct destination buffer</li></ul>
	 * @return Data size in bits
	 * @param buffer GLIB data buffer
	 * @param index idxBuf of event data in buffer
	 * @param dest destination buffer. Set to NULL to only compute size.
	 * @param bitDest index to put data for this fiber into destination buffer
	 * @param nbCBC Number of CBCs in this FE
	 */
uint32_t FedEvent2020::calcBitsForFE(const char* buffer, uint32_t idxBuf, char* dest, uint32_t bitDest, uint32_t nbCBC){
	uint32_t uChip, uBit, nbCluster=0, uClusterSize, nbMax=63, uPos;
	uint32_t nbBits=7;//FE header size for 2S modules
	bool bCluster;
	
	for (uChip=0; uChip<nbCBC; uChip++){
		uClusterSize=0;
		bCluster=false;
		if (nbCluster>=nbMax) break;
		for (uBit=0; uBit<NB_STRIPS_CBC2-2; uBit++){
			if (bCluster){
				if ((buffer[idxBuf + (uChip+1)*CBC_SIZE -4 - uBit/8 ] & (1<<(uBit%8))) == 0  || uBit==NB_STRIPS_CBC2-3){//end of cluster
					bCluster=false;
					if (dest){//<chip ID 4b><position 8b><size 3b>
						setValueBits(dest, bitDest+nbBits   , 4, uChip);//Chip ID
						setValueBits(dest, bitDest+nbBits+ 4, 8, uPos);//position of cluster
						setValueBits(dest, bitDest+nbBits+12, 3, uClusterSize-1); //size of cluster
					}
					nbBits+=15;
					if (nbCluster>=nbMax) break;
				} else if (buffer[idxBuf + (uChip+1)*CBC_SIZE -4 - uBit/8 ] & (1<<(uBit%8))){// cluster continuation
					uClusterSize++;
					if (uClusterSize>8){
						if (dest){
							setValueBits(dest, bitDest+nbBits   , 4, uChip);//Chip ID
							setValueBits(dest, bitDest+nbBits+ 4, 8, uPos);//position of cluster
							setValueBits(dest, bitDest+nbBits+12, 3, 7);  //size of cluster
						}
						nbBits+=15;
						if (nbCluster>=nbMax) break;
						nbCluster++;
						uClusterSize=1;
						uPos=uBit;
					}
				}
			} else {//Beginning of  cluster
				if (buffer[idxBuf + (uChip+1)*CBC_SIZE -4 - uBit/8] & (1<<(uBit%8))){
					uPos=uBit;
					nbCluster++;
					uClusterSize=1;
					bCluster=true;
				}
			}
		}//for strips
	}//for CBC
	if (dest){
		setValueBits(dest, bitDest+1, 6, nbCluster);//Number of S clusters, module type bit is 0 (2S module)
	}
	return nbBits;
}
/** Set bit values over one or two bytes 
 * \param arrDest Destination array of bytes
 * \param bitDest position of Most Significant bit in arrDest
 * \param width field length in bits
 * \param uVal field value */
void FedEvent2020::setValueBits(char* arrDest, uint32_t bitDest, uint8_t width, uint8_t uVal){
	if (width>8-bitDest%8){//over two bytes
		uint32_t nbMsb= width - (8-bitDest%8);
		uint32_t nbLsb= width - nbMsb;
		arrDest[littleEndian8(bitDest/8)] 	|= (uVal >> nbLsb);
		arrDest[littleEndian8(bitDest/8 + 1)] 	|= (uVal&((1<<nbLsb)-1))<< (8-nbLsb);
	} else {//over one byte
		arrDest[littleEndian8(bitDest/8)] 	|= uVal << (8-bitDest%8-width);
	}
}

uint32_t FedEvent2020::countConditionData(ParameterSetUhal* pPSetCondition){
	uint32_t uRet=0;
	for (int iCond=0; iCond<NB_CONDITION_DATA; iCond++){
		if (pPSetCondition!=NULL && pPSetCondition->getValue((boost::format(CONDITION_DATA_ENABLED)%iCond).str())==1)
			uRet++;
	}
	return uRet;
}
