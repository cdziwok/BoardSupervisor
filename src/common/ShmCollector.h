#ifndef _ShmCollector_h_
#define _ShmCollector_h_


#include "xcept/tools.h"
#include "xdata/Integer.h"
#include "xdata/UnsignedShort.h"
#include "TrackerCommandSender.h"
#include "jsinterface.h"
#include "FRLA.h"

#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/Action.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/TimeInterval.h"
#include "toolbox/BSem.h"


#include "FifoManager.h"


/**
  \class ShmCollector
  \author  L.Mirabito 
  \date January 2009
  \version 1.0

   \brief Data Sender
*/

class ShmCollector :  public TrackerCommandSender, public FRLA , public JsInterface
{

 public:

  ShmCollector (xdaq::ApplicationStub* stub);
//
  // Callback for requesting current exported parameter values
  //
  void actionPerformed (xdata::Event& e);

  /**
     Find all applications descriptors
   */
  void initDescriptors();

  void createVectorOfDescriptors(std::vector<xdaq::ApplicationDescriptor*> &v,std::string name);





  xoap::MessageReference fireEvent (xoap::MessageReference msg) throw (xoap::exception::Exception);

  void ConfigureAction(toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception);
  void EnableAction(toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception );
  void HaltAction(toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception );

  // Web callback functions
  void Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);



  void displayStateMachine(xgi::Input * in, xgi::Output * out);
  void displayParameters(xgi::Input * in, xgi::Output * out);
  /** 
     Handler of the State machine CGI page
   */

  void dispatch(xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
  /**
   Partial display
  */
  void WebShowGenericLinks(xgi::Output * out) throw (xgi::exception::Exception);
  void WebShowParameters(xgi::Output * out) throw (xgi::exception::Exception);
  void WebShowStateMachine(xgi::Output * out) throw (xgi::exception::Exception);


  //

  virtual void onBuffer (){;}



  virtual toolbox::mem::Reference* senderTrackerCommandHandler(PI2O_TRACKER_COMMAND_MESSAGE_FRAME data);

  void ConfigurePolling();
  bool serviceShmPolling(toolbox::task::WorkLoop* wl);

  void flagSlots();
  void purgeSlots();
  void purgeCorrupted();
  void purgeSlot(uint64_t gtc);
  void pushEvent(uint64_t gtc);


  void checkTrigger();




 public:
  virtual void sendBuffer (toolbox::mem::Reference * channelRef);


 void failedTransition (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception);

  void displayHaltedPage(xgi::Input * in, xgi::Output * out);
  void displayConfiguredPage(xgi::Input * in, xgi::Output * out);
  void displayEnabledPage(xgi::Input * in, xgi::Output * out);
  void displayFailurePage(xgi::Input * in, xgi::Output * out);
  //DIAGREQUESTED
  //void displayDiag(xgi::Input * in, xgi::Output * out);




 
 private:

  FsmWithStateName fsm_;

  


  xdata::Integer ruInstance_;
  xdata::String ruName_;

  xdata::Integer theNumberOfDIF_;
  xdata::Integer nbOfBufferedTrigger_;
  xdata::UnsignedLong preAllocated_;

  xdata::UnsignedLong count_;
  xdata::Boolean usePolling_;

  xdaq::ApplicationDescriptor *ruDescriptor_,*nextDescriptor_;

  int nsent_;
 protected:
  xdata::Boolean  continuous_;
  int totalSize_;
  int nbook_;
  int eventNumber_,eventTrigger_;
  std::string  lastKnownError_,lastpage_;
  std::string XMLCh2String (const XMLCh* ch)
    {
      if (ch == 0) return "";
      
      std::auto_ptr<char> v(XMLString::transcode (ch));
      return std::string(v.get());
    }


  unsigned int lastEvent_[128];

 // work loops for transitional states
  toolbox::task::WorkLoop         *workLoopPolling_;

  toolbox::task::ActionSignature  *asPolling_;

  uint64_t lastGtc_;

  Logger logger_;
  xdata::Boolean triggerPaused_;
  xdata::Boolean theEventDriven_;
  uint32_t nPurged_;

  FifoManager* theFifoManager_;
  std::vector<uint64_t> vGTC_;
  uint32_t difID_[128],LDASlot_[128],DCCSlot_[128];
  uint32_t theFifoSize_,theLastGTCSize_;
};
#endif

