/* Copyright 2012 Institut Pluridisciplinaire Hubert Curien
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

   FileName : 		Acquisition.cc
   Content : 		CDR controller module
   Used in : 		
   Programmer : 	Christian Bonnin
   Version : 		1.0
   Date of creation 01/08/2012
   Support : 		mail to : christian.bonnin@iphc.cnrs.fr
*/
#include <boost/thread.hpp>
#include <boost/date_time.hpp>
#include <boost/format.hpp>
#include <boost/algorithm/string/predicate.hpp> //ends_with
#include <iostream>
#include <iomanip>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>

#include "interface/shared/frl_header.h"

#include "Acquisition.h"
#include "FedEvent.h"
#include "FedEvent2020.h"
//#include "Board.h"
#include "I2cCommunication.h"

#include <cstring> 
#include "Utils/Utilities.h"
#include "HWDescription/Cbc.h"
#include "HWDescription/Module.h"
#include "HWDescription/BeBoard.h"
#include "HWInterface/CbcInterface.h"
#include "HWInterface/BeBoardInterface.h"
#include "HWDescription/Definition.h"
#include "Utils/Event.h"
#include "Utils/Timer.h"
#include "Utils/argvparser.h"
#include "Utils/ConsoleColor.h"
#include "Utils/FileHandler.h"
#include "Utils/FileHeader.h"
#include "Utils/Data.h"
#include "System/SystemController.h"
#include "Tracker/TrackerEvent.h"
#include <inttypes.h>

using namespace std;
/*
void AcfVisitor::init(ofstream* pfSave, ofstream* pfSave2, Acquisition* pAcq){
	pfilSave=pfSave;
	pfilSave2nd=pfSave2;
	pAcquisition = pAcq;
	luMaxSize = pAcquisition->getStreamerParameterSet().getValue(STREAMER_DEST_MAX_SIZE)<<20UL;
	uRatio2nd = pAcquisition->getStreamerParameterSet().getValue(STREAMER_2ND_DEST_RATIO);
	luDestSize = lu2ndDestSize = uNumAcq = 0;
}

void AcfVisitor::visit ( const Ph2_HwInterface::Event& pEvent ) {
	if (pfilSave->is_open()) {//write data to file
		if (pAcquisition->isTextDestination()){
			*pfilSave<<pEvent ;
			luDestSize+=TEXT_EVENT_APPROXIMATE_SIZE;
		} else{
                        for (uint8_t uChar:pEvent.GetEventData()) *pfilSave<<(char)uChar;
                        //pfilSave->write(pEvent.GetEventData(), pEvent.GetSize());

			luDestSize+=pEvent.GetSize();
		}
		pfilSave->flush();
		if (luMaxSize>0 && luDestSize > luMaxSize){
			pAcquisition->incrementDestinationFile(*pfilSave, STREAMER_DEST_FILE);
			luDestSize=0;
		}
		if (pfilSave2nd->is_open() && uNumAcq%uRatio2nd==0){
			if (pAcquisition->isText2ndDestination()){
				*pfilSave2nd<<pEvent ;
				lu2ndDestSize+=TEXT_EVENT_APPROXIMATE_SIZE;
			} else{
                                for (uint8_t uChar:pEvent.GetEventData()) *pfilSave2nd<<(char)uChar;
				//pfilSave2nd->write(pEvent.GetEventData(), pEvent.GetSize());

				lu2ndDestSize+=pEvent.GetSize();
			}
			pfilSave2nd->flush();
			if (luMaxSize>0 && lu2ndDestSize > luMaxSize){
				pAcquisition->incrementDestinationFile(*pfilSave2nd, STREAMER_2ND_DEST_FILE);
				lu2ndDestSize=0;
			}
		}
	} 
	uNumAcq++;
}
*/
Acquisition::Acquisition() {
	transferRate=0;
	triggerRate=0;
	triggerRateInstant=0;
	numAcq=0;
	nbCommAcq=0;
	nbBunchCrossing=nbOrbit=nbLumiSection=nbL1A=nbCbcData=0;
	dataFlags=0;
	uFwVersion=0;
	running=false;
	paused=false;
	pauseToggle=false;
	sharedMem=NULL;
	arrVisu=NULL;
	pEventCopy=NULL;
	pEventCopy2=NULL;
	pfilSave=NULL;
	pfilSimulatedData=NULL;
	bAutoExit=false;

//Default values for acquisition parameters (Supervisor) in case of they are not in Ph2_ACF hardware XML description file
	pSetSuper.setValue(PARAM_DATASIZE, 0);//    dataSize=1;
	pSetSuper.setValue(PARAM_TRIGGER_FROM_TTC, 0);//triggerFromTtc=true;
	pSetSuper.setValue(PARAM_TRIGGER_FREQ,0);
	pSetSuper.setValue(PARAM_EXTERNAL_DATA,1);//=true;
	pSetSuper.setValue(PARAM_COMMISSIONNING_RQ, false);
	pSetSuper.setValue(PARAM_COMMISSIONNING_DELAY_AFTER_FAST_RESET,10);
	pSetSuper.setValue(PARAM_COMMISSIONNING_DELAY_AFTER_TEST_PULSE,0);
	pSetSuper.setValue(PARAM_COMMISSIONNING_DELAY_AFTER_L1A,0);
	pSetSuper.setValue(PARAM_DIO5_THRESHOLD_TRIG_IN, 128);
	pSetSuper.setValue(PARAM_DIO5_THRESHOLD_CLK_IN,  128);
	pSetSuper.setValue(PARAM_DIO5_TRIG_IN_50OHMS, 0);
	pSetSuper.setValue(PARAM_DIO5_TRIG_OUT_50OHMS, 0);		
	pSetSuper.setValue(PARAM_DIO5_CLK_IN_50OHMS, 0);		
	pSetSuper.setValue(PARAM_DIO5_CLK_OUT_50OHMS, 0);		
	pSetSuper.setValue(PARAM_DIO5_BACKPRESSURE_OUT_50OHMS, 0); 	
	pSetSuper.setValue(PARAM_DIO5_TRIG_IN_EDGE, 0);
	pSetSuper.setValue(PARAM_DIO5_BACKPRESSURE_OUT_POLAR, 0);
	pSetSuper.setValue(PARAM_CLK_MUX_SEL, 0);
	
	pSetSuper.setValue(PARAM_FE0_MASKED,0);
	pSetSuper.setValue(PARAM_FE1_MASKED,1);

//    pSetSuper.setValue( EXPECTED_FRONT_END, 1);//temporary
//    pSetSuper.setValue( EXPECTED_CBC, 3);
	lstRef.clear();
}

Acquisition::~Acquisition() {}

bool Acquisition::isRunning() {
    if (pSetStreamer.getValue(STREAMER_ACF_ACQUISITION)<3)
	return running;
    else 
	return beBoardInterface && beBoard;
}

bool Acquisition::isSavingData() const{
	return running && ((pfilSave!=nullptr && pfilSave->file_open()) || filNewDaq.is_open());
}

void Acquisition::run() {
    numAcq=0;
    software_event_counter = 0;
    transferRate=0;
    triggerRate=0;
    triggerRateInstant=0;
    acqStart.tv_sec= acqStart.tv_usec=0;
    timForceStart.tv_sec= timForceStart.tv_usec=0;
    triggerOneShot=0;
    luEvtCounter=luDestFileSize=lu2ndDestFileSize=luDaqFileSize=0;
    spuriousFrame=false;
    sendForceStart=false;
    running=true;
    paused=pauseToggle=false;
    textDestination = text2ndDestination = false;
    string strDest=pSetStreamer.getStrValue(STREAMER_DEST_FILE);
    if (!strDest.empty()){
	textDestination = boost::algorithm::ends_with(strDest, ".txt");
	FileHeader header(beBoard->getBoardType(), uFwVersion>>8U, uFwVersion&0xF, beBoard->getBeId() ,nbCBC, uPacketSize);
        pfilSave=new FileHandler(strDest.c_str(), 'w', header);//textDestination ? ios_base::out : ios_base::binary);
    }
    //string str2ndDest=pSetStreamer.getStrValue(STREAMER_2ND_DEST_FILE);
    //if (!str2ndDest.empty()){
	//text2ndDestination = boost::algorithm::ends_with(str2ndDest, ".txt");
        //filSave2nd.open(str2ndDest.c_str(), text2ndDestination ? ios_base::out : ios_base::binary);
    //}
    if (!pSetStreamer.getStrValue(STREAMER_NEW_DAQ_FILE).empty())
        filNewDaq.open(pSetStreamer.getStrValue(STREAMER_NEW_DAQ_FILE).c_str(), ios_base::binary);
        
    if (pSetStreamer.getValue(STREAMER_MEMTOFILE))
    	filMemDump.open(getMemDumpFilename().c_str(), ios_base::binary);
    	
    if (!pSetStreamer.getStrValue(STREAMER_DATA_FILE).empty()){
    	string strFile = pSetStreamer.getStrValue(STREAMER_DATA_FILE);
    	pfilSimulatedData=new FileHandler(strFile, 'r');
    	//pSetStreamer.setValue(STREAMER_NB_ACQ,0);//Acquisition will finish with the input file
    	binaryRawSimulation=(strFile.substr(strFile.length()-4,4)==".raw");
    	binaryDaqSimulation=(strFile.substr(strFile.length()-4,4)==".daq");
    }
    //if (pSetSuper.getValue(PARAM_COMMISSIONNING_RQ)) pSetStreamer.setValue(STREAMER_NB_ACQ,1);//Only one acquisition in Commissioning mode
		
    if (pSetStreamer.getValue(STREAMER_COMMISSIONING_LOOP)){
		if (pSetStreamer.getValue(STREAMER_NB_ACQ)==0)
			pSetStreamer.setValue(STREAMER_NB_ACQ,1);
			
		comLoop.initAcq();
		nbCommAcq = nbCommMax = comLoop.getNbIterations() ;
		comLoop.nextAcq(cbcInterface);
		if (pSetStreamer.getValue(STREAMER_DISPLAY_LOG)) appendLog(comLoop.getLog());
    } else {
		nbCommAcq=0;
    }
    if (pSetStreamer.getValue(STREAMER_FAKE_DATA)){
	    //pSetSuper.setValue(PARAM_DATASIZE,0);
	    //pSetStreamer.setValue(STREAMER_ACF_ACQUISITION, 1);
    } 
    nbMaxAcq=pSetStreamer.getValue(STREAMER_NB_ACQ);
    switch (pSetStreamer.getValue(STREAMER_ACF_ACQUISITION)){
    case 1:
	thrAcq = boost::thread(&Acquisition::start, this, pSetStreamer.getValue(STREAMER_DISPLAY_FLAGS), pSetStreamer.getValue(STREAMER_DISPLAY_LOG));	
	break;
    case 2:
	pSetCondition.setValue(STREAMER_ACQ_MODE, 	 pSetStreamer.getValue(STREAMER_ACQ_MODE));
	pSetCondition.setValue(STREAMER_ZERO_SUPPRESSED, pSetStreamer.getValue(STREAMER_ZERO_SUPPRESSED));
	thrAcq = boost::thread(&Acquisition::startAcf, this, pSetStreamer.getValue(STREAMER_DISPLAY_FLAGS), pSetStreamer.getValue(STREAMER_DISPLAY_LOG));
	break;
    //default: visitor.init(&filSave, &filSave2nd, this); beBoardInterface->StartThread(beBoard, nbMaxAcq, &visitor);
    } 
    if (arrVisu){
	delete arrVisu;
	arrVisu=NULL;
	delete pEventCopy;
	delete pEventCopy2;
	pEventCopy=pEventCopy2=NULL;
    }
}
	/// GLIB configuration method called by BoardSupervisor to initialise the acquisition
void Acquisition::configure(bool bLog) {
    boost::posix_time::milliseconds pause(pSetStreamer.getValue(STREAMER_LONG_PAUSE));
    if (bLog)
        initLog("Initialising acquisition");
    
    std::vector < std::pair< std::string , uint32_t > > pRegVec;
	    //aclr enabled
	    pRegVec.push_back({"pc_commands.PC_config_ok", 1});
	    //init handshake
	    pRegVec.push_back({"pc_commands.SRAM1_end_readout", 0});
	    pRegVec.push_back({"pc_commands.SRAM2_end_readout", 0});
	    pRegVec.push_back({"ctrl_sram.sram1_user_logic", 1});//#sram1 user -- 0: ipbus, 1: user
	    pRegVec.push_back({"ctrl_sram.sram2_user_logic", 1});//#sram2 user -- 0: ipbus, 1: user
	    // beBoardInterface->WriteBoardMultReg( beBoard, pRegVec);
	    boost::this_thread::sleep(pause);
	    if (bLog)
		    appendLog("Init done");
    pSetSuper.readAllValuesAndWriteIntoBoard(NULL, true);

    if (beBoard->getBoardType()=="GLIB"){
	    pRegVec.clear();
	    pRegVec.push_back({"pc_commands.SPURIOUS_FRAME", 0});
	    pRegVec.push_back({"pc_commands2.force_BG0_start", 0});
	    pRegVec.push_back({"cbc_acquisition.CBC_TRIGGER_ONE_SHOT", 0});
	    pRegVec.push_back({"break_trigger", 0});
	    beBoardInterface->WriteBoardMultReg( beBoard, pRegVec);
    }
    if (bLog)
        appendLog("Config done.");
}

void Acquisition::initConfiguration() { 
	nbCBC = nbTotalCbc();//temporary
	uPacketSize = std::max(nbCBC, (uint32_t)4)*9+6;//in 32 bits words
	cout<<"packet size="<<uPacketSize<<", nbCbc="<<nbCBC<<endl;
}
///Compute the number of CBC data blocks in data format from the board
uint32_t Acquisition::nbTotalCbc(){
	int iFE, nbFE=beBoard->getNFe();//nbFrontEnd(pSetConfig);
	int nbCBC=beBoard->fModuleVector[0]->getNCbc();//pSetConfig.getValue(CONFIG_HYBRID_TYPE);
	uint32_t nbTot=beBoard->getNCbcDataSize();

	if (nbTot==0)	
		for (iFE=0; iFE<(nbFE==0 ? 1:nbFE); iFE++)
			if (pSetFeCbc.getValue(toolbox::toString(CONFIG_FE_ENABLED, iFE))==1)
				nbTot+=nbCBC;	
	
	return nbTot;
}

void Acquisition::loadFlags(){
	statusFlags = beBoardInterface->ReadBoardReg(beBoard,"status_flags");
	if (beBoard->getBoardType()=="CTA")
		statusFlags = (statusFlags & 0x0F000000) | (statusFlags & 0xF0)<<24 | beBoardInterface->ReadBoardReg(beBoard, "event_counter_SRAM1");
}

void Acquisition::start(bool bFlags, bool bLog) {
    boost::posix_time::milliseconds wait(pSetStreamer.getValue(STREAMER_SHORT_PAUSE));
    bool bFake = pSetStreamer.getValue(STREAMER_FAKE_DATA)==1;
    bool bBreakTrigger = pSetStreamer.getValue(STREAMER_BREAK_TRIGGER)==1;
    uint32_t lVal;
    uint32_t nbErr=0;
    uint32_t nbPackets=	pSetSuper.getValue(PARAM_DATASIZE)+1;
    char bufWrite[ nbPackets * uPacketSize * 4];//temporary
    timeval acqEnd={0,0}, acqPrevious={0,0};
    //	bool b5555=true;
    std::string strFull, strRam, strUserLogic, strReadout;
    std::vector< uint32_t > lData;
    std::vector < std::pair< std::string , uint32_t > > vecReg;

	if (pSetStreamer.getValue(STREAMER_CONDITION_DATA))
		readI2CValuesForConditionData();

    if (bLog) appendLog((boost::format("nbPackets=%d")%nbPackets).str());
    if (!bFake)
	beBoardInterface->Start( beBoard );
/*{
	    beBoardInterface->WriteBoardReg(beBoard, PC_CONFIG_OK, 1);//to allow the DIO5 initialization
	    boost::this_thread::sleep(wait); 
	    vecReg.push_back(make_pair(BREAK_TRIGGER, 0));
	    vecReg.push_back(make_pair(FORCE_BG0_START,1));
	    beBoardInterface->WriteBoardMultReg( beBoard, vecReg);
	    do{//Wait for start acknowledge
		    lVal=beBoardInterface->ReadBoardReg(beBoard, CMD_START_VALID);
		    if (lVal==0)
			    boost::this_thread::sleep(wait);
	    } while (running && lVal==0 && !bFake); 
    }*/
    while (running && (nbMaxAcq==0 || numAcq<nbMaxAcq)) {
        strFull = (boost::format("flags.SRAM%d_full") % (numAcq%2+1)).str();
        strRam = (boost::format("sram%d") % (numAcq%2+1)).str();
        strUserLogic =  (boost::format("ctrl_sram.sram%d_user_logic") % (numAcq%2+1)).str();
        strReadout= (boost::format("pc_commands.SRAM%d_end_readout") % (numAcq%2+1)).str();
        // "Waiting CBC DATA..."
        do {
        	if (pauseToggle){
        		pauseToggle=false;
        		paused=!paused;
        		beBoardInterface->WriteBoardReg(beBoard,"break_trigger", paused ? 1 : 0);
			if (bLog) appendLog(paused ? "PAUSE": "RESUME");
        	}
        	if (!paused && !bFake){
		    vecReg.clear();
	            lVal = beBoardInterface->ReadBoardReg(beBoard,strFull);//read Full flag until 1
	            if (spuriousFrame) {
	                spuriousFrame=false;
			vecReg.push_back({"pc_commands.SPURIOUS_FRAME", 0});
	            }
	            if (triggerOneShot==2){
	            	vecReg.push_back({"cbc_acquisition.CBC_TRIGGER_ONE_SHOT", 0});
	            	triggerOneShot=1;
	            } else if (triggerOneShot==1){
	            	vecReg.push_back({"cbc_acquisition.CBC_TRIGGER_ONE_SHOT", 0});
	            	triggerOneShot=0;
	        	}
	            if (sendForceStart) {
	            	if (timForceStart.tv_sec==0){
	            		gettimeofday(&timForceStart, NULL);
		                vecReg.push_back({"pc_commands2.force_BG0_start", 0});
		                appendLog("write start=1");
	            	} else {
					gettimeofday(&acqEnd, NULL);
					if (timForceStart.tv_sec+1<acqEnd.tv_sec 
							|| (timForceStart.tv_sec+1==acqEnd.tv_sec && timForceStart.tv_usec<acqEnd.tv_usec)){
			            	sendForceStart=false;
					vecReg.push_back({"pc_commands2.force_BG0_start", 0});
			                appendLog("write start=0");
					}
	            	}
	            }
    		    if (!vecReg.empty()) beBoardInterface->WriteBoardMultReg( beBoard, vecReg);
        	}//if (!paused)
            if ((paused || lVal==0))// && !bFake)
                boost::this_thread::sleep(wait);
        } while (lVal==0 && running && !bFake);
        if (!running)
            break;

        if (bLog && !bFake) appendLog((boost::format("%s=%d")%strFull%lVal).str());
        if (acqStart.tv_sec==0){//Heure de debut pour le calcul du taux de transfert
            gettimeofday(&acqStart, NULL);
            acqPrevious=acqStart;
	    	if (bLog) appendLog((boost::format("Start time: %d s, %d us")%acqStart.tv_sec%acqStart.tv_usec).str());
        }
	if (!bFake){
		if (bBreakTrigger){
			beBoardInterface->WriteBoardReg( beBoard, "break_trigger", 1);
			if (bLog) appendLog("PAUSE");
		}
		beBoardInterface->WriteBoardReg( beBoard, strUserLogic, 0);// write User Logic 0
		//    	boost::this_thread::sleep(wait);
		if (bLog)
			appendLog((boost::format("%s<=0. blockRead(%s)")%strUserLogic%strRam).str());

		//Workaround of the bug256
		lData = beBoardInterface->ReadBlockBoardReg(beBoard, strRam, nbPackets*uPacketSize);//Board::readBlock256AndDispatch(board, strRam, nbPackets*uPacketSize);
		beBoardInterface->WriteBoardReg( beBoard, strUserLogic, 1);// write User Logic 1
		//board->dispatch();//Mandatory or else sramX_full remains to 1
		beBoardInterface->WriteBoardReg( beBoard, strReadout, 1);// write Readout 1
		if (bFlags)
			lVal = beBoardInterface->ReadBoardReg(beBoard,"status_flags");
	}
        saveData(lData, bufWrite, bLog, nbPackets);
        //		checkData(lData, bVerif, &b5555, &nbErr);
        if (bFlags) {
            statusFlags = lVal;
        }
	if (!bFake){
		if (bLog) appendLog((boost::format("Wait for %s")%strFull).str());
		do {
			lVal = beBoardInterface->ReadBoardReg(beBoard,strFull);//read full flag until 0
			if (lVal==1)
				boost::this_thread::sleep(wait);
		} while (lVal==1 && running && !bFake && !pauseToggle && !paused);
		beBoardInterface->WriteBoardReg( beBoard,strReadout, 0);// write readout 0
		if (bLog)
			appendLog((boost::format("%s=%d. Acq number=%d. Evt counter=%d")%strFull%lVal%numAcq%((lData.size()>4 ? lData[4] : 0)&0x00FFFFFF)).str()); 
	}
	if (running){ 
		numAcq++;
		if (!bFake && nbCommAcq>1 && numAcq==nbMaxAcq){
			paused=true;
			comLoop.nextAcq(cbcInterface);
			if (bLog) appendLog(comLoop.getLog());
			if (pSetStreamer.getValue(STREAMER_CONDITION_DATA))
				readI2CValuesForConditionData();

			beBoardInterface->Stop(beBoard);
			nbCommAcq--;
			numAcq=0;
			beBoardInterface->Start(beBoard);
			togglePause();
		} else if (bBreakTrigger && (nbMaxAcq==0 || numAcq<nbMaxAcq)){
			beBoardInterface->WriteBoardReg( beBoard, "break_trigger", 0);
			if (bLog) appendLog( "RESUME");
		}
	}
        if (acqEnd.tv_sec!=0)	
        	acqPrevious=acqEnd;
        	
	gettimeofday(&acqEnd,NULL);
	triggerRateInstant= 1e+6 * nbPackets / ((acqEnd.tv_sec - acqPrevious.tv_sec)*1e+6 + (acqEnd.tv_usec - acqPrevious.tv_usec));
    }//while
    if (!bFake)
	beBoardInterface->Stop(beBoard);
/*{
	    vecReg.clear();
	    vecReg.push_back(make_pair(BREAK_TRIGGER, 1));
	    vecReg.push_back(make_pair(PC_CONFIG_OK, 0));
	    beBoardInterface->WriteBoardMultReg( beBoard, vecReg); 
    }*/
    closeFiles();
    gettimeofday(&acqEnd,NULL);
    if (bLog)
        appendLog((boost::format("Acquisition %s (%d s, %d us)")%(running ? "finished" : "stopped")%acqEnd.tv_sec%acqEnd.tv_usec).str());

    if (nbErr)
        appendLog((boost::format("Data errors detected: %d")%nbErr).str());

    running=false;
    transferRate= numAcq * nbPackets * uPacketSize * 32e+6 /
                ((acqEnd.tv_sec - acqStart.tv_sec)*1e+6 + (acqEnd.tv_usec - acqStart.tv_usec));
    triggerRate= numAcq*1e+6 * nbPackets / 
				((acqEnd.tv_sec - acqStart.tv_sec)*1e+6 + (acqEnd.tv_usec - acqStart.tv_usec));
}//start(...)

///Close files after acquisition (raw data, memory dump, simulated data)
void Acquisition::closeFiles(){
    stopSaving();
    if (filMemDump.is_open()){
    	filMemDump.flush();
    	filMemDump.close();
    }
    delete pfilSimulatedData;
    pfilSimulatedData=nullptr;
}

using namespace Ph2_HwDescription;
using namespace Ph2_HwInterface;
using namespace Ph2_System; 


void Acquisition::startAcf(bool bFlags, bool bLog){
    timeval acqEnd={0,0}, acqPrevious={0,0};
    boost::posix_time::milliseconds wait(pSetStreamer.getValue(STREAMER_SHORT_PAUSE));
    uint32_t nbPackets=	pSetSuper.getValue(PARAM_DATASIZE)+1;
    //uint32_t uBufSize = nbPackets*uPacketSize;
    //uint32_t luMaxSize=pSetStreamer.getValue(STREAMER_DEST_MAX_SIZE)<<20UL;
    //uint32_t uRatio = pSetStreamer.getValue(STREAMER_2ND_DEST_RATIO);
    bool bBreakTrigger = pSetStreamer.getValue(STREAMER_BREAK_TRIGGER)==1;
    bool bVisu = pSetStreamer.getValue(STREAMER_DATA_VISU);
    bool bFake = pSetStreamer.getValue(STREAMER_FAKE_DATA);
    bool bOneAfterPause=false;
    bool bCTA = (beBoard->getBoardType()=="CTA");
    std::vector< uint8_t > cbcData ;
    uint32_t uFeMask = (1 << beBoard->getNFe() ) - 1;
    uint32_t uCbcMask = pSetFeCbc.getValue(toolbox::toString(CONFIG_FE_HEADER, 0));
    Ph2_HwInterface::Data dataSimu;

    if (pSetStreamer.getValue(STREAMER_CONDITION_DATA))
		readI2CValuesForConditionData();

    beBoardInterface->SetFileHandler(beBoard, textDestination ? nullptr : pfilSave);
    if (bLog) appendLog((boost::format("nbPackets=%d")%nbPackets).str());
    gettimeofday(&acqStart, NULL);
    if (!bFake) beBoardInterface->Start( beBoard );
    while (nbMaxAcq==0 || numAcq<=nbMaxAcq) {
	if (pauseToggle){
		pauseToggle=false;	   
		paused=!paused;
		bOneAfterPause=true;
		if (paused)
			beBoardInterface->Pause(beBoard);
		else 
			beBoardInterface->Resume(beBoard);

		if (bLog) appendLog(paused ? "pause" : "resume");
	}
	if (paused){
	    if (bOneAfterPause){
		bOneAfterPause=false;
		if (bLog) appendLog("After pause");
	    } else {
                boost::this_thread::sleep(wait);
		if (running){
			continue;
		} else
			break;
	   }
	}

	if (sendForceStart && !bFake) {
		beBoardInterface->SetForceStart(beBoard, true);
		appendLog("write start=1");
	}
	if (bFake){
		dataSimu.Set( beBoard, pfilSimulatedData->readFileChunks(uPacketSize*nbPackets), nbPackets, false, false);
		if (dataSimu.GetEvents(beBoard).size()==0) 
			break;
	} else
		beBoardInterface->ReadData( beBoard,  bBreakTrigger );

	if (bLog) appendLog((boost::format("Acq number=%d")%numAcq).str()); 
	if (bFlags && !bFake)
		statusFlags = beBoardInterface->ReadBoardReg(beBoard,"status_flags");

    	if ((pfilSave && pfilSave->file_open()) || bVisu || filNewDaq.is_open()) {//write data to file
                for (const Ph2_HwInterface::Event *cEvent = bFake ? dataSimu.GetNextEvent(beBoard) : beBoardInterface->GetNextEvent( beBoard ); cEvent; cEvent = bFake ? dataSimu.GetNextEvent(beBoard) : beBoardInterface->GetNextEvent( beBoard )){
			software_event_counter++;
			//if (bLog) appendLog(toolbox::toString("Event #%d, size: %d", cEvent->GetEventCount(), cEvent->GetSize()));
			
                        //if (bVisu || (filSave.is_open() && !textDestination) || (!text2ndDestination && filSave2nd.is_open() && numAcq%uRatio==0)) cEvent->GetEventBytes(cbcData);

			if (pfilSave && pfilSave->file_open() && textDestination){
					pfilSave->fBinaryFile<<*cEvent ;
					luDestFileSize+=TEXT_EVENT_APPROXIMATE_SIZE;//approximate size of a text event
			}// else {
			//filSave.write(reinterpret_cast<const char*>(cbcData.data()), cEvent->GetSize()*4);
			//luDestFileSize+=cEvent->GetSize();
			//}
			if (filNewDaq.is_open() || (sharedMem && ruDescriptor_)){
				char arrSize[4];
				TrackerEvent evtTracker (cEvent, beBoard->fModuleVector[0]->getNCbc(), uFeMask, uCbcMask, false, &pSetCondition );
				evtTracker.fillArrayWithSize (arrSize);
				if (filNewDaq.is_open()){
					filNewDaq.write (arrSize, 4);
					filNewDaq.write (evtTracker.getData(), evtTracker.getDaqSize() );
					filNewDaq.flush();
				}
				if (sharedMem && ruDescriptor_){
					FRLData datFrl(evtTracker.getData(),evtTracker.getDaqSize());
					toolbox::mem::Reference* pRef=datFrl.generateReference(sharedMem, software_event_counter);
					sharedMem->updateEventNumber(pRef, software_event_counter);
					sharedMem->forwardBuffer(application_, pRef, ruDescriptor_);
				}
			}
			if (bVisu)
				addDataVisu(cEvent /*reinterpret_cast<const char*>(cbcData.data())*/, bLog);
		}//for
		//if (luMaxSize>0 && luDestFileSize > luMaxSize){
			//incrementDestinationFile(filSave, STREAMER_DEST_FILE);
			//luDestFileSize=0;
		//}
		//if (luMaxSize>0 && lu2ndDestFileSize > luMaxSize){
			//incrementDestinationFile(filSave2nd, STREAMER_2ND_DEST_FILE);
			//lu2ndDestFileSize=0;
		//}
	}
	if (sendForceStart && !bFake) {
		beBoardInterface->SetForceStart(beBoard, false);
		appendLog("write start=0");
		sendForceStart=false;
	}
        if (acqEnd.tv_sec!=0)	
        	acqPrevious=acqEnd;
        	
	gettimeofday(&acqEnd,NULL);
	triggerRateInstant= 1e+6 * nbPackets / ((acqEnd.tv_sec - acqPrevious.tv_sec)*1e+6 + (acqEnd.tv_usec - acqPrevious.tv_usec));
	if (running) 
		numAcq++;
	else 
		break;
	
	if ( !bFake && nbCommAcq>1 && numAcq==(bCTA?nbMaxAcq+1:nbMaxAcq)){
		if (!paused){//!bCta
			beBoardInterface->Pause(beBoard);
			paused=true;
		}
		if (bVisu){
			comLoop.storeVisuData(arrVisu, software_event_counter);
			delete arrVisu;
			arrVisu=nullptr;
		}
		comLoop.nextAcq(cbcInterface);
		if (bLog) appendLog(comLoop.getLog());
		if (pSetStreamer.getValue(STREAMER_CONDITION_DATA))
			readI2CValuesForConditionData();
		nbCommAcq--;
		numAcq=0;
		togglePause();
	} else if (numAcq==nbMaxAcq){
		if ((!bFake && nbCommAcq>1) || bCTA) 
			togglePause();
		else 
			break;
	}
    }//while
    if (!bFake) 
	beBoardInterface->Stop( beBoard);
	
    if (bVisu && nbCommMax>0 && !bFake) 
	    comLoop.storeVisuData(arrVisu, software_event_counter);
    
    closeFiles();
    gettimeofday(&acqEnd,NULL);
    running=false;
    double dblDuration=((acqEnd.tv_sec - acqStart.tv_sec)*1e+6 + (acqEnd.tv_usec - acqStart.tv_usec));
    transferRate= numAcq * nbPackets * uPacketSize * 32e+6 / dblDuration;
    triggerRate= numAcq*1e+6 * nbPackets / dblDuration;
    autoExit(dblDuration);	
}//startAcf

void Acquisition::stop() {
    if (running) {
        running=false;
	try {
           thrAcq.join();
	} catch (exception & e) {
		cout << "Death to Stop in Acquisition::stop()" << e.what() << endl;
	} catch (...) {
	   cout << "Death to Stop in Acquisition::stop(). failed to perform thrAcq.join()" << endl;
	}
	//if ( beBoardInterface && beBoard && beBoardInterface->isRunningThread(beBoard)) beBoardInterface->StopThread(beBoard);
    }
}

//void Acquisition::breakTriggers(bool bBreak){
//    board->getNode("break_trigger").write(bBreak ? 1 : 0);
//    board->dispatch();
//}

void Acquisition::togglePause(){
	pauseToggle=true;
}

uint32_t Acquisition::frequenceMetre() {
    uint32_t val1, val2;
    //test freq cdr_clk from TTC_FMC via V6
    val1 = beBoardInterface->ReadBoardReg(beBoard,"freq_ttc_fmc");
    boost::posix_time::seconds pause(1);
    boost::this_thread::sleep(pause);
    val2 = beBoardInterface->ReadBoardReg(beBoard,"freq_ttc_fmc");
    return val2-val1;
}

//void Acquisition::setLongPause(uint32_t uPause) {
//    longPause = uPause;
//}

//void Acquisition::setShortPause(uint32_t uPause) {
//    shortPause = uPause;
//}

//void Acquisition::setNbAcq(uint32_t uNb){
//	nbAcq=uNb;
//}

//void Acquisition::setDataSize(uint32_t uDataSize) {
//    dataSize = uDataSize;
//}

//void Acquisition::setDestinationFile(const std::string& strFile) {
//    destFile = strFile;
//}

void Acquisition::setBoard( Ph2_HwInterface::BeBoardInterface* pbbi, Ph2_HwInterface::CbcInterface *pcbci, Ph2_HwDescription::BeBoard* pbb){
    //board=iBoard;	
    beBoard = pbb;
    beBoardInterface = pbbi;
    cbcInterface = pcbci;
    pSetSuper.setBoard(pbbi, pbb);
    //pSetConfig.setBoard(pbbi, pbb);
    pSetFeCbc.setBoard(pbbi, pbb);
    /*if (beBoard){
	    pSetConfig.setValue(CONFIG_NB_FE,beBoard->getNFe());//temporary
	    pSetConfig.setValue(CONFIG_HYBRID_TYPE,beBoard->fModuleVector[0]->getNCbc());//8);
    }*/
}

void Acquisition::setResetSelection(uint32_t uSel){
	resetSelection = uSel;
}

void Acquisition::setTriggerMode(uint32_t uMode){
	triggerMode= uMode;
}

void Acquisition::setTriggerCyclicFrequency(uint32_t uFreq){
	triggerCyclicFrequency = uFreq;
}

std::string Acquisition::getMemDumpFilename() const{
	std::string strFile;
	const char* tmpDir = getenv("TMPDIR");
	if (tmpDir)
		strFile.assign(tmpDir);
	else		
		strFile.assign("/tmp");
		
	return strFile.append("/memDump.dat");
}

void Acquisition::sendSpuriousFrame() {
    spuriousFrame=true;
}

void Acquisition::sendTriggerOneShot() {
    triggerOneShot=2;
}

uint32_t Acquisition::getNumAcq() {
    //if (pSetStreamer.getValue(STREAMER_ACF_ACQUISITION)<3)
	return numAcq;
    //else return (beBoardInterface && beBoard) ? beBoardInterface->getNumAcqThread(beBoard) : 0;
}
///Save data to destination file and/or send it to Read Unit
void Acquisition::saveData(std::vector< uint32_t > lData, char* bufWrite, bool bLog, uint32_t nbPackets) {
	int iOct, size=sizeof(uint32_t);//4
    bool bFake = pSetStreamer.getValue(STREAMER_FAKE_DATA);
    bool bVisu = pSetStreamer.getValue(STREAMER_DATA_VISU);
    if (!bFake && lData.size()< nbPackets * uPacketSize){
	appendLog(toolbox::toString("Error: data vector size is %d instead of %d!", lData.size(), nbPackets * uPacketSize));
	return;
    }
    Ph2_HwInterface::Data dataEvt;
    if (bVisu){
	dataEvt.Set( beBoard, lData, nbPackets, false, false);
    }
    for (uint32_t uPack=0; uPack<nbPackets; uPack++) {
    	if (bFake){//Fake data
    		memset(bufWrite, uPack*uPacketSize, uPacketSize*size);
    		if (isDataFileOpen()){
    			if (!readSimulatedDataFromFile(bufWrite, uPack)){
    				running=false;
    				return;
    			}
//    			break;
    		} else
    			memcpy(&bufWrite[(uPack*uPacketSize+5)*size], simulationData, NB_SIMULATION_BYTES);
    			
		nbL1A++;
		nbCbcData++;
	} else {//Acquisition data
		for (uint32_t sLig=0; sLig<uPacketSize; sLig++) {//copy 32-bit words into bytes
			//cout << hex << "!Addr " << sLig << " : ";
			for (iOct=0; iOct<size; iOct++){
				bufWrite[(sLig+uPack*uPacketSize)*size+iOct]=(lData[(sLig+uPack*uPacketSize)]>>((size-1-iOct)*8))&255;
				//cout << ((lData[(sLig+uPack*uPacketSize)]>>((size-1-iOct)*8))&255) << " ";
			}
			//cout << endl;
		}
		nbBunchCrossing	= lData[uPack*uPacketSize + 0]&0x00FFFFFF;
		nbOrbit 		= lData[uPack*uPacketSize + 1]&0x00FFFFFF;
		nbLumiSection 	= lData[uPack*uPacketSize + 2]&0x00FFFFFF;
		nbL1A 			= lData[uPack*uPacketSize + 3]&0x00FFFFFF;
		nbCbcData 		= lData[uPack*uPacketSize + 4]&0x00FFFFFF;
	}
	software_event_counter++;
        sendData(bufWrite, (uPack*uPacketSize)*size, bLog);
	if (bVisu){
		addDataVisu(dataEvt.GetEvent(beBoard, uPack), bLog);
	}
    }//for
    if (!bFake)
	    dataFlags		= lData[(nbPackets-1)*uPacketSize + 9];
	
    if (pfilSave && pfilSave->file_open()) {//write data to file
        pfilSave->fBinaryFile.write(bufWrite, nbPackets*uPacketSize*size);
        pfilSave->fBinaryFile.flush();
	luDestFileSize+=nbPackets*uPacketSize*size;
	//uint64_t luMaxSize=pSetStreamer.getValue(STREAMER_DEST_MAX_SIZE)<<20UL;
	//if (luMaxSize>0 && luDestFileSize > luMaxSize){
		//incrementDestinationFile(filSave, STREAMER_DEST_FILE);
		//luDestFileSize=0;
	//}
        if (bLog)
            appendLog((boost::format("Write %d bytes to file: %s") %(nbPackets*uPacketSize*size) % pSetStreamer.getStrValue(STREAMER_DEST_FILE)).str());
	
    }
}
/// Send one event to read unit via shared memory
void Acquisition::sendData(const char * buffer, uint32_t index, bool bLog) {
	if (sharedMem || filNewDaq.is_open()){
		DAQData *pdatDaq;
		FedEvent* pEvtFed;
		if (binaryDaqSimulation){
			pEvtFed = new SimulatedEvent(buffer+DAQ_HEADER_SIZE, uPacketSize*4-DAQ_HEADER_SIZE-DAQ_TRAILER_SIZE);
			pdatDaq = new DAQData((SimulatedEvent*)pEvtFed, buffer);
		} else {
			pEvtFed = new FedEvent2020(pSetStreamer.getValue(STREAMER_ACQ_MODE),
						pSetStreamer.getValue(STREAMER_ZERO_SUPPRESSED), 
						pSetStreamer.getValue(STREAMER_CONDITION_DATA),
						pSetStreamer.getValue(STREAMER_FAKE_DATA),//temporary
						getEnabledFrontEnd(pSetConfig, pSetFeCbc),//1 //temporary
						beBoard->fModuleVector[0]->getNCbc(),
						pSetFeCbc.getValue(toolbox::toString(CONFIG_FE_HEADER, 0)),//0xFF,//pSetSuper.getValue(EXPECTED_CBC),//temporary
						&pSetCondition,
						buffer, index + 5*sizeof(uint32_t) );
			pdatDaq = new DAQData(pEvtFed, nbL1A, nbBunchCrossing);
			//write new format data to file
			if (filNewDaq.is_open()){
				char arrSize[4];
				uint32_t uSize= pdatDaq->getDaqSize();
				arrSize[0]=uSize & 255; arrSize[1] = (uSize>>8)&255; arrSize[2] = (uSize>>16)&255; arrSize[3] = (uSize>>24)&255;
				filNewDaq.write(arrSize, 4);
				filNewDaq.write(pdatDaq->getHeaderAddress(),DAQ_HEADER_SIZE);
				filNewDaq.write(pdatDaq->getDataAddress(), pEvtFed->getSize());
				filNewDaq.write(pdatDaq->getTrailerAddress(),DAQ_TRAILER_SIZE);
				filNewDaq.flush();
                                //uint64_t luMaxSize=pSetStreamer.getValue(STREAMER_DEST_MAX_SIZE)<<20UL;
				//uint32_t uRatio = pSetStreamer.getValue(STREAMER_2ND_DEST_RATIO);
                                luDaqFileSize+=pEvtFed->getSize()+4+DAQ_HEADER_SIZE+DAQ_TRAILER_SIZE;
				//if (luMaxSize>0 && luDaqFileSize > luMaxSize){
					//incrementDestinationFile(filNewDaq, STREAMER_NEW_DAQ_FILE);
					//luDaqFileSize=0;
				//}
				/*if (filSave2nd.is_open() && numAcq%uRatio==0){
					filSave2nd.write(arrSize, 4);
					filSave2nd.write(pdatDaq->getHeaderAddress(),DAQ_HEADER_SIZE);
					filSave2nd.write(pdatDaq->getDataAddress(), pEvtFed->getSize());
					filSave2nd.write(pdatDaq->getTrailerAddress(),DAQ_TRAILER_SIZE);
					filSave2nd.flush();
					lu2ndDestFileSize+=pEvtFed->getSize()+4+DAQ_HEADER_SIZE+DAQ_TRAILER_SIZE;
					if (luMaxSize>0 && lu2ndDestFileSize > luMaxSize){
						incrementDestinationFile(filSave2nd, STREAMER_2ND_DEST_FILE);
						lu2ndDestFileSize=0;
					}
				}*/
			}
		}
		if (sharedMem){
			FRLData datFrl(pdatDaq);
			uint32_t evtCnt = pSetStreamer.getValue(STREAMER_USE_HARDWARE_COUNTER) ? nbCbcData : software_event_counter; 
			toolbox::mem::Reference* pRef=datFrl.generateReference(sharedMem, evtCnt);
			if (ruDescriptor_){	
				sharedMem->updateEventNumber(pRef,evtCnt);
				sharedMem->forwardBuffer(application_, pRef, ruDescriptor_);
			} 
			else {
				lstRef.push_front(pRef);
				if (lstRef.size()>=65535){
					try {
						if(lstRef.back()) //Added by Jonni, try not to operate on null pointers!!!
							lstRef.back()->release();
						else
							cout << "lstRef pointer at end of list is NULL!" << endl;
					} catch (exception & e) {
						cout << "Exception Caught when trying to release Shared memory. This pointer points to previously deallocated memory: " << e.what() << endl; 
					} catch (...) {
						cout << "Exception Caught when trying to release Shared memory. This pointer points to previously deallocated memory: " << endl;
					}
					lstRef.pop_back();
				}
			}
			if (filMemDump.is_open()){ 
				filMemDump.write((char *) sharedMem->getUserAddress(pRef), datFrl.getFrlSize());
			}
			if (bLog) 
				appendLog(toolbox::toString("Event # %d sent: %d bytes",  evtCnt, datFrl.getFrlSize()));
		}
		delete pEvtFed;
                delete pdatDaq;
	}
}

void Acquisition::addDataVisu(const Ph2_HwInterface::Event* pEvent /*char * buffer, uint32_t idxBuf*/, bool bLog) {
	uint32_t uFeId, uCbcId, uOct, uVal, uType = pSetStreamer.getValue(STREAMER_DATA_VISU);
	bool bLogThis=true;
	if (arrVisu==NULL){
		uint32_t nbBin=NB_STRIPS_CBC2;
		arrVisu = new uint32_t[nbBin]; 
		memset(arrVisu, 0, sizeof(uint32_t)*nbBin);
	}
	if (uType==1){//TDC
		//uVal=buffer[idxBuf+ uPacketSize*4-1];//packet last byte
		arrVisu[pEvent->GetTDC()]++;
	} else if (uType==2){//Error bits
		uOct=0;
		for (uFeId=0; uFeId<beBoard->getNFe(); uFeId++){
			for (uCbcId=0; uCbcId<beBoard->fModuleVector[uFeId]->getNCbc(); uCbcId++){
				uVal=pEvent->Error(uFeId, uCbcId);
				uOct++;
				if (uVal>0){
					arrVisu[uOct*10+ uVal]++;
					if (bLog) appendLog(toolbox::toString("Event #%d, CBC #%d: Error bits=%d", software_event_counter, uOct, uVal));
				}
			}
		}
	} else if (uType==3){//bad counters
		uint64_t luNewCounter=pEvent->GetLumi();//(buffer[idxBuf+2*sizeof(uint32_t)+3]&255U) | ((buffer[idxBuf+2*sizeof(uint32_t)+2]&255U)<<8U) | ((buffer[idxBuf+2*sizeof(uint32_t)+1]&255U)<<16U);
		luNewCounter<<=30U;
		luNewCounter|=	pEvent->GetOrbit()<<12U | pEvent->GetBunch();
//(buffer[idxBuf+0*sizeof(uint32_t)+3]&255U) | ((buffer[idxBuf+0*sizeof(uint32_t)+2]&255U)<<8U) | (buffer[idxBuf+1*sizeof(uint32_t)+3]&255U)<<12U | ((buffer[idxBuf+1*sizeof(uint32_t)+2]&255U)<<20U) | ((buffer[idxBuf+1*sizeof(uint32_t)+1]&255U)<<28U);

		if (luNewCounter<luEvtCounter){
			arrVisu[2]++;
			if (bLog) appendLog(toolbox::toString("Event #%d: Bunch, Orbit and Lumi counters less than expected", software_event_counter));
		} else 
			luEvtCounter=luNewCounter;
 
		uint32_t cntL1A=pEvent->GetEventCount();//(buffer[idxBuf+3*sizeof(uint32_t)+3]&255U) | ((buffer[idxBuf+3*sizeof(uint32_t)+2]&255U)<<8U) | ((buffer[idxBuf+3*sizeof(uint32_t)+1]&255U)<<16U));
		uint32_t cntCbc=pEvent->GetEventCountCBC();//(buffer[idxBuf+4*sizeof(uint32_t)+3]&255U) | ((buffer[idxBuf+4*sizeof(uint32_t)+2]&255U)<<8U) | ((buffer[idxBuf+4*sizeof(uint32_t)+1]&255U)<<16U));
		if (software_event_counter != cntL1A ){
			arrVisu[0]++;
			if (bLog) appendLog(toolbox::toString("Event #%d: L1A=%d", software_event_counter, cntL1A));
		} 
		if (software_event_counter != cntCbc ){
			arrVisu[1]++; 
			if (bLog) appendLog(toolbox::toString("Event #%d: CBC=%d", software_event_counter, cntCbc));
		}
	} else if (uType==4){//Identical events
		if (pEventCopy==NULL){//Copy of first event content
			pEventCopy=new Ph2_HwInterface::Event(*pEvent);
			return;
		}
		if(!(*pEventCopy==*pEvent)){
			arrVisu[0]++;
			if (bLog && bLogThis){
				bLogThis=false;
				appendLog(toolbox::toString("Event #%d: is different than first", software_event_counter));
			}
		}
	} else if (uType==5){//Identical events every 2
		if (pEventCopy==NULL){
			pEventCopy=new Ph2_HwInterface::Event(*pEvent);
			return;
		} else if (pEventCopy2==NULL){
			pEventCopy2=new Ph2_HwInterface::Event(*pEvent);
			return;
		} else if (software_event_counter%2==1 && !(*pEventCopy==*pEvent)){
			arrVisu[0]++;
			if (bLog && bLogThis){
				bLogThis=false;
				appendLog(toolbox::toString("Event #%d: is different than first", software_event_counter));
			}
		} else if (software_event_counter%2==0 && !(*pEventCopy2==*pEvent)){
			arrVisu[1]++;
			if (bLog && bLogThis){
				bLogThis=false;
				appendLog(toolbox::toString("Event #%d: is different than second", software_event_counter));
			}
		}
	} else if (uType>=100){//CBC
		uFeId=(uType-100)/beBoard->fModuleVector[0]->getNCbc();
		uCbcId=(uType-100)%beBoard->fModuleVector[0]->getNCbc();
		for (uOct=0; uOct<NB_STRIPS_CBC2; uOct++){
			if (pEvent->Bit(uFeId, uCbcId, uOct))
				arrVisu[uOct]++;
		}
	}
}

	/** Read one line of simulated data from file into buffer
	 * @return true if data was read and false if end of file was reached
	 */
bool Acquisition::readSimulatedDataFromFile(char* buffer, uint32_t uPack){
	int iOct, sizInt=sizeof(uint32_t);
	if (binaryRawSimulation || binaryDaqSimulation){
		if (pfilSimulatedData->fBinaryFile.eof() && pSetStreamer.getValue(STREAMER_READ_NEXT_FILES)>0)
			incrementInputFile(pfilSimulatedData->fBinaryFile, STREAMER_DATA_FILE);

		if (!pfilSimulatedData->fBinaryFile.eof()){
                        if (binaryDaqSimulation){
                                uint32_t uSize;
                                pfilSimulatedData->fBinaryFile.read(buffer, 4);
				if (pfilSimulatedData->fBinaryFile.eof() && pSetStreamer.getValue(STREAMER_READ_NEXT_FILES)>0 && incrementInputFile(pfilSimulatedData->fBinaryFile, STREAMER_DATA_FILE)){
					pfilSimulatedData->fBinaryFile.read(buffer, 4);
				}
				if (pfilSimulatedData->fBinaryFile.good()){
					uSize=(buffer[0]&255) | ((buffer[1]&255)<<8U) | ((buffer[2]&255)<<16U) | ((buffer[3]&255)<<24U);
					uPacketSize=uSize / sizInt;//The size of each event is written before it
				//cout<<"DAQ file "<<pSetStreamer.getStrValue(STREAMER_DATA_FILE)<<", size="<<uSize<<endl;
				}
                        }
			if (uPacketSize>0)
				pfilSimulatedData->fBinaryFile.read(&buffer[uPack*uPacketSize*sizInt], uPacketSize*sizInt);

			if (binaryRawSimulation)
				nbBunchCrossing=((buffer[uPack*uPacketSize*sizInt+1]&255)<<16U) |  ((buffer[uPack*uPacketSize*sizInt+2]&255)<<8U) | (buffer[uPack*uPacketSize*sizInt+3]&255);

			if (pfilSimulatedData->fBinaryFile.eof() && pSetStreamer.getValue(STREAMER_READ_NEXT_FILES)>0)
				incrementInputFile(pfilSimulatedData->fBinaryFile, STREAMER_DATA_FILE);

			return pfilSimulatedData->fBinaryFile.good();
		} else
			return false;
	} else {//Text simulation
		for (iOct=0; iOct<sizInt; iOct++){ 
			buffer[(uPack*uPacketSize+3)*sizInt+iOct]=(nbL1A	>>((sizInt-1-iOct)*8))&255;
			buffer[(uPack*uPacketSize+4)*sizInt+iOct]=(nbCbcData>>((sizInt-1-iOct)*8))&255;
		}
		return readSimulatedDataFromTextFile(&buffer[(uPack*uPacketSize+5)*sizInt]);
	}
}
/** Read fake data from text file and convert it into input data format
 * @return true if at least one line was read from text file
 */
bool Acquisition::readSimulatedDataFromTextFile(char* buffer){
	string strLine;
	unsigned int iCar, iLen, iLine=0;
	uint32_t cbcSize = NB_STRIPS_CBC2/8+4;//36
	while (!pfilSimulatedData->fBinaryFile.eof()){
		getline(pfilSimulatedData->fBinaryFile, strLine);
		iLen = strLine.length();
		if (strLine[0]!='#' && iLen>0){
			for (iCar=0; iCar<iLen && iCar<NB_STRIPS_CBC2; iCar++)
				if (strLine[iCar]=='x' || strLine[iCar]=='s')
					break;
				else if (strLine[iCar]=='1'){//strip values
					//Old GLIB format (tracker1.1.glibv3)//	iOct = (iCar<10) ? 19-iCar/8 : 15-(iCar-10)/8;//iBit = (iCar<10) ? iCar%8 : (iCar-10)%8;//buffer[iOct] |= (1<<iBit);
					buffer[(cbcSize*(iLine+1) - 4 - iCar/8)] |= (1<<(iCar%8));
				}
				
			if (iLen>iCar+2 && strLine.at(iCar)=='x'){//CBC Status
				stringstream ss;
				uint32_t uStatus;
				ss<<std::hex<<strLine.substr(iCar+1,2);
				ss>>uStatus;
//				appendLog(toolbox::toString("Before buffer[%d]=0x%X", cbcSize*iLine, buffer[cbcSize*iLine]));
//				appendLog(toolbox::toString("Before buffer[%d]=0x%X", cbcSize*iLine+1, buffer[cbcSize*iLine+1]));
				buffer[cbcSize*iLine+1] |= (uStatus&0x03)<<6;
				buffer[cbcSize*iLine+0] |= (uStatus&0xFC)>>2;
//				appendLog(toolbox::toString("buffer[%d]=0x%X", cbcSize*iLine, buffer[cbcSize*iLine]));
//				appendLog(toolbox::toString("buffer[%d]=0x%X", cbcSize*iLine+1, buffer[cbcSize*iLine+1]));
				iCar+=3;
			}
			if (iLen>iCar+1 && strLine[iCar]=='s'){//Stub bit
				if (strLine[iCar+1]=='1')
					buffer[cbcSize*(iLine+1) - 1 -iCar/8]=0x01;
					
				iCar+=2;
				if (iLen>iCar+1){//Error bits (2)
					if (strLine[iCar]=='1')
						buffer[cbcSize*iLine + 0] |= 0x40;
						
					if (strLine[iCar+1]=='1')
						buffer[cbcSize*iLine + 0] |= 0x80;
				}
			}			
			iLine++;
			appendLog(toolbox::toString("%s (%d)",strLine.c_str(), iLen));
			if (iLine==nbCBC)
				break;
		}
	}//while
	return iLine>0;
}
	///Check that the data is AAAAAAAA 55555555... and display error messages in log if it is not.
void Acquisition::checkData(std::vector< uint32_t > lData, bool bVerif, bool *pb5555, uint32_t *pNbErr) {
    if (bVerif) {
        string strLine;
        if (*pb5555) {
            if (lData[1]!=0x55555555 || lData[2]!=0x55555555 ||lData[3]!=0x55555555
                    ||lData[4]!=0x55555555 ||lData[5]!=0x555) {
                if (numAcq) {
                    (*pNbErr)++;
                    appendLog((boost::format("Data is: %X %X %X %X %X instead of 0x5555...")
                               %lData[1]%lData[2]%lData[3]%lData[4]%lData[5]).str());
                }
                if (lData[1]==0xAAAAAAAA && lData[2]==0xAAAAAAAA && lData[3]==0xAAAAAAAA
                        && lData[4]==0xAAAAAAAA && lData[5]==0x6AA)
                    return;
            }
        } else if (lData[1]!=0xAAAAAAAA || lData[2]!=0xAAAAAAAA ||lData[3]!=0xAAAAAAAA
                   ||lData[4]!=0xAAAAAAAA ||lData[5]!=0x6AA) {
            (*pNbErr)++;
            appendLog((boost::format("Data is: %X %X %X %X %X instead of 0xAAAA...")
                       %lData[1]%lData[2]%lData[3]%lData[4]%lData[5]).str());
            if (lData[1]==0x55555555 && lData[2]==0x55555555 && lData[3]==0x55555555
                    && lData[4]==0x55555555 && lData[5]==0x555)
                return;
        }
        *pb5555 = not *pb5555;
    }
}

void Acquisition::initLog(const std::string& strInit) {
    if (pSetStreamer.getValue(STREAMER_DISPLAY_LOG)) {
        strLog="0 ms: ";
        strLog.append(strInit).append("\n");
        gettimeofday(&logStart, NULL);
        logEnd.tv_sec=logEnd.tv_usec=0;
    } else
        strLog="";
}

void Acquisition::appendLog(const std::string& strAppend) {
    //	using namespace boost::chrono;
    std::ostringstream ostream;
    timeval logPrev={logEnd.tv_sec, logEnd.tv_usec};
    gettimeofday(&logEnd,NULL);
    uint32_t diff = (logEnd.tv_sec - logStart.tv_sec)*1000 + (logEnd.tv_usec - logStart.tv_usec)/1000;
    ostream<<diff<<" ms: ";
    if (logPrev.tv_sec){ 
    	diff = (logEnd.tv_sec - logPrev.tv_sec)*1000 + (logEnd.tv_usec - logPrev.tv_usec)/1000;
    	ostream<<"(+"<<diff<<" ms) ";
    }
    ostream<<strAppend<<std::endl;
    strLog.append(ostream.str());
}

std::string Acquisition::getCountersAsHtml(const char* small) {
    ostringstream ostream;

    ostream<<"<table border='1'><tr><th colspan='2'>Counters<small>"<<small<<"</small></th></tr>";
    ostream<<"<tr><td>Bunch Crossing</td><td align='right'>"<<nbBunchCrossing<<"</td></tr>"<<endl;
    ostream<<"<tr><td>Orbit</td><td align='right'>"<<nbOrbit<<"</td></tr>"<<endl;
    ostream<<"<tr><td>Lumi-Section</td><td align='right'>"<<nbLumiSection<<"</td></tr>"<<endl;
    ostream<<"<tr><td>L1A</td><td align='right'>"<<nbL1A<<"</td></tr>"<<endl;
    ostream<<"<tr><td>CBC Data</td><td align='right'>"<<nbCbcData<<"</td></tr>"<<endl;
    ostream<<"<tr><td>Software counter</td><td align='right'>"<<software_event_counter<<"</td></tr>"<<endl;
    ostream<<"</table>";
    return ostream.str();
}


void Acquisition::setSharedMemory(FRLA *mem, xdaq::ApplicationDescriptor *ruDesc, xdaq::Application *app) {
    sharedMem = mem;
    ruDescriptor_=ruDesc;
    application_ = app;
    pSetStreamer.setValue(STREAMER_SHARED_MEMORY, ruDescriptor_ ? 1 : 0);
}


void Acquisition::forceStart()
{
    time_t tim;
    time(&tim);
    lastStartTime=ctime(&tim);
    if (isRunning()){
    	sendForceStart=true;
    	timForceStart.tv_sec=0;
    } else {
	    beBoardInterface->SetForceStart( beBoard, 1);
	    sleep(1);
	    beBoardInterface->SetForceStart( beBoard, 0);
    }
}

void Acquisition::stopSaving(){
    if (pfilSave && pfilSave->file_open()) {
        delete pfilSave;
        pfilSave=nullptr;
    }
    //if (filSave2nd.is_open()) {
        //filSave2nd.flush();
        //filSave2nd.close();
    //}
    if (filNewDaq.is_open()) {
        filNewDaq.flush();
        filNewDaq.close();
    }
}

void Acquisition::startSaving(){
	//incrementDestinationFile(filSave, STREAMER_DEST_FILE);
	//incrementDestinationFile(filSave2nd, STREAMER_2ND_DEST_FILE);
	incrementDestinationFile(filNewDaq, STREAMER_NEW_DAQ_FILE);
}
///Increment a destination file name by incrementing the number found after last point in file name or adding '.1'
void Acquisition::incrementDestinationFile(std::ofstream& filDest, const std::string& strParam){
	if (filDest.is_open()){
		filDest.flush();
		filDest.close();
	}
	string destFile = pSetStreamer.getStrValue(strParam);
	ifstream infile(destFile.c_str());
	while (infile.good()){
		infile.close();
		size_t found = destFile.rfind(".");
		if (found==string::npos)
			destFile.append(".1");
		else{
			int iNum = atoi(destFile.substr(found+1).c_str());
			if (iNum){
				destFile = destFile.substr(0, found).append(".");
				destFile.append(toolbox::toString("%d",iNum+1));
			} else 
				destFile.append(".1");
		}
		infile.open(destFile.c_str(), ios_base::in);
	}
	filDest.open(destFile.c_str(), ios_base::binary);
	pSetStreamer.setValue(strParam, destFile);
}
bool Acquisition::incrementInputFile(std::fstream&  filInput, const std::string& strParam){
	if (filInput.is_open())
		filInput.close();

	string strFile = pSetStreamer.getStrValue(strParam);
	size_t found = strFile.rfind(".");
	if (found==string::npos)
		strFile.append(".1");
	else{
		int iNum = atoi(strFile.substr(found+1).c_str());
		if (iNum){
			strFile = strFile.substr(0, found).append(".");
			strFile.append(toolbox::toString("%d",iNum+1));
		} else 
			strFile.append(".1");
	}
    	filInput.open(strFile.c_str(), ios_base::in); 
	if (filInput.good())
		pSetStreamer.setValue(strParam, strFile);

	return filInput.good();
}
///Read from board CBCs the I2C configuration values needed for condition data
void Acquisition::readI2CValuesForConditionData(){
	uint32_t uCond, numFE, numCBC;//, numPage;
	//vector<uint32_t> vecReq;
	//I2cCommunication i2cComm("cbc_reg_i2c_settings", "cbc_reg_i2c_command", "cbc_reg_i2c_reply", 0x5B);
	//vecReq.clear();
	for (uCond=0; uCond<NB_CONDITION_DATA; uCond++){//first loop pass
		if (pSetCondition.getValue((boost::format(CONDITION_DATA_ENABLED)%uCond).str())==1
				&& pSetCondition.getValue((boost::format(CONDITION_DATA_TYPE)%uCond).str())==1 ){//FE configuration parameter
			numFE =  pSetCondition.getValue((boost::format(CONDITION_DATA_FE_ID)%uCond).str());
			numCBC = pSetCondition.getValue((boost::format(CONDITION_DATA_CBC)%uCond).str());
			//numPage = pSetCondition.getValue((boost::format(CONDITION_DATA_PAGE)%uCond).str());
//		cout<<"Value to be read: CBC "<<(numCBC&0x0F)<<", page "<<(numCBC>>4)<<", register "<<pSetCondition.getValue((boost::format(CONDITION_DATA_REGISTER)%uCond).str())<<endl;
			Module* module= beBoard->getModule(numFE);
			if (module){
				Cbc*    pCbc = module->getCbc(numCBC);
				if (pCbc){
					uint8_t uVal = cbcInterface->ReadCbcReg(pCbc, pSetCondition.getStrValue((boost::format(CONDITION_DATA_NAME)%uCond).str()));
					pSetCondition.setValue((boost::format(CONDITION_DATA_VALUE)%uCond).str(), uVal);
				}
			}
			//i2cComm.blockAdd(vecReq, numFE, numCBC, numPage, pSetCondition.getValue((boost::format(CONDITION_DATA_REGISTER)%uCond).str()), 0);
		}
	}
	/*if (!vecReq.empty()){
		i2cComm.blockInit(board);
		i2cComm.blockSendRequest(board, vecReq, false);		
		std::vector< uint32_t > lData = i2cComm.blockWaitAndReadResult(board, vecReq.size());
		
		std::vector< uint32_t>::const_iterator ite = lData.begin();
		for (uCond=0; uCond<NB_CONDITION_DATA; uCond++){//second loop pass with same tests to set values
			if (pSetCondition.getValue((boost::format(CONDITION_DATA_ENABLED)%uCond).str())==1
					&& pSetCondition.getValue((boost::format(CONDITION_DATA_TYPE)%uCond).str())==1 ){
			 	pSetCondition.setValue((boost::format(CONDITION_DATA_VALUE)%uCond).str(), (*ite)&0xFF);
//						cout<<"Read value: "<<((*ite)&0xFF)<<endl;
				ite++;
			}
		}
	}*/
}

void Acquisition::initFeCbcParameterSet(){
	int iFE, nbFE=beBoard->getNFe();//Board::nbFrontEnd(pSetConfig);
	int iCBC, nbCBC=beBoard->fModuleVector[0]->getNCbc();//pSetConfig.getValue(CONFIG_HYBRID_TYPE);
	
	if (beBoard->getBoardType()!="GLIB" && beBoard->getBoardType()!="CTA")
		return;
 
	pSetConfig.setValue(CONFIG_NB_FE, nbFE);
	pSetConfig.setValue(CONFIG_HYBRID_TYPE, nbCBC);
	pSetFeCbc.clearValues();
	for (iFE=0; iFE<nbFE; iFE++){
//		if (nbFE==1) iFE=1;			
		pSetFeCbc.setValue(toolbox::toString(CONFIG_FE_ENABLED, iFE),1);
		pSetFeCbc.setValue(toolbox::toString(CONFIG_FE_HEADER, iFE),(1U<<nbCBC)-1);//temporary
		for (iCBC=0; iCBC<nbCBC; iCBC++)
			pSetFeCbc.setValue(toolbox::toString(CONFIG_FE_CBC_ENABLED, iFE, iCBC), 1);
	}		
}
/// get Front End status: one bit per enabled FE
uint64_t Acquisition::getEnabledFrontEnd(ParameterSetUhal& pSetConfig, ParameterSetUhal& pSetFeCbc){
	uint64_t uRet=0;
	uint32_t uFE, nbFE=beBoard->getNFe();//nbFrontEnd(pSetConfig);
	for (uFE=0; uFE<nbFE; uFE++)
		uRet |= pSetFeCbc.getValue(toolbox::toString(CONFIG_FE_ENABLED, uFE))<<uFE;
		
	return uRet;
}

bool Acquisition::isDestFileOpen() const 
{ return pfilSave && pfilSave->file_open();}
	
bool Acquisition::isDataFileOpen() const { 
	return pfilSimulatedData && pfilSimulatedData->file_open();
}

void Acquisition::setFwVersion(uint32_t uVer){
	uFwVersion=uVer;
}

void Acquisition::autoExit(double dblDur){
	if (bAutoExit){
		cout<<"Auto exit after: "<<dblDur/1e6<<" s, "<<numAcq<<" acq. loops, transfer rate: "<<transferRate<<" bit/s, trigger rate: "<<triggerRate<<" Hz, packet size: "<<(pSetSuper.getValue(PARAM_DATASIZE)+1)<<endl;
		exit(0);
	}
}
