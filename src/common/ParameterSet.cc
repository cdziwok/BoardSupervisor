/* Copyright 2013 Institut Pluridisciplinaire Hubert Curien
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

   FileName : 		ParameterSet.cc
   Content : 		Set of parameters written into board
   Used in : 		
   Programmer : 	Christian Bonnin
   Version : 		1.0
   Date of creation 13/06/2013
   Support : 		mail to : christian.bonnin@iphc.cnrs.fr
*/

#include <stdio.h>
#include <stdlib.h>
#include <ostream>
#include <boost/algorithm/string.hpp>
#include <boost/regex.hpp>
#include "xdata/Integer.h"
#include "xdata/Boolean.h"
#include "xdata/Vector.h"

#include "uhal/uhal.hpp"
#include "toolbox/string.h"
#include "ParameterSet.h"
#include "HWInterface/BeBoardInterface.h"
#include "HWDescription/BeBoard.h"

using namespace std;

ParameterSetUhal::ParameterSetUhal(){
	beBoardInterface=NULL;
	beBoard=NULL;
}

uint32_t ParameterSetUhal::setValue(const std::string& strName, uint32_t uVal){
	ParamSet::setValue(strName, uVal);
	if (mapSerializable.find(strName)!=mapSerializable.end()){
	    if (mapSerializable[strName]->type()=="bool")
		((xdata::Boolean*)mapSerializable[strName])->value_=(uVal!=0);
	    else 
		((xdata::Integer*)mapSerializable[strName])->value_=uVal;
	}
		//mapSerializable[strName]->fromString( toolbox::toString("%d", uVal));

	return uVal;
}
///Set a parameter value (string)
void ParameterSetUhal::setValue(const std::string& strName, const std::string& strVal){
	ParamSet::setValue(strName, strVal);
	if (mapSerializable.find(strName)!=mapSerializable.end())
		mapSerializable[strName]->fromString( strVal );
}

uint32_t ParameterSetUhal::setFromGui(cgicc::Cgicc& cgi, const std::string& strName, HTML_CONTROL_TYPE type){
	string strHtmlName = boost::replace_all_copy(strName,".","_");
	uint32_t uRet=0;
    
	if (type == CHECKBOX)
		uRet= setValue(strName, cgi.queryCheckbox(strHtmlName) ? 1 : 0);
	else{
		cgicc::form_iterator iteVal = cgi.getElement(strHtmlName);
	 	if (iteVal != cgi.getElements().end()){
	 		if (type==HEXA)
	 			uRet= setValue(strName, strtol(iteVal->getValue().c_str(), NULL, 16));
	 		else if (type==INTEGER || type==COMBOBOX)
				uRet= setValue(strName, iteVal->getIntegerValue());
			else if (type==CHARS){
				string strVal=iteVal->getValue();
				uRet= setValue(strName,  (strVal.length()>3 ? strVal.at(3) : 0) 
								| ((strVal.length()>2 ? strVal.at(2) : 0)<<8)
								| ((strVal.length()>1 ? strVal.at(1) : 0)<<16)
								| ((strVal.length()>0 ? strVal.at(0) : 0)<<24));
			} else//TEXTFIELD
				setValue(strName, iteVal->getValue());
	 	} else 
			cout<<"no HTML component named "<<strHtmlName<<endl;
	}

	return uRet;
}

void ParameterSetUhal::writeIntoBoard(const std::string& strName){//, bool bDispatch){
	if (beBoard && beBoardInterface && beBoardInterface->getUhalNode(beBoard, strName).getPermission()!=uhal::defs::READ)
	{//NodePermission: do not write into readonly nodes 
		beBoardInterface->getUhalNode(beBoard, strName).write(getValue(strName));
			
	//if (board!=NULL && board->getNode(strName).getPermission()!=uhal::defs::READ){//NodePermission: do not write into readonly nodes 
		//board->getNode(strName).write(getValue(strName));
//		cout<<"Write "<<getValue(strName)<<" into "<<strName<<endl;
		//if (bDispatch) board->dispatch();
	}
}
	/**Create an HTML description string of the component with the parameter value.
	 * @param strName parameter name
	 * @param ctrlType component type (checkbox, combobox, text field)
	 * @param strArg additionnal HTML argument to be added to the component main tag*/
std::string ParameterSetUhal::htmlDescription(const std::string& strName, HTML_CONTROL_TYPE ctrlType, const std::string& strArg) {
    ostringstream ostream;
	string strDesc;
	//if (board!=NULL)
	if (beBoard && beBoardInterface)
	    try{
		strDesc = beBoardInterface->getUhalNode(beBoard, strName).getDescription();
	    } catch (exception& e){
		strDesc = "-";
	    }
			
	switch (ctrlType){
	case INTEGER: 
		ostream<<toolbox::toString("<input type='text' value='%d' name='%s' size='6' title='%s' %s/>",
			getValue(strName), boost::replace_all_copy(strName,".","_").c_str(), strDesc.c_str(), strArg.c_str());
		break;
	case TEXTFIELD: 
		ostream<<toolbox::toString("<input type='text' value='%s' name='%s' size='6' title='%s' %s/>",
			getStrValue(strName).c_str(), boost::replace_all_copy(strName,".","_").c_str(), strDesc.c_str(), strArg.c_str());
		break;
	case HEXA:
		ostream<<"<input type='text' value='"<< std::hex << getValue(strName) << "' name='" << boost::replace_all_copy(strName,".","_");
		ostream<<"' size='6' title='" << strDesc << "' "<< strArg << "/>";
		break;
	case CHECKBOX:
	    ostream<<toolbox::toString("<input type='checkbox' name='%s' title='%s' %s %s/>",
	    	boost::replace_all_copy(strName,".","_").c_str(), strDesc.c_str(), 
	    	getValue(strName)!=0 ? "checked" : "", strArg.c_str());
	    	
	    break;
	case COMBOBOX:
		ostream<<toolbox::toString("<select name='%s' %s>", boost::replace_all_copy(strName,".","_").c_str(), strArg.c_str());
		insertOptionsForNode(ostream, strName);			 
		ostream<<"</select>";
		break;
	case CHARS:
		uint32_t uVal = getValue(strName);
		ostream<<toolbox::toString("<input type='text' value='%c%c%c%c' name='%s' size='6' title='%s' %s/>",
			(uVal>>24)&255, (uVal>>16)&255, (uVal>>8)&255, uVal&255, 
			boost::replace_all_copy(strName,".","_").c_str(), strDesc.c_str(), strArg.c_str());
		break;
	}
    return ostream.str();
}

//void ParameterSetUhal::setBoard(uhal::HwInterface *iBoard) {
void ParameterSetUhal::setBoard(Ph2_HwInterface::BeBoardInterface* pbbi, Ph2_HwDescription::BeBoard* pbb){
	beBoardInterface=pbbi;    
	beBoard=pbb;
}

void ParameterSetUhal::readAllFromBoard(){
	//map< string, uhal::ValWord<uint32_t> > mapValid;
	std::vector < std::pair< std::string , uint32_t > > vecReg;

	if (!(beBoard && beBoardInterface))
		return;
		
	for (map<string, uint32_t>::const_iterator it=mapIntValues.begin(); it!=mapIntValues.end(); it++)
		if (beBoardInterface->getUhalNode(beBoard, it->first).getPermission()!=uhal::defs::WRITE){
			//cout<<"read node "<<it->first<<endl;
			vecReg.push_back(make_pair(it->first, 0));
			//mapValid[it->first] = beBoardInterface->getUhalNode(it->first).read();
		}

	if (!vecReg.empty())
		beBoardInterface->ReadBoardMultReg(beBoard, vecReg);
		
	//for (map<string, uhal::ValWord<uint32_t> >::iterator itVal=mapValid.begin(); itVal!=mapValid.end(); itVal++)
	for (std::vector < std::pair< std::string , uint32_t > >::const_iterator itVal=vecReg.begin(); itVal!=vecReg.end(); itVal++)
		setValue(itVal->first, itVal->second);
}
///Read all HTML component names from the form file, add them to the maps, then read the values from the board.
void ParameterSetUhal::readHtmlFormAndValues(const std::string& strPath){
	ifstream fHtml(strPath.c_str());
	string strLig, strName;
	size_t iPos=0, iName, iEnd;
	vector<string> lstNodes = beBoardInterface->getHardwareInterface(beBoard)->getNodes();
	
	mapIntValues.clear();
	while (fHtml.good()){
		getline(fHtml, strLig);
		do{
			iName = nextHtmlComponentName(strLig, strName, &iPos, &iEnd);
			if (iName!=string::npos){
				for(vector<string>::iterator it = lstNodes.begin(); it != lstNodes.end(); ++it) {
					if (!strName.compare(*it))
						setValue(strName, 0);
				}
			}			
		}while(iName!=string::npos);
	}
	fHtml.close();
	readAllFromBoard();
}
	/** Read HTML form file and return its content with HTML components filled by parameter values
	 * @param strPath HTML form file path
	 * @param strLoadSave end of the action name (to be added to 'save' and 'load') of the HTML form containing buttons to save parameter values and load them from a file.*/
std::string ParameterSetUhal::transformHtmlFileDescription(const std::string& strPath, const std::string& strLoadSave){
	ifstream fHtml(strPath.c_str());
	if (!fHtml.good()){
		ostringstream ostream;
		ostream<<"Error while loading file: "<<strPath<<"<br/>"<<endl;
		return ostream.str();
	} else {
		string strRet = transformHtmlFormDescription(fHtml, strPath, strLoadSave);
		fHtml.close();
		return strRet;
	}
}
///Transform an HTML form content to fill components with parameter values
std::string ParameterSetUhal::transformHtmlFormDescription(std::istream& isForm, const std::string& strPath, const std::string& strLoadSave){
	ostringstream ostream;
	string strLig, strName, strType, strStripName;
	size_t iPos=0, iName, iEnd, iPrevEnd, iPar;
	
	if (!strLoadSave.empty()){
		ostream<<"<table><tr><td>";
		ostream<<"<form action='save"<<strLoadSave<<"'><input type='submit' value='Save to file'/></form> "<<endl;
		ostream<<"</td><td>";
		ostream<<"<form action='load"<<strLoadSave<<"'><input type='submit' value='Load from file'/>";
		ostream<<"<input type='text' size='40' name='htmlValuesFile'/></form>"<<endl;
		ostream<<"</td></tr></table>"<<endl;
	}
	while (isForm.good()){
		bool bNoName=true;
		getline(isForm, strLig);
		iPrevEnd=0;
		do{
			iName = nextHtmlComponentName(strLig, strName, &iPos, &iEnd);
			strStripName= strName;
			if ( (iPar=strName.find('('))!=string::npos)
				strStripName=strName.substr(0, iPar);//name without parenthesis and text after
				
			if (iName!=string::npos && (mapIntValues.find(strStripName)!=mapIntValues.end() 
										|| mapStrValues.find(strStripName)!=mapStrValues.end())){
				if (mapSerializable.find(strStripName)!=mapSerializable.end()){
					setOneValueFromSerializable(strStripName, mapSerializable[strStripName]);
				}
				bNoName=false;			
				strType = componentTypeFromName(strName);
				ostream<<(strLig).substr(iPrevEnd, iName-iPrevEnd);
				if (boost::algorithm::iequals( strType,"checkbox")){
					if (getValue(strStripName)==1)
						ostream<<" checked"; 
				}else if (boost::algorithm::iequals( strType,"string")){
					ostream<<" value='"<<getStrValue(strStripName)<<"'";;
				}else if (boost::algorithm::iequals( strType,"hexa")){
					ostream<<" value='"<<std::hex<<getValue(strStripName)<<"'"<<std::dec;
				}else if (boost::algorithm::iequals( strType,"chars")){
					uint32_t uVal = getValue(strStripName);
					ostream<<" value='"<<(char)((uVal>>24)&255)<<(char)((uVal>>16)&255)<<(char)((uVal>>8)&255)<<(char)(uVal&255);
					ostream<<"'"<<std::dec;
				}else
					ostream<<" value='"<<getValue(strStripName)<<"'";
					
				ostream<<" name='"<<boost::replace_all_copy(strStripName,".","_").c_str()<<"' ";
				if (boost::algorithm::iequals(strType, "select"))
					insertHtmlSelectOptions(ostream, strLig, strName, &iPos, &iEnd);
				
				iPrevEnd=iEnd;
        	}
		}while(iName!=string::npos);
		if (bNoName)
			ostream<<strLig<<endl;
		else
			ostream<<(strLig).substr(iPrevEnd)<<endl;			
	}//while
	ostream<<"<hr title='html file: "<<strPath;
	if (beBoard && beBoardInterface)
		ostream<<", board id:"<<beBoardInterface->getHardwareInterface(beBoard)->id()<<", uri:"
			<<beBoardInterface->getHardwareInterface(beBoard)->uri();
		
	ostream<<"'/>"<<endl;
	return ostream.str();
}	
/** Search the next HTML component name (name=) in a string and return its position 
 * @param strLig HTML line
 * @param strName string modified to contain the HTML component hame
 * @param piPos position in the line to begin the search. Will be modified
 * @param piEnd Will be modified to the end position of the string name='...'
 */ 
size_t ParameterSetUhal::nextHtmlComponentName(const std::string& strLig, std::string& strName, size_t *piPos, size_t* piEnd){
	char cQuote;
	size_t iName = boost::algorithm::to_lower_copy(strLig).find("name=", *piPos);
	size_t iDebName;
	if (iName!=string::npos && strLig.length()>iName+5){
		iDebName=iName+5;
		cQuote= strLig.at(iDebName);
		if (cQuote!='"' && cQuote!='\'')
			cQuote=' ';//Name without quote
		else 
			iDebName++;
			
		*piEnd=strLig.find(cQuote, iDebName);
		if (*piEnd==string::npos){//Name without quote at the end of the line
			strName=strLig.substr(iDebName);
			*piPos = strLig.length();
		}else{
			strName=strLig.substr(iDebName, *piEnd - iDebName);
			(*piEnd)++;
			*piPos = *piEnd;
		}
	} else if (iName!=string::npos)
		*piPos=iName+1;//should not happen because the name should be on the same line as 'name='
	else
		*piPos=0;
					
	return iName;
}
///Insert an HTML select (combobox) component into an output stream
void ParameterSetUhal::insertHtmlSelectOptions(ostringstream& ostream, 
		const std::string& strLig, const std::string& strName, size_t *piPos, size_t *piEnd){
	boost::regex patNoEnd("/ *>", boost::regex::icase);
	boost::regex patSelectEnd(">< */ *select *>", boost::regex::icase);
	boost::sregex_iterator iteNoEnd(strLig.begin()+*piEnd, strLig.end(), patNoEnd);
	boost::sregex_iterator iteSelectEnd(strLig.begin()+*piEnd, strLig.end(), patSelectEnd);
    boost::sregex_iterator iteEnd;
    if (iteNoEnd!=iteEnd && (iteSelectEnd==iteEnd || iteNoEnd->position()<iteSelectEnd->position())){//case "<select ... />"
    	ostream<<strLig.substr(*piEnd, iteNoEnd->position())<<">";//The position is given from the beginning string iterator!
    	insertOptionsForNode(ostream, strName);
    	*piEnd+=iteNoEnd->position() + iteNoEnd->length();
    	ostream<<"</select>"<<endl;
    } else if (iteSelectEnd!=iteEnd){//case "<select ...></select>"
    	ostream<<strLig.substr(*piEnd, iteSelectEnd->position())<<">";
    	insertOptionsForNode(ostream, strName);
    	*piEnd+=iteSelectEnd->position() + iteSelectEnd->length();
    	ostream<<"</select>"<<endl;
    }
    *piPos = *piEnd;
}
///Insert the option tags of an HTML select (choices of a combobox)
void ParameterSetUhal::insertOptionsForNode(std::ostringstream& ostream, const std::string& strName){
	string strStripName = strName;
	string strDesc=descriptionFromName(strName);
	vector<string> arrOption;
	size_t iPar=strName.find('(');
	
	if ( iPar!=string::npos)
		strStripName=strName.substr(0, iPar);	
		
	boost::split(arrOption, strDesc, boost::is_any_of(","));
	uint32_t uVal =  getValue(strStripName);
	for (vector<string>::iterator it=arrOption.begin(); it!=arrOption.end(); it++)
		if (!boost::starts_with(boost::algorithm::to_lower_copy(*it), "default:") && !(*it).empty()) 
			ostream<<toolbox::toString("<option %s>%s</option>", uVal == ((uint32_t)atoi( (*it).c_str() ))  ? "selected":"", (*it).c_str());
}

std::string ParameterSetUhal::descriptionFromName(const std::string& strName){
	if (beBoard && beBoardInterface)
		return beBoardInterface->getUhalNode(beBoard, strName).getDescription();
		
	size_t ferme=strName.find(')');
	if (ferme==string::npos)
		return "";
	else
		return strName.substr(ferme+1);
}

std::string ParameterSetUhal::componentTypeFromName(const std::string& strName){
	if (beBoard && beBoardInterface)
		return beBoardInterface->getUhalNode(beBoard, strName).getTags();
	
	size_t ouvre=strName.find('(');
	if (ouvre!=string::npos){
		size_t ferme=strName.find(')',ouvre);
		if (ferme!=string::npos)
			return strName.substr(ouvre+1, ferme-ouvre-1);
		else
			return strName.substr(ouvre+1);
	}
	return "int";	
}
///Read all parameter values from GUI and write them into the board
void ParameterSetUhal::readAllValuesAndWriteIntoBoard(cgicc::Cgicc* pcgi, bool bWrite){
	for (map<string, uint32_t>::const_iterator it=mapIntValues.begin(); it!=mapIntValues.end(); it++){
		if (pcgi){
			string strTag=beBoardInterface->getUhalNode(beBoard, it->first).getTags();
			if (boost::algorithm::iequals(strTag, "checkbox"))
				setFromGui( *pcgi, it->first, CHECKBOX);
			else if (boost::algorithm::iequals(strTag, "hexa"))
				setFromGui( *pcgi, it->first, HEXA);
			else if (boost::algorithm::iequals(strTag, "chars"))
				setFromGui( *pcgi, it->first, CHARS);
			else 
				setFromGui( *pcgi, it->first, INTEGER);
		}
		if (bWrite)
			writeIntoBoard(it->first);//, false);
	}
	//if (bWrite) board->dispatch();
}
	
void ParameterSetUhal::mapToSerializable(xdaq::Application* pApp, const std::string& strParam, xdata::Serializable* pDataParam){
	pApp->getApplicationInfoSpace()->fireItemAvailable(strParam, pDataParam);
	mapSerializable[strParam]=pDataParam;
}

void ParameterSetUhal::setValuesFromSerializable(){
	for (map<string, xdata::Serializable*>::const_iterator it=mapSerializable.begin(); it!=mapSerializable.end(); it++)
		setOneValueFromSerializable(it->first, it->second);
}

void ParameterSetUhal::setOneValueFromSerializable(const std::string& strName, xdata::Serializable* pSer){
	if (pSer->type()=="string")
		ParamSet::setValue(strName, pSer->toString());
	else if (pSer->type()=="int" || pSer->type()=="unsigned int")
		ParamSet::setValue(strName, (uint32_t)strtol( pSer->toString().c_str(), NULL, 10));
	else if (pSer->type()=="bool")
		ParamSet::setValue(strName, ((xdata::Boolean*)pSer)->value_ ? 1 : 0);
	/*else if (pSer->type()=="vector")
		for (size_t iAt=0; iAt<((xdata::Vector<xdata::UnsignedInteger>*)pSer)->elements(); iAt++)
			setOneValueFromSerializable(strName, ((xdata::Vector<xdata::UnsignedInteger>*)pSer)->elementAt(iAt));*/
}

