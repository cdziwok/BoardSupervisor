/* Copyright 2013 Institut Pluridisciplinaire Hubert Curien
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

   Programmer : 	Christian Bonnin
   Version : 		1.0
   Date of creation:18/09/2013
   Support : 		mail to : christian.bonnin@iphc.cnrs.fr
   FileName : 		Board.cc
   Content : 		Generic board management (GLIB, FC7)
   Used in : 		CBCDAQ
*/
#include "toolbox/string.h"
#include "Board.h"
#include "BoardSupervisor.h"
#include "Utils/pugixml.hpp"
#include "HWInterface/GlibFWInterface.h"
#include "HWInterface/BeBoardInterface.h"
#include "HWInterface/CbcInterface.h"
#include "HWInterface/FpgaConfig.h"
#include "HWDescription/BeBoard.h"
#include "HWDescription/Module.h"
#include "HWDescription/Cbc.h"
#include "HWDescription/Definition.h"
#include "System/SystemController.h"

using namespace std;
using namespace cgicc;

Board::Board(){
	beBoard=NULL;
	beBoardInterface=NULL;
}

Board::~Board(){
	destroy();
}

//void Board::setHardwareInterface(uhal::HwInterface* pBoard){ lBoard = pBoard; }
/** Read the ACF middleware XML configuration file */
void Board::initialize(const std::string& strFile, ParameterSetUhal* pSetSuper){
        systemCtrl=new Ph2_System::SystemController();
	beBoard=NULL;
/*
	uint32_t cShelveId, cBeId, cModuleId;

	pugi::xml_document doc;
	doc.load_file( strFile.c_str() );
	// Iterate the Shelve Nodes
	for ( pugi::xml_node cShelveNode = doc.child( "HwDescription" ).child( "Shelve" ); cShelveNode; cShelveNode = cShelveNode.next_sibling() ){//for each shelve
		cShelveId = cShelveNode.attribute( "Id" ).as_int();
		for ( pugi::xml_node cBeBoardNode = cShelveNode.child( "BeBoard" ); cBeBoardNode; cBeBoardNode = cBeBoardNode.next_sibling() ){//for each board
			beBoard = new Ph2_HwDescription::BeBoard( cShelveId, cBeBoardNode.attribute( "Id" ).as_int() );
			cBeId = cBeBoardNode.attribute( "Id" ).as_int();
			Ph2_HwInterface::BeBoardFWInterface* cBeBoardFWInterface;
			pugi::xml_node cBeBoardFWVersionNode = cBeBoardNode.child( "FW_Version" );
			uint16_t cNCbcDataSize = uint16_t( cBeBoardFWVersionNode.attribute( "NCbcDataSize" ).as_int() );

			beBoard->setNCbcDataSize( cNCbcDataSize );
			if ( std::string( cBeBoardNode.attribute( "boardType" ).value() ).compare( std::string( "Glib" ) ) )
			{
				Ph2_HwInterface::BeBoardFWMap fBeBoardFWMap;                                //!< Map of connections to the BeBoard
				string strUhalConfig = expandEnvironmentVariables(doc.child( "HwDescription" ).child( "Connections" ).attribute( "name" ).value()); 
				std::cout <<"Using uHAL config file: " << strUhalConfig << " with board Id: " << cBeId << std::endl;
				cBeBoardFWInterface = new Ph2_HwInterface::GlibFWInterface(strUhalConfig.c_str() , cBeId);
				fBeBoardFWMap[beBoard->getBeBoardIdentifier()] = cBeBoardFWInterface;
				beBoardInterface = new Ph2_HwInterface::BeBoardInterface( fBeBoardFWMap );
				cbcInterface = new Ph2_HwInterface::CbcInterface( fBeBoardFWMap );
				//lBoard = cBeBoardFWInterface->getHardwareInterface();//new uhal::HwInterface(lConnectionManager.getDevice ( strBoard.toString() )));
				pSetInfo.setBoard(beBoardInterface, beBoard);
				for ( pugi::xml_node cModuleNode = cBeBoardNode.child( "Module" ); cModuleNode; cModuleNode = cModuleNode.next_sibling() )
  				  if ( static_cast<std::string>( cModuleNode.name() ) == "Module" )
				  {// Iterate the module node
				    if (cModuleNode.child("CBC_Files"))
					strI2cDirectory = expandEnvironmentVariables(cModuleNode.child("CBC_Files").attribute("path").value());
				
				    cModuleId = cModuleNode.attribute( "ModuleId" ).as_int();
				    Module* cModule = new Module( cShelveId, cBeId, cModuleNode.attribute( "FMCId" ).as_int(), cModuleNode.attribute( "FeId" ).as_int(), cModuleId );
				    beBoard->addModule( cModule ); 
				    for ( pugi::xml_node cCbcNode = cModuleNode.child( "CBC" ); cCbcNode; cCbcNode = cCbcNode.next_sibling() )
				    {		// Iterate the CBC node
					    std::string cFileName;
					    if ( !strI2cDirectory.empty() )
						    cFileName = strI2cDirectory +"/"+ expandEnvironmentVariables(cCbcNode.attribute( "configfile" ).value());
					    else cFileName = expandEnvironmentVariables(cCbcNode.attribute( "configfile" ).value());

					    Cbc* cCbc = new Cbc( cShelveId, cBeId, cModuleNode.attribute( "FMCId" ).as_int(), cModuleNode.attribute( "FeId" ).as_int(), cCbcNode.attribute( "Id" ).as_int(), cFileName );
					    for ( pugi::xml_node cCbcRegisterNode = cCbcNode.child( "Register" ); cCbcRegisterNode; cCbcRegisterNode = cCbcRegisterNode.next_sibling() )
						    cCbc->setReg( std::string( cCbcRegisterNode.attribute( "name" ).value() ), atoi( cCbcRegisterNode.first_child().value() ) );

					    for ( pugi::xml_node cCbcGlobalNode = cModuleNode.child( "Global_CBC_Register" ); cCbcGlobalNode != cModuleNode.child( "CBC" ) && cCbcGlobalNode != cModuleNode.child( "CBC_Files" ) && cCbcGlobalNode != NULL; cCbcGlobalNode = cCbcGlobalNode.next_sibling() )
					    { //CBC global registers
						    if ( cCbcGlobalNode != NULL ) {
							    std::string regname = std::string( cCbcGlobalNode.attribute( "name" ).value() );
							    cCbc->setReg( regname, cCbcGlobalNode.text().as_int() ) ; 
						    }
					    }
					    beBoard->getModule( cModuleId )->addCbc( cCbc );
				    }//for CBC
				  } // for Module

			}
		}
			
	}*/
        systemCtrl->InitializeHw(strFile);
        beBoardInterface=systemCtrl->fBeBoardInterface;
        cbcInterface=systemCtrl->fCbcInterface;
        beBoard=systemCtrl->fBoardVector[0];
	pSetInfo.setBoard(beBoardInterface, beBoard);

	pugi::xml_document doc;
	doc.load_file( strFile.c_str() );
	for ( pugi::xml_node cBeBoardNode = doc.child( "HwDescription" ).child( "BeBoard" ); cBeBoardNode; cBeBoardNode = cBeBoardNode.next_sibling() ){//for each board
		for ( pugi::xml_node cModuleNode = cBeBoardNode.child( "Module" ); cModuleNode; cModuleNode = cModuleNode.next_sibling() )
			if (cModuleNode.child("CBC_Files"))
				strI2cDirectory = expandEnvironmentVariables(cModuleNode.child("CBC_Files").attribute("path").value());
		if (pSetSuper)
			for ( pugi::xml_node cRegisterNode = cBeBoardNode.child( "Register" ); cRegisterNode; cRegisterNode = cRegisterNode.next_sibling() ){//for each register
				pSetSuper->setValue(cRegisterNode.attribute("name").value(), cRegisterNode.text().as_int());//Register default values
				//cout<<"Register "<<cRegisterNode.attribute("name").value()<<" = "<<cRegisterNode.text().as_int()<<endl;
			}

		break;
	}
}

/// Delete uhal object to disconnect from board
void Board::destroy(){
	//delete beBoard;
	//delete beBoardInterface;
	//delete cbcInterface;
	delete systemCtrl;
	beBoard=NULL; beBoardInterface=NULL; cbcInterface=NULL; 
}

///Load status values from card
void Board::loadStatus() {
    if (beBoardInterface && beBoard){
	std::vector < std::pair< std::string , uint32_t > > pRegVec;
	pRegVec.push_back(make_pair("board_id", 0));
	pRegVec.push_back(make_pair("sys_id", 0));
	pRegVec.push_back(make_pair("firm_id.firmware_major", 0));
	pRegVec.push_back(make_pair("firm_id.firmware_minor", 0));
	/*pRegVec.push_back(make_pair("firm_id.firmware_build", 0));
	pRegVec.push_back(make_pair("firm_id.firmware_dd", 0));
	pRegVec.push_back(make_pair("firm_id.firmware_mm", 0));
	pRegVec.push_back(make_pair("firm_id.firmware_yy", 0));
	pRegVec.push_back(make_pair("status.fmc1_present", 0));
	pRegVec.push_back(make_pair("status.fmc2_present", 0));*/
	beBoardInterface->ReadBoardMultReg( beBoard, pRegVec);
	uint32_t lBoardId=pRegVec[0].second;
	uint32_t lSysId = pRegVec[1].second;
	uFirmwareVersion = (pRegVec[2].second & 0xF)<<8U | (pRegVec[3].second & 0xF);
/*	uint32_t lFirmBuild = pRegVec[4].second;
	uint32_t lFirmD = pRegVec[5].second;
	uint32_t lFirmM = pRegVec[6].second;
	uint32_t lFirmY = pRegVec[7].second;*/
            ostringstream ostream;
            int iCar;
            for (iCar = 3; iCar>=0; iCar--)
                ostream<<(char)(lBoardId>>(iCar*8));

            ostream<<"<br/>";
            ostream<<"0x"<<hex<<lSysId<<dec<<"<br/>"<<endl;
	    //if (lFirmM!=0) ostream<<"20";
	    //ostream<<setw(2)<<setfill('0')<<lFirmY<<"-"<<setw(2)<<lFirmM<<"-"<<setw(2)<<lFirmD<<" v"<<lFirmMajor<<"."<<lFirmMinor<<"."<<lFirmBuild;
            //strGlibVer = ostream.str();
    }
}


void Board::sendResetCommand(int iVal){
	if (beBoard && beBoardInterface){// && beBoardInterface->getBoardType(beBoard)!=BoardType::CTA) {
		beBoardInterface->WriteBoardReg( beBoard, iVal==1 ? "cbc_hard_reset":"cbc_fast_reset",1);
     		usleep(200000);   	
		beBoardInterface->WriteBoardReg( beBoard, iVal==1 ? "cbc_hard_reset":"cbc_fast_reset",0);
        }
}

std::string Board::launchTest(int iNb){
	ostringstream out;
	if (beBoard && beBoardInterface){
		timeval logStart, logEnd;
		
		int iCpt, iErr=0;
		uint32_t  lVal;

		gettimeofday(&logStart, NULL);
		for (iCpt=1; iCpt<=iNb;iCpt++){
			beBoardInterface->ReadBoardReg( beBoard, "sram1");
		}
		gettimeofday(&logEnd,NULL);
	    	uint32_t diff = (logEnd.tv_sec - logStart.tv_sec)*1000 + (logEnd.tv_usec - logStart.tv_usec)/1000;
    		out<<iNb<<" one word reading: "<<diff<<" ms"<<endl;	

		gettimeofday(&logStart, NULL);
		for (iCpt=1; iCpt<=iNb;iCpt++){
			beBoardInterface->WriteBoardReg(beBoard, "sram1",0xFFFFFFFF);
		}			
		gettimeofday(&logEnd,NULL);
	    	diff = (logEnd.tv_sec - logStart.tv_sec)*1000 + (logEnd.tv_usec - logStart.tv_usec)/1000;
    		out<<iNb<<" one word writing: "<<diff<<" ms"<<endl;	

		gettimeofday(&logStart, NULL);
		for (iCpt=1; iCpt<=iNb;iCpt++){
			beBoardInterface->WriteBoardReg(beBoard, "sram1",0xFFFFFFFF);
			lVal = beBoardInterface->ReadBoardReg(beBoard, "sram1");
			if (lVal!=0xFFFFFFFF){
				iErr++;
				if (iErr==1)
					out<<" expected: 0xFFFFFFFF"<<", actual: 0x"<<hex<<lVal<<std::dec<<endl;
			}
		}			
		gettimeofday(&logEnd,NULL);
	    	diff = (logEnd.tv_sec - logStart.tv_sec)*1000 + (logEnd.tv_usec - logStart.tv_usec)/1000;
    		out<<iNb<<" one word writing then reading: "<<diff<<" ms, nbErr="<<iErr<<endl;	
		iErr=0;
		gettimeofday(&logStart, NULL);
		for (iCpt=1; iCpt<=iNb;iCpt++){
			beBoardInterface->WriteBoardReg(beBoard, "sram1",0xFFFFFFFF);
			beBoardInterface->ReadBoardReg(beBoard, "sram1");
			lVal=beBoard->getReg("sram1");
			if (lVal!=0xFFFFFFFF)
				iErr++;
		}			
		gettimeofday(&logEnd,NULL);
	    	diff = (logEnd.tv_sec - logStart.tv_sec)*1000 + (logEnd.tv_usec - logStart.tv_usec)/1000;
    		out<<iNb<<" one word writing then reading (no return): "<<diff<<" ms, nbErr="<<iErr<<endl;	
    } else 
    	out<<"board not initialized!";
    	
    return out.str();
}

void  Board::initCbcFilename(){
	uCbcIterator=0;
}

std::string Board::nextCbcFilename(ParameterSetUhal& pSetFeCbc){
	uint32_t iFE, nbFE=beBoard->getNFe();//nbFrontEnd(pSetConfig);
	uint32_t iCBC, nbCBC=beBoard->fModuleVector[0]->getNCbc();//pSetConfig.getValue(CONFIG_HYBRID_TYPE);

	while (uCbcIterator<nbFE * nbCBC){
		iFE=uCbcIterator/nbCBC;
		iCBC=uCbcIterator%nbCBC;	
//		if (nbFE==1) iFE=1;
			
		if (pSetFeCbc.getValue(toolbox::toString(CONFIG_FE_ENABLED, iFE))==1){
			uCbcIterator++;
			if (pSetFeCbc.getValue(toolbox::toString(CONFIG_FE_CBC_ENABLED,iFE, iCBC))==1){
				ostringstream ostream;
				ostream<<"FE"<<iFE<<"CBC"<<iCBC;
				return ostream.str();
			}
		} else 
			uCbcIterator+=nbCBC;
	}
	return "";
}
///Number of Front End depending on the CONFIG_NB_FE parameter
uint32_t Board::nbFrontEnd(ParameterSetUhal& pSetConfig){
	switch (pSetConfig.getValue(CONFIG_NB_FE)){
		case 0 : case 1: return 1;
		default: return 2;
	}
}
	
///Insertion of HTML check boxes for CBC and FE enabling
void Board::insertFeCbcEnabling(xgi::Output *out, ParameterSetUhal& pSetConfig, ParameterSetUhal& pSetFeCbc){
	int iFE, nbFE=beBoard->getNFe();//nbFrontEnd(pSetConfig);
	int iCBC, nbCBC=beBoard->fModuleVector[0]->getNCbc();//pSetConfig.getValue(CONFIG_HYBRID_TYPE);
	string strBoardImg= toolbox::toString("/img/%sConfig%d.jpg",beBoard->getBoardType()=="GLIB" ? "glib":"cta", nbCBC);
	*out<<script()<<"function checkAllBoxes(iFE, bCheck){"<<endl;//javascript function to check all boxes on a FE line
	*out<<"for (var i=0;i<"<<nbCBC<<";i++){"<<endl;
    *out<<"x=document.getElementsByName('FE'+iFE+'_CBC'+i);x[0].checked=bCheck;}"<<endl;
    *out<<"x=document.getElementsByName('FE'+iFE+'_all');x[0].checked=bCheck; }"<<endl;
	*out<<script();
	*out<<form().set("method","POST").set("action","saveFeCbcEnabling")<<"<table border='1'>"<<endl;
	*out<<tr()<<th("FE").set("rowspan","2")<<th("FE<br/>Enabled").set("rowspan","2")<<th("CBC Enabled")
		.set("colspan",toolbox::toString("%d",nbCBC==1 ? 1 : nbCBC+1));
	*out<<td(a(img().set("height","200").set("src", strBoardImg)).set("href", strBoardImg))
		.set("rowspan",toolbox::toString("%d", nbFE>1 ? nbFE+3 : 3));
	*out<<tr()<<endl;
	*out<<tr();
	for (iCBC=0; iCBC<nbCBC; iCBC++)//CBC numbers as column headers
		*out<<th(toolbox::toString("%C%d",(char)('A'+iCBC%2), iCBC/2));
	
	if (nbCBC>1)	
		*out<<td("All");
	
	*out<<tr()<<endl;
	for (iFE=0; iFE<nbFE; iFE++){
//		if (nbFE==1) iFE=1;
			
		*out<<tr();
		*out<<th(toolbox::toString("%d",iFE));
		*out<<td().set("align","center")<<pSetFeCbc.htmlDescription(toolbox::toString(CONFIG_FE_ENABLED, iFE),CHECKBOX,
				toolbox::toString("onclick='if (this.checked==false) checkAllBoxes(%d,false)'", iFE));
		*out<<td()<<endl;
		for (iCBC=0; iCBC<nbCBC; iCBC++){
			*out<<td().set("align","center")<<pSetFeCbc.htmlDescription(toolbox::toString(CONFIG_FE_CBC_ENABLED,iFE, iCBC),CHECKBOX,"")<<td()<<endl;
		}
		if (nbCBC>1){
			*out<<td()<<input().set("type","checkbox").set("name",toolbox::toString("FE%d_all",iFE)).set("checked")
				.set("onClick",toolbox::toString("checkAllBoxes(%d, this.checked);",iFE));
			*out<<td()<<endl;					
		}
		*out<<tr()<<endl;
	}//for FE
	if (nbFE>1){
		*out<<tr()<<td("All")<<td().set("align","center")<<input().set("type","checkbox").set("name","FE_all").set("checked")
			.set("onClick",toolbox::toString(//javascript code to check all FE enable boxes
			"for (var i=0;i<%d;i++){elm=document.getElementsByName('FE'+i+'_enabled');elm[0].checked=FE_all.checked;}", 
			nbFE));
		*out<<td()<<tr();
	}
	*out<<"</table>"<<br()<<endl;
	*out<<input().set("value","OK").set("type","submit")<<form()<<endl;
}
///Insertion of HTML links for CBC configuration of I2C values
void Board::insertFeCbcLinks(xgi::Output *out, ParameterSetUhal& pSetConfig, ParameterSetUhal& pSetFeCbc){
	//int  nbFE=nbFrontEnd(pSetConfig);
	//int iCBC, nbCBC=pSetConfig.getValue(CONFIG_HYBRID_TYPE);
	//*out<<h4(toolbox::toString("nb FE=%d", beBoard->getNFe()));
	*out<<"<table border='1'>"<<endl;

	for ( std::vector<Module*>::iterator cModuleIt = beBoard->fModuleVector.begin(); cModuleIt != beBoard->fModuleVector.end(); cModuleIt++ ){
		*out<<tr()<<td()<<toolbox::toString("Front End %d", (*cModuleIt)->getModuleId())<<br();
		*out<<"(check all "<<input().set("type", "checkbox").set("onClick", toolbox::toString(//javascript code to check all CBC of one FE
			"lst=document.getElementsByClassName('FE%d');for (var i=0; i<lst.length; i++){lst[i].checked=this.checked;}", (*cModuleIt)->getModuleId()));
		*out<<")"<<td()<<endl;
		*out<<td().set("align", "right");
		for ( std::vector<Cbc*>::iterator cCbcIt = (*cModuleIt)->fCbcVector.begin(); cCbcIt != (*cModuleIt)->fCbcVector.end(); cCbcIt++ ){
			uint8_t uCBC = (*cCbcIt)->getCbcId();
			if (pSetFeCbc.getValue(toolbox::toString(CONFIG_FE_CBC_ENABLED,(*cModuleIt)->getModuleId(), uCBC))==1){
                                string strCbcFile=toolbox::toString("FE%dCBC%d", (*cModuleIt)->getModuleId(), uCBC);
				*out<<a(toolbox::toString("CBC %C%d", (char)('A'+uCBC%2),uCBC/2)).set("href", toolbox::toString("i2cRead?cbc=%s", strCbcFile.c_str()))
                                                .set("title",string("Display values from memory and file ").append(strCbcFile));
				*out<<input().set("type", "checkbox").set("name", string("chk").append(strCbcFile))
						.set("class", toolbox::toString("FE%d", (*cModuleIt)->getModuleId()));
				*out<<a("(read)").set("href", toolbox::toString("i2cRead?read=on&cbc=%s", strCbcFile.c_str())).set("title", "Read from board");
				*out<<br()<<endl;
			} else
				*out<<toolbox::toString("CBC %C%d", (char)('A'+uCBC%2), uCBC/2)<<br()<<endl;

		}
		*out<<td()<<tr()<<endl;
	}

	/*for (iFE=0;iFE<nbFE;iFE++){
//		if (nbFE==1) iFE=1;
			
		if (pSetFeCbc.getValue(toolbox::toString(CONFIG_FE_ENABLED, iFE))==1){
			*out<<tr()<<td()<<toolbox::toString("Front End %d", iFE)<<br();
			*out<<"(check all "<<input().set("type", "checkbox").set("onClick", toolbox::toString(//javascript code to check all CBC of one FE
				"lst=document.getElementsByClassName('FE%d');for (var i=0; i<lst.length; i++){lst[i].checked=this.checked;}", iFE));
			*out<<")"<<td()<<endl;
			*out<<td().set("align", "right");
			for (iCBC=0; iCBC<nbCBC; iCBC++){
				if (pSetFeCbc.getValue(toolbox::toString(CONFIG_FE_CBC_ENABLED,iFE, iCBC))==1){
					*out<<a(toolbox::toString("CBC %C%d", (char)('A'+iCBC%2),iCBC/2)).set("href", toolbox::toString("i2cRead?cbc=FE%dCBC%d.txt", iFE, iCBC));
					*out<<input().set("type", "checkbox").set("name", toolbox::toString("chkFE%dCBC%d", iFE, iCBC))
						.set("class", toolbox::toString("FE%d", iFE));
					*out<<br()<<endl;
				} else
					*out<<toolbox::toString("CBC %C%d", (char)('A'+iCBC%2), iCBC/2)<<br()<<endl;
			}
			*out<<td()<<tr()<<endl;
		}
	}//for FE*/
	*out<<tr()<<td("")<<td()<<"All CBC"<<input().set("type", "checkbox").set("onClick", //javascript code to check all CBC of all FE
				"lst=document.getElementsByTagName('input');for (var i=0; i<lst.length; i++){if (lst[i].name.slice(0,5)=='chkFE') lst[i].checked=this.checked;}");
	*out<<input().set("type","submit").set("value","Write")<<endl;
	*out<<td()<<tr()<<endl;	
	*out<<"</table>"<<endl;
}
void Board::insertFpgaImages(xgi::Output *out){
	if (beBoard->getBoardType()=="GLIB")
		*out<<option("Golden Image")<<option("User Image");
	else
	    for (auto &strImg : vecImages)
		    *out<<option(strImg); 
}

void Board::loadFpgaImages(){
	vecImages = beBoardInterface->getFpgaConfigList(beBoard);
}
/**Switch to FPGA configuration
 * @param numImage Image number (0: golden, 1: user) */
void Board::jumpToImage(const std::string& strImage){
	if (beBoard && beBoardInterface){
		//beBoardInterface->WriteBoardReg( beBoard, "ctrl_2.icap_page",numImage);
		//beBoardInterface->WriteBoardReg( beBoard, "ctrl_2.icap_trigg",1);
		if (beBoard->getBoardType()=="GLIB")
			beBoardInterface->JumpToFpgaConfig(beBoard, strImage=="Golden Image" ? "1" : "2");
		else 
			beBoardInterface->JumpToFpgaConfig(beBoard, strImage);
	}
}
/**Reset FLASH and trigger FPGA configuration (asserting the program_b pin). Works only on GLIB boards. */
void Board::reboot(){
	if (beBoard && beBoardInterface)
		beBoardInterface->RebootBoard( beBoard);
}
/** Upload an MCS file into the FPGA
 * @param bGolden True to upload into the golden configuration (the one used after a reboot). False for the user configuration
 * @param strFile MCS file path */
void Board::uploadFpgaConfig(const std::string& strImage, const std::string& strFile) throw (std::string){
	if (beBoard && beBoardInterface){
	    if (beBoard->getBoardType()=="GLIB")
		beBoardInterface->FlashProm( beBoard, strImage=="Golden Image" ? "1" : "2", strFile.c_str());
	    else
		beBoardInterface->FlashProm( beBoard, strImage, strFile.c_str());
	}
}

uint32_t Board::isUploadingFpga() const { 
	if (beBoard && beBoardInterface){
		const Ph2_HwInterface::FpgaConfig* pConfig=beBoardInterface->getConfiguringFpga(beBoard);
		if (pConfig) 
			return pConfig->getUploadingFpga();
	}
	return 0;
}

std::string Board::htmlFpgaProgressBar()const { 
	ostringstream ostr;
	if (beBoard && beBoardInterface){
		const Ph2_HwInterface::FpgaConfig* pConfig = beBoardInterface->getConfiguringFpga(beBoard);
		if (pConfig){
			ostr<<"<div align='center'><progress max='100' style='width:100%' value='"<<pConfig->getProgressValue()<<"'></progress></div>";
			ostr<<"<div align='center'>"<<pConfig->getProgressString()<<"</div>";
		}
	}
	return ostr.str();
}
	
std::string Board::expandEnvironmentVariables( std::string s ) {
    if( s.find( "${" ) == std::string::npos ) return s;

    std::string pre  = s.substr( 0, s.find( "${" ) );
    std::string post = s.substr( s.find( "${" ) + 2 );

    if( post.find( '}' ) == std::string::npos ) return s;

    std::string variable = post.substr( 0, post.find( '}' ) );
    std::string value    = "";

    post = post.substr( post.find( '}' ) + 1 );

    if( getenv( variable.c_str() ) != NULL ) value = std::string( getenv( variable.c_str() ) );

    return expandEnvironmentVariables( pre + value + post );
}
