/* Copyright 2012 Institut Pluridisciplinaire Hubert Curien
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

   Programmer : 	Christian Bonnin
   Version : 		1.0
   Date of creation 01/12/2012
   Support : 		mail to : christian.bonnin@iphc.cnrs.fr
*/
#include <fstream>
#include "interface/shared/frl_header.h"
#include "FedEvent.h"

//#define DEBUG_SIZE
//DAQ header and trailer
#define BOE_1			0x5
#define EVENT_TYPE		0x01 //Physics trigger
#define SOURCE_FED_ID	0x33
#define FOV				0x00

#define EOE_1			0xA0
#define TTS_VALUE		0x70

#define IDX_DAQ_HEADER_FOV			0
#define IDX_DAQ_HEADER_SOURCE_LSB	1
#define IDX_DAQ_HEADER_SOURCE_MSB	2
#define IDX_DAQ_HEADER_BX			3
#define IDX_DAQ_HEADER_LV1_LSB		4
#define IDX_DAQ_HEADER_LV1_1		5
#define IDX_DAQ_HEADER_LV1_MSB		6
#define IDX_DAQ_HEADER_TYPE			7

#define IDX_DAQ_TRAILER_EOE		7
#define IDX_DAQ_TRAILER_LEN_MSB	6
#define IDX_DAQ_TRAILER_LEN_1	5
#define IDX_DAQ_TRAILER_LEN_LSB	4
#define IDX_DAQ_TRAILER_CRC_MSB	3
#define IDX_DAQ_TRAILER_CRC_LSB	2
#define IDX_DAQ_TRAILER_STAT	1
#define IDX_DAQ_TRAILER_TTS		0
//Tracker header (FED)
#define GLOBAL_FED_HEADER_SIZE_IN_BYTES	592 //576 padded + 2 mots de 64 bits
#define NB_FIBRE				12
#define	NB_FE					8
#define FIBRE_DATA_MIN_SIZE		7	//Fibre length + packet code + median information
#define TRACKER_HEADER_SIZE		136 //NB_FE * 16 + 8 bytes

#define APVE_ADDRESS		0x00
#define APV_ADDRESS_ERROR	0xFF
#define FE_ENABLE			0x80
#define FRONT_END_OVERFLOW	0x00
#define FED_STATUS_REGISTER	0x00

#define TK_EVT_TYPE_SCOPE_MODE_REAL					0x1
#define TK_EVT_TYPE_VIRGIN_RAW_MODE_REAL			0x2
#define TK_EVT_TYPE_VIRGIN_RAW_MODE_FAKE			0x3
#define TK_EVT_TYPE_PROCESSED_RAW_MODE_REAL			0x6
#define TK_EVT_TYPE_PROCESSED_RAW_MODE_FAKE			0x7
#define TK_EVT_TYPE_ZERO_SUPPRESSED_MODE_REAL		0xA
#define TK_EVT_TYPE_ZERO_SUPPRESSED_MODE_FAKE		0xB
#define TK_EVT_TYPE_ZERO_SUPPRESSED_LITE_MODE_REAL	0xC
#define TK_EVT_TYPE_ZERO_SUPPRESSED_LITE_MODE_FAKE	0xD

#define RESERVED_BYTE_DEFAULT_VALUE	0xED

#define HDR_FULL_DEBUG_MODE			0x10
#define HDR_APV_ERROR_MODE			0x20

#define IDX_RESERVED				7
#define IDX_TRK_EVENT_TYPE			6
#define IDX_APVE_ADDR				5
#define IDX_APV_ADDR_ERROR			4
#define IDX_FE_ENABLE				3
#define IDX_FE_OVERFLOW				2
#define IDX_FED_STATUS_REGISTER_MSB	1
#define IDX_FED_STATUS_REGISTER_LSB	0
#define IDX_FED_FE1_LENGTH_MSB		(128+7)
#define IDX_FED_FE1_LENGTH_LSB		(128+6)

#define FIBER_STRIP_IS_HIT			243
#define FIBER_STRIP_IS_NOT_HIT		0
#define FIBER_PACKET_CODE			0xEA	//0b11101010
//indexes in data payload. Those are in big endian order (left to right) because little endian index computing is performed after
#define IDX_FIBER_LEN_LSB			0
#define IDX_FIBER_LEN_MSB			1
#define IDX_FIBER_PACKET_CODE		2
#define IDX_FIBER_MEDIAN1_LSB		3
#define IDX_FIBER_MEDIAN2_LSB		5

using namespace std;

FedEvent::FedEvent(){
	data_=NULL;
	size_=0;
}

FedEvent::~FedEvent(){
	delete data_;
}

uint32_t FedEvent::littleEndian8(uint32_t n){
	return n^7; //(n/8+1)*8 - n%8 -1;
}

void FedEvent::reverseByte(char & b) {
   b = (b & 0xF0) >> 4 | (b & 0x0F) << 4;
   b = (b & 0xCC) >> 2 | (b & 0x33) << 2;
   b = (b & 0xAA) >> 1 | (b & 0x55) << 1;
}

/****************************************************************************/
/*                               SimulatedEvent                             */
/****************************************************************************/
SimulatedEvent::SimulatedEvent(const char *buffer, uint32_t size){
        size_=size;
        data_=new char[size_];
        memcpy(data_, buffer, size);
}
/****************************************************************************/
/*                               OneCbcEvent                                */
/****************************************************************************/
OneCbcEvent::OneCbcEvent(const char *buffer, uint32_t index):FedEvent(){
	uint32_t lenFE = calcBytesForFiber(buffer, index, NULL, 0) + (NB_FIBRE-1)*FIBRE_DATA_MIN_SIZE;//one FE enabled with one fibre
	lenFE+= (8 - lenFE%8)%8;//padding to 64bit words
	
	size_=TRACKER_HEADER_SIZE + lenFE;
	data_=new char[size_];
	memset (data_, 0, size_);	
//Tracker Header Full Debug Mode
//reservedByte_ | hdrFormat_ | trkEventType_ | apveAddress_ | apvAddressError_ | FEenable_ | FEoverflow_ | FedStatusRegister_;	
	data_[IDX_FED_STATUS_REGISTER_MSB]	=FED_STATUS_REGISTER>>8;
	data_[IDX_FED_STATUS_REGISTER_LSB]	=FED_STATUS_REGISTER&255;
	data_[IDX_FE_OVERFLOW]				=FRONT_END_OVERFLOW;
	data_[IDX_FE_ENABLE]				=FE_ENABLE;
	data_[IDX_APV_ADDR_ERROR]			=APV_ADDRESS_ERROR;
	data_[IDX_APVE_ADDR]				=APVE_ADDRESS;
	data_[IDX_TRK_EVENT_TYPE]			=TK_EVT_TYPE_ZERO_SUPPRESSED_MODE_REAL | HDR_FULL_DEBUG_MODE;
	data_[IDX_RESERVED]					=RESERVED_BYTE_DEFAULT_VALUE;
 
 	data_[IDX_FED_FE1_LENGTH_MSB]		=lenFE>>8 & 255;
 	data_[IDX_FED_FE1_LENGTH_LSB]		=lenFE & 255;
 	fillDataForFibers(buffer, index);
}

uint32_t OneCbcEvent::calcBytesForFiber(const char* buffer, uint32_t index, char* dest, uint32_t idxDest){
	uint32_t nb=FIBRE_DATA_MIN_SIZE;//Fibre length + packet code + median information
	unsigned char iOct, iBit, iShift, iLen=0;
	bool bCluster = false, bCurrent, bPrev=false, bBeforePrev=false;
	
	for (iBit=0; iBit<NB_STRIPS; iBit++){
		iOct=iBit<10 ? 39-iBit/8 : 35-(iBit-10)/8;
		iShift=iBit<10 ? iBit%8 : (iBit-10)%8; 
		bCurrent = (buffer[index + iOct]>>iShift)%2!=0;
		if(bCurrent){
			if (bCluster){//cluster continuing
				if (dest){	
					dest[littleEndian8(idxDest+nb)]=FIBER_STRIP_IS_HIT;
					iLen++;
				}
				nb++;//data
			} else {//cluster beginning
				bCluster=true;
				if (bBeforePrev){//Cluster joining
					if (dest){	
						dest[littleEndian8(idxDest+nb)]=FIBER_STRIP_IS_NOT_HIT;
						dest[littleEndian8(idxDest+nb+1)]=FIBER_STRIP_IS_HIT;
						iLen+=2;
					}
					nb+=2;//data
				} else {//cluster true beginning
					if (dest){
						dest[littleEndian8(idxDest+nb)]=iBit;
						dest[littleEndian8(idxDest+nb+2)]=FIBER_STRIP_IS_HIT;
						iLen=1;
					}
					nb+=3;//cluster address and length + data
				}
			}
		} 
		if (!bCurrent || iBit==NB_STRIPS-1) {//No hit or last strip 
			if (bCluster && dest){//cluster ending
				dest[littleEndian8(idxDest+nb-iLen-1)]=iLen;
			}
			bCluster=false;
		}
		bBeforePrev=bPrev;
		bPrev=bCurrent;
	}//for NB_STRIPS bits
	if (dest){
		dest[littleEndian8(idxDest+IDX_FIBER_LEN_LSB)]		=nb&255;
		dest[littleEndian8(idxDest+IDX_FIBER_LEN_MSB)]		=nb>>8 & 255;
		dest[littleEndian8(idxDest+IDX_FIBER_PACKET_CODE)]	=FIBER_PACKET_CODE;
		dest[littleEndian8(idxDest+IDX_FIBER_MEDIAN1_LSB)]	=FIBER_STRIP_IS_HIT;//Valeurs medianes = ??
		dest[littleEndian8(idxDest+IDX_FIBER_MEDIAN2_LSB)]	=FIBER_STRIP_IS_HIT;
	}
	return nb;
}	

void OneCbcEvent::fillDataForFibers(const char *buffer, uint32_t index){
	for (short sFiber=0; sFiber<NB_FIBRE-1; sFiber++){//Length of empty fibres
		data_[littleEndian8(TRACKER_HEADER_SIZE+sFiber*FIBRE_DATA_MIN_SIZE)]=FIBRE_DATA_MIN_SIZE;
	}
	calcBytesForFiber(buffer, index, data_, TRACKER_HEADER_SIZE+(NB_FIBRE-1)*FIBRE_DATA_MIN_SIZE);
}	

/****************************************************************************/
/*                               DAQData                                    */
/****************************************************************************/
int DAQData::crc_tab16_init          = false;
unsigned short  DAQData::crc_tab16[256];


DAQData::DAQData(FedEvent *pEvent, uint32_t nbL1A, uint32_t nbBunchCrossing){
	event_ = pEvent;
	header_ = new char[DAQ_HEADER_SIZE];
	trailer_= new char[DAQ_TRAILER_SIZE];
//DAQ header	
//	globalDaqHeader_ = boe_ | eventType_ | l1aCounter_ | bxCounter_ | sourceFedID_ | fov_;
	header_[IDX_DAQ_HEADER_TYPE]		= (BOE_1<<4) | EVENT_TYPE;
	header_[IDX_DAQ_HEADER_LV1_MSB]		= (nbL1A>>16)&255;
	header_[IDX_DAQ_HEADER_LV1_1]		= (nbL1A>>8)&255;
	header_[IDX_DAQ_HEADER_LV1_LSB]		= nbL1A&255;
	header_[IDX_DAQ_HEADER_BX]			= (nbBunchCrossing>>4)&255;
	header_[IDX_DAQ_HEADER_SOURCE_MSB]	= ((nbBunchCrossing&15)<<4) | ((SOURCE_FED_ID>>8)&255);
	header_[IDX_DAQ_HEADER_SOURCE_LSB]	= (SOURCE_FED_ID)&255;
	header_[IDX_DAQ_HEADER_FOV]			= (FOV<<4) | (0<<3) /*one word header*/ | 0; /*reserved*/
//DAQ trailer
	uint32_t len = event_->getSize()/8 + (event_->getSize()%8 ? 1 : 0) + 2;
	uint16_t crc = Crc16(header_, DAQ_HEADER_SIZE, 0xFFFF);
	crc = Crc16(event_->getDataAddress(), event_->getSize(), crc);
	trailer_[IDX_DAQ_TRAILER_EOE]		= EOE_1;
	trailer_[IDX_DAQ_TRAILER_LEN_MSB]	= (len>>16)&255;
	trailer_[IDX_DAQ_TRAILER_LEN_1]		= (len>>8)&255; 
	trailer_[IDX_DAQ_TRAILER_LEN_LSB]	= (len)&255;
	trailer_[IDX_DAQ_TRAILER_CRC_MSB]	= (crc>>8)&255; 
	trailer_[IDX_DAQ_TRAILER_CRC_LSB]	= (crc)&255;
	trailer_[IDX_DAQ_TRAILER_STAT]		= 0;
	trailer_[IDX_DAQ_TRAILER_TTS]		= TTS_VALUE;
}

///Constructor from simulated event
DAQData::DAQData(SimulatedEvent *pEvent, const char * buffer){
        event_=pEvent;
	header_ = new char[DAQ_HEADER_SIZE];
	trailer_= new char[DAQ_TRAILER_SIZE];
        memcpy(header_, buffer, DAQ_HEADER_SIZE);
        memcpy(trailer_, buffer+DAQ_HEADER_SIZE+event_->getSize(), DAQ_TRAILER_SIZE);
}

DAQData::~DAQData(){
	delete header_;
	delete trailer_;
}

uint16_t DAQData::Crc16(const char *Adresse_tab , uint32_t Taille_max, uint16_t Crc=0xFFFF) 
{
	uint16_t Polynome = 0xA001;// Polynome 2^16 + 2^15 + 2^2 + 2^0 = 0x8005.
	uint32_t CptOctet;
	unsigned char CptBit, Parity;

	for ( CptOctet= 0 ; CptOctet < Taille_max ; CptOctet++)
	{
		Crc ^= *( Adresse_tab + CptOctet); //Ou exculsif entre octet message et CRC
		for ( CptBit = 0; CptBit <= 7 ; CptBit++) /* Mise a 0 du compteur nombre de bits */
		{
			Parity= Crc%2;
			Crc >>= 1; // Decalage a droite du crc
			if (Parity) 
				Crc ^= Polynome; // Test si nombre impair -> Apres decalage a droite il y aura une retenue
		} // "ou exclusif" entre le CRC et le polynome generateur.
	}
	return(Crc);
}

std::string DAQData::calcCrc16FromFile(const std::string& strFile){
	ifstream ifFile(strFile, ifstream::in | ifstream::binary);
	char * buffer;
	int length;
	if (ifFile.good()){ // get length of file:
		ifFile.seekg (0, ifFile.end);
		length = ifFile.tellg();
		ifFile.seekg (0, ifFile.beg);

		buffer = new char [length];
	}
	uint32_t uWord=0;
	while (ifFile.good()){
		ifFile.read(&buffer[uWord*8], 8);
		uWord++;
	}
	uWord--;
	ifFile.close();
	uint16_t uCrc=(buffer[(uWord-1)*8+2]&255) + ((buffer[(uWord-1)*8+3]&255)<<8) ;
	buffer[(uWord-1)*8+2]=0;//reset CRC in buffer before computing it
	buffer[(uWord-1)*8+3]=0;
	uint16_t crc16=0xFFFF;
	for (int iBuf=0; iBuf<length; iBuf+=8)
		crc16=crcVhdl(&buffer[iBuf], crc16);
	//for (int iBuf=0; iBuf<length; iBuf++)
		//crc16=update_crc_16(crc16, buffer[iBuf]);
		
	string strRet=toolbox::toString("Size of file: %d Bytes (%d in DAQ trailer)\nComputed CRC16: %04X (%04X in DAQ trailer)", length, 
				((buffer[(uWord-1)*8+4]&255) + ((buffer[(uWord-1)*8+5]&255)<<8) + ((buffer[(uWord-1)*8+6]&255)<<16))*8,
				crc16,
				//Crc16(buffer, length),
				uCrc);
				
				
	delete buffer;
	return strRet;
}

uint16_t DAQData::crcVhdl(char *pBuf, uint16_t uCrc){
	bitset<16> NewCRC(0);
	bitset<64> D(0);
	bitset<16> C(uCrc);
	for (int iOct=7; iOct>=0; iOct--){
		D |= pBuf[iOct]&255U;
		if (iOct>0)
			D<<=8;
	}

	NewCRC[0] = D[63] ^ D[62] ^ D[61] ^ D[60] ^ D[55] ^ D[54] ^ 
                 D[53] ^ D[52] ^ D[51] ^ D[50] ^ D[49] ^ D[48] ^ 
                 D[47] ^ D[46] ^ D[45] ^ D[43] ^ D[41] ^ D[40] ^ 
                 D[39] ^ D[38] ^ D[37] ^ D[36] ^ D[35] ^ D[34] ^ 
                 D[33] ^ D[32] ^ D[31] ^ D[30] ^ D[27] ^ D[26] ^ 
                 D[25] ^ D[24] ^ D[23] ^ D[22] ^ D[21] ^ D[20] ^ 
                 D[19] ^ D[18] ^ D[17] ^ D[16] ^ D[15] ^ D[13] ^ 
                 D[12] ^ D[11] ^ D[10] ^ D[9] ^ D[8] ^ D[7] ^ 
                 D[6] ^ D[5] ^ D[4] ^ D[3] ^ D[2] ^ D[1] ^ 
                 D[0] ^ C[0] ^ C[1] ^ C[2] ^ C[3] ^ C[4] ^ 
                 C[5] ^ C[6] ^ C[7] ^ C[12] ^ C[13] ^ C[14] ^ 
                 C[15];
	NewCRC[1] = D[63] ^ D[62] ^ D[61] ^ D[56] ^ D[55] ^ D[54] ^ 
                 D[53] ^ D[52] ^ D[51] ^ D[50] ^ D[49] ^ D[48] ^ 
                 D[47] ^ D[46] ^ D[44] ^ D[42] ^ D[41] ^ D[40] ^ 
                 D[39] ^ D[38] ^ D[37] ^ D[36] ^ D[35] ^ D[34] ^ 
                 D[33] ^ D[32] ^ D[31] ^ D[28] ^ D[27] ^ D[26] ^ 
                 D[25] ^ D[24] ^ D[23] ^ D[22] ^ D[21] ^ D[20] ^ 
                 D[19] ^ D[18] ^ D[17] ^ D[16] ^ D[14] ^ D[13] ^ 
                 D[12] ^ D[11] ^ D[10] ^ D[9] ^ D[8] ^ D[7] ^ 
                 D[6] ^ D[5] ^ D[4] ^ D[3] ^ D[2] ^ D[1] ^ 
                 C[0] ^ C[1] ^ C[2] ^ C[3] ^ C[4] ^ C[5] ^ 
                 C[6] ^ C[7] ^ C[8] ^ C[13] ^ C[14] ^ C[15];
	NewCRC[2] = D[61] ^ D[60] ^ D[57] ^ D[56] ^ D[46] ^ D[42] ^ 
                 D[31] ^ D[30] ^ D[29] ^ D[28] ^ D[16] ^ D[14] ^ 
                 D[1] ^ D[0] ^ C[8] ^ C[9] ^ C[12] ^ C[13];
	NewCRC[3] = D[62] ^ D[61] ^ D[58] ^ D[57] ^ D[47] ^ D[43] ^ 
                 D[32] ^ D[31] ^ D[30] ^ D[29] ^ D[17] ^ D[15] ^ 
                 D[2] ^ D[1] ^ C[9] ^ C[10] ^ C[13] ^ C[14];
	NewCRC[4] = D[63] ^ D[62] ^ D[59] ^ D[58] ^ D[48] ^ D[44] ^ 
                 D[33] ^ D[32] ^ D[31] ^ D[30] ^ D[18] ^ D[16] ^ 
                 D[3] ^ D[2] ^ C[0] ^ C[10] ^ C[11] ^ C[14] ^ 
                 C[15];
	NewCRC[5] = D[63] ^ D[60] ^ D[59] ^ D[49] ^ D[45] ^ D[34] ^ 
                 D[33] ^ D[32] ^ D[31] ^ D[19] ^ D[17] ^ D[4] ^ 
                 D[3] ^ C[1] ^ C[11] ^ C[12] ^ C[15];
	NewCRC[6] = D[61] ^ D[60] ^ D[50] ^ D[46] ^ D[35] ^ D[34] ^ 
                 D[33] ^ D[32] ^ D[20] ^ D[18] ^ D[5] ^ D[4] ^ 
                 C[2] ^ C[12] ^ C[13];
	NewCRC[7] = D[62] ^ D[61] ^ D[51] ^ D[47] ^ D[36] ^ D[35] ^ 
                 D[34] ^ D[33] ^ D[21] ^ D[19] ^ D[6] ^ D[5] ^ 
                 C[3] ^ C[13] ^ C[14];
	NewCRC[8] = D[63] ^ D[62] ^ D[52] ^ D[48] ^ D[37] ^ D[36] ^ 
                 D[35] ^ D[34] ^ D[22] ^ D[20] ^ D[7] ^ D[6] ^ 
                 C[0] ^ C[4] ^ C[14] ^ C[15];
	NewCRC[9] = D[63] ^ D[53] ^ D[49] ^ D[38] ^ D[37] ^ D[36] ^ 
                 D[35] ^ D[23] ^ D[21] ^ D[8] ^ D[7] ^ C[1] ^ 
                 C[5] ^ C[15];
	NewCRC[10] = D[54] ^ D[50] ^ D[39] ^ D[38] ^ D[37] ^ D[36] ^ 
                  D[24] ^ D[22] ^ D[9] ^ D[8] ^ C[2] ^ C[6];
	NewCRC[11] = D[55] ^ D[51] ^ D[40] ^ D[39] ^ D[38] ^ D[37] ^ 
                  D[25] ^ D[23] ^ D[10] ^ D[9] ^ C[3] ^ C[7];
	NewCRC[12] = D[56] ^ D[52] ^ D[41] ^ D[40] ^ D[39] ^ D[38] ^ 
                  D[26] ^ D[24] ^ D[11] ^ D[10] ^ C[4] ^ C[8];
	NewCRC[13] = D[57] ^ D[53] ^ D[42] ^ D[41] ^ D[40] ^ D[39] ^ 
                  D[27] ^ D[25] ^ D[12] ^ D[11] ^ C[5] ^ C[9];
	NewCRC[14] = D[58] ^ D[54] ^ D[43] ^ D[42] ^ D[41] ^ D[40] ^ 
                  D[28] ^ D[26] ^ D[13] ^ D[12] ^ C[6] ^ C[10];
	NewCRC[15] = D[63] ^ D[62] ^ D[61] ^ D[60] ^ D[59] ^ D[54] ^ 
                  D[53] ^ D[52] ^ D[51] ^ D[50] ^ D[49] ^ D[48] ^ 
                  D[47] ^ D[46] ^ D[45] ^ D[44] ^ D[42] ^ D[40] ^ 
                  D[39] ^ D[38] ^ D[37] ^ D[36] ^ D[35] ^ D[34] ^ 
                  D[33] ^ D[32] ^ D[31] ^ D[30] ^ D[29] ^ D[26] ^ 
                  D[25] ^ D[24] ^ D[23] ^ D[22] ^ D[21] ^ D[20] ^ 
                  D[19] ^ D[18] ^ D[17] ^ D[16] ^ D[15] ^ D[14] ^ 
                  D[12] ^ D[11] ^ D[10] ^ D[9] ^ D[8] ^ D[7] ^ 
                  D[6] ^ D[5] ^ D[4] ^ D[3] ^ D[2] ^ D[1] ^ 
                  D[0] ^ C[0] ^ C[1] ^ C[2] ^ C[3] ^ C[4] ^ 
                  C[5] ^ C[6] ^ C[11] ^ C[12] ^ C[13] ^ C[14] ^ 
                  C[15];
		
	//cout<<hex<<"Data: "<<D.to_ulong()<<", CRC: "<<NewCRC.to_ulong()<<endl;
	return NewCRC.to_ulong();
}

    /*******************************************************************\
    *                                                                   *
    *   unsigned short update_crc_16( unsigned short crc, char c );     *
    *                                                                   *
    *   The function update_crc_16 calculates a  new  CRC-16  value     *
    *   based  on  the  previous value of the CRC and the next byte     *
    *   of the data to be checked.                                      *
    *                                                                   *
    \*******************************************************************/

unsigned short DAQData::update_crc_16( unsigned short crc, char c ) {
    unsigned short tmp, short_c;
    short_c = 0x00ff & (unsigned short) c;
    if ( ! crc_tab16_init ) init_crc16_tab();
    tmp =  crc       ^ short_c;
    crc = (crc >> 8) ^ DAQData::crc_tab16[ tmp & 0xff ];
    return crc;
}  /* update_crc_16 */

    /*******************************************************************\
    *                                                                   *
    *   static void init_crc16_tab( void );                             *
    *                                                                   *
    *   The function init_crc16_tab() is used  to  fill  the  array     *
    *   for calculation of the CRC-16 with values.                      *
    *                                                                   *
    \*******************************************************************/
#define                 P_16        0xA001

void DAQData::init_crc16_tab( void ) {

    int i, j;
    unsigned short crc, c;

    for (i=0; i<256; i++) {

        crc = 0;
        c   = (unsigned short) i;

        for (j=0; j<8; j++) {

            if ( (crc ^ c) & 0x0001 ) crc = ( crc >> 1 ) ^ P_16;
            else                      crc =   crc >> 1;

            c = c >> 1;
        }

        DAQData::crc_tab16[i] = crc;
    }

    crc_tab16_init = true;

}  /* init_crc16_tab */


const char* DAQData::getDataAddress() const{
	return event_->getDataAddress();
}

const char* DAQData::getHeaderAddress() const{
	return header_;
}

const char* DAQData::getTrailerAddress() const{
	return trailer_;
}

uint32_t	DAQData::getDaqSize() const{
	return event_->getSize()+DAQ_HEADER_SIZE+DAQ_TRAILER_SIZE;
}
/***************************************************************************/
/*                        FRLData                                          */
/***************************************************************************/
FRLData::FRLData(DAQData *pDaq){
	daqData_ = pDaq;
	uDataSize= daqData_->getDaqSize();
}

FRLData::FRLData(const char *pChar, uint32_t uSize){
	daqData_ = nullptr;
	ptrData  = pChar;
	uDataSize= uSize;	
}

uint32_t FRLData::getFrlSize() const{
	return sizeof(frlh_t) + uDataSize + sizeof(I2O_EVENT_DATA_BLOCK_MESSAGE_FRAME);
}

toolbox::mem::Reference* FRLData::generateReference(FRLA* sharedMem, uint32_t uEvt){
	int returned_buffer_size = uDataSize;
	
	//DEBUG - dump FED buffer & check headers + trailers
	//binBufferToText((char *)&globalFedHeader[0],GLOBAL_FED_HEADER_SIZE_IN_BYTES,64,1);
	
		
    int dataShift = /*sizeof(I2O_EVENT_DATA_BLOCK_MESSAGE_FRAME) +*/ sizeof(frlh_t);
    //int dataShift = sizeof(frlh_t)/sizeof(int);
    //uint32_t allo_size= (dataShift+32)*sizeof(int)+returned_buffer_size;
//    uint32_t allo_size= (dataShift+1)*sizeof(uint32_t)+returned_buffer_size;
    uint32_t allo_size = dataShift + returned_buffer_size + sizeof(I2O_EVENT_DATA_BLOCK_MESSAGE_FRAME);
    toolbox::mem::Reference* tail=NULL;
    while (tail==NULL) {
        try {
            tail=sharedMem->allocateDataBuffer(allo_size);
        } catch(xcept::Exception e) {
            std::cout<<"Cannot allocate buffer of size "<<allo_size<<std::endl;
            std::cout<<e.what()<<std::endl;
            tail=NULL;
            for (uint32_t jj=0;jj<200;jj++)
                usleep(100);
                
//            sharedMem->clearBuffers();
        }
    }
    char * data = (char *) sharedMem->getUserAddress(tail);


	//LGROSS -- FED BUFFER RELATED
    //memcpy(&data[dataShift],&buffer[index],returned_buffer_size);
    if (daqData_!= nullptr){
	memcpy(&data[dataShift],daqData_->getHeaderAddress(),DAQ_HEADER_SIZE);
	memcpy(&data[dataShift+DAQ_HEADER_SIZE],daqData_->getDataAddress(),returned_buffer_size-DAQ_HEADER_SIZE-DAQ_TRAILER_SIZE);
	memcpy(&data[dataShift+returned_buffer_size-DAQ_TRAILER_SIZE],daqData_->getTrailerAddress(),DAQ_TRAILER_SIZE);
    } else 
	memcpy(&data[dataShift], ptrData, uDataSize);

      // Fill the FRL header
	//DDEBUG
//      frlh_t* frlh = (frlh_t*)(&data[0] + sizeof(I2O_EVENT_DATA_BLOCK_MESSAGE_FRAME));
      frlh_t* frlh = (frlh_t*)(&data[0]);
      //frlh_t* frlh = (frlh_t*)(data + sizeof(I2O_EVENT_DATA_BLOCK_MESSAGE_FRAME));
//std::cout << std::hex << "frlh_t* frlh origine cree, pointe a 0x" << (long)frlh << std::dec << std::endl;


		//DDEBUG
//      frlh->source = 0xd3;
//DDEBUG

	//std::cout << "uEvt= " << uEvt << ", returned_buffer_size=" << returned_buffer_size << std::endl;
      frlh->trigno = uEvt; 
      frlh->segno = 0;
	  frlh->source = 0xd3;


//AAAAAAAAAAAAAAAAAAAaa

	//DDEBUG
      //frlh->segsize=returned_buffer_size | FRL_LAST_SEGM;
      //frlh->segsize=(returned_buffer_size | FRL_LAST_SEGM)+8 ;
//      frlh->segsize=(returned_buffer_size & FRL_SEGSIZE_MASK) | FRL_LAST_SEGM;
	  frlh->segsize=(returned_buffer_size & FRL_SEGSIZE_MASK) | FRL_LAST_SEGM;
	  
	  
//	  std::cout << "frlh->segsize as setted = " << (long)(frlh->segsize & FRL_SEGSIZE_MASK);
//	  std::cout << "(returned_buffer_size& FRL_SEGSIZE_MASK)=" << (returned_buffer_size & FRL_SEGSIZE_MASK) << std::endl;
//	  std::cout << "((returned_buffer_size& FRL_SEGSIZE_MASK) | FRL_LAST_SEGM )=" << ((returned_buffer_size & FRL_SEGSIZE_MASK) | FRL_LAST_SEGM) << std::endl;
	  
      //uint32_t newsiz= returned_buffer_size+dataShift*sizeof(uint32_t)+8;
      //sharedMem->updateSize(tail,newsiz);

	  I2O_EVENT_DATA_BLOCK_MESSAGE_FRAME* block  = (I2O_EVENT_DATA_BLOCK_MESSAGE_FRAME*) tail->getDataLocation();
	  block->nbBlocksInSuperFragment = 1;

	block->PvtMessageFrame.XFunctionCode   = I2O_RU_DATA_READY;
	block->eventNumber                     = uEvt;                      // trigger number (from SLINK header)
	block->blockNb		               		= 0;                       // Block nb
	block->nbBlocksInSuperFragment         = 1;                      // Nb of blocks in fragment

	block->PvtMessageFrame.StdMessageFrame.Function = I2O_PRIVATE_MESSAGE;
	block->PvtMessageFrame.OrganizationID = XDAQ_ORGANIZATION_ID; // function group offset
	block->PvtMessageFrame.StdMessageFrame.MsgFlags = 0;  // normal message
//	block->PvtMessageFrame.StdMessageFrame.TargetAddress = destinationTid_; 
//	block->PvtMessageFrame.StdMessageFrame.InitiatorAddress = initiatorTid_;
	block->PvtMessageFrame.StdMessageFrame.VersionOffset = 0;
//	block->PvtMessageFrame.StdMessageFrame.InitiatorContext = initiatorContext;

	block->PvtMessageFrame.StdMessageFrame.MessageSize = (sizeof (I2O_EVENT_DATA_BLOCK_MESSAGE_FRAME)+sizeof(frlh_t) + returned_buffer_size) >> 2;
#ifdef DEBUG_SIZE
	std::cout << "Buffer size="<<returned_buffer_size<<", Allocated size="<<allo_size<<", Message Size="<<block->PvtMessageFrame.StdMessageFrame.MessageSize*4<<std::endl;
#endif
	return tail;
}

void FRLData::dumpData(FRLA* sharedMem, toolbox::mem::Reference* tail) const{
	//Now check what we built up
	std::cout << "DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG " << std::endl;
	std::cout << std::hex << "tail->getDataLocation() gives " << tail->getDataLocation()  << std::endl;
	std::cout << "sharedMem->getUserAddress(tail) gives 0x"  << sharedMem->getUserAddress(tail)  << std::dec << std::endl;
	
	
	//On recupere dans blockAddr le pointeur de tout debut d I2O message qui pointe sur une structure 64 bytes : I2O_EVENT_DATA_BLOCK_MESSAGE_FRAME
	char * blocAdresse;
	blocAdresse = (char *)tail->getDataLocation();
	std::cout << "blocAdresse recupere , pointe a 0x" << std::hex << (long)blocAdresse << std::endl;
	
	//On chope l'adresse du header FRL qui est normalemet situe apres le bloc I2O_EVENT_DATA_BLOCK_MESSAGE_FRAME
	//donc on devrait pouvoir retrouver aussi cette adresse avec sharedMem->getUserAddress(tail) ?
	char * frlHeaderAddr;
	frlHeaderAddr = blocAdresse + sizeof(I2O_EVENT_DATA_BLOCK_MESSAGE_FRAME);
	std::cout << "frlHeaderAddr recupere , pointe a 0x" << (long)frlHeaderAddr << std::dec << std::endl;
	
    char * data = (char *) sharedMem->getUserAddress(tail);
	frlh_t* frlh = (frlh_t*)(&data[0]);	
	frlh_t* frlHeader = (frlh_t*)frlHeaderAddr;
	std::cout << "Pointeur de structure frlHeader cree, pointe sur les infos FRL du buffer I2O" << std::endl;
	std::cout << "frlHeader->trigno = " << frlHeader->trigno << std::endl;
	std::cout << "frlh->trigno = " << frlh->trigno << std::endl;
	std::cout << "frlHeader->segno = " << frlHeader->segno << std::endl;
	std::cout << "frlh->segno = " << frlh->segno << std::endl;
	std::cout << "frlHeader->source = " << frlHeader->source << std::endl;
	std::cout << "frlh->source = " << frlh->source << std::endl;
	
	std::cout << "frlHeader->segsize = " << (long)(frlHeader->segsize & FRL_SEGSIZE_MASK) << std::endl;
	std::cout << "frlh->segsize = " << (long)(frlh->segsize & FRL_SEGSIZE_MASK) << std::endl;
	
	
	//uint32_t len = frlHeader->segsize & FRL_SEGSIZE_MASK;
	//std::cout << "sizeof(fedt_t)=" << sizeof(fedt_t) << std::endl;
	
	
	
	/*
	char* fedAddr       = frlHeaderAddr + sizeof(frlh_t);
	uint32_t nbBytes       = frlHeader->segsize & FRL_SEGSIZE_MASK;
	*/
	
	std::cout << std::dec;
}
