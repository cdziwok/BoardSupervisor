/* Copyright 2012-2016 Institut Pluridisciplinaire Hubert Curien
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

   Programmer : 	Christian Bonnin
   Version : 		1.0
   Date of creation:01/08/2012
   Support : 		mail to : christian.bonnin@iphc.cnrs.fr
   FileName : 		BoardSupervisor.cc
   Content : 		BoardSupervisor module
   Support : 		mail to : christian.bonnin@iphc.cnrs.fr
*/

#include <exception>
#include <boost/algorithm/string.hpp>
#include <boost/format.hpp>
#include "toolbox/fsm/FailedEvent.h"
#include "HWDescription/BeBoard.h"
#include "HWDescription/Module.h"
#include "HWDescription/Cbc.h"
#include "HWDescription/CbcRegItem.h"

#include "Streamer.h"
#include "FedEvent.h"
#include "BoardSupervisor.h"

using namespace cgicc;
using namespace std;

BoardSupervisor::Streamer::Streamer(Supervisor *pApp) : frla_(8) {
    /* bind xgi and xoap commands specific to this application */
    xgi::bind(pApp,&Supervisor::StreamerAcq, "StreamerDefault");
    xgi::bind(pApp,&Supervisor::AutoRefresh,"autoRefresh");
    xgi::bind(pApp,&Supervisor::ValidParam,"validParam");
    xgi::bind(pApp,&Supervisor::SendSpuriousFrame,"sendSpuriousFrame");
    xgi::bind(pApp,&Supervisor::ReadFlags,"readFlags");
    xgi::bind(pApp,&Supervisor::EnterData,"enterData");
    xgi::bind(pApp,&Supervisor::ValidSimulationData,"validSimulationData");
    xgi::bind(pApp,&Supervisor::ForceStartBg0,"forceStartXgi");
    xgi::bind(pApp,&Supervisor::PauseAcquisition,"pauseAcquisition");
    xgi::bind(pApp,&Supervisor::LoadFromStreamerFileHtml, "loadStreamerHtmlValues");
    xgi::bind(pApp,&Supervisor::SaveToStreamerFileHtml, "saveStreamerHtmlValues");

    xgi::bind(pApp,&Supervisor::EnterConditionData,"enterConditionData");
    xgi::bind(pApp,&Supervisor::ValidConditionData,"validConditionData");
    xgi::bind(pApp,&Supervisor::LoadFromFileHtmlCondition, "loadConditionData");
    xgi::bind(pApp,&Supervisor::SaveToFileHtmlCondition, "saveConditionData");

    xgi::bind(pApp,&Supervisor::EnterCommissioningLoop,"enterCommissioningLoop");
    xgi::bind(pApp,&Supervisor::ValidCommissioningLoop,"validCommissioningLoop");
    xgi::bind(pApp,&Supervisor::LoadFromFileCommissioningLoop, "loadCommissioningLoop");
    xgi::bind(pApp,&Supervisor::SaveToFileCommissioningLoop, "saveCommissioningLoop");
    xgi::bind(pApp,&Supervisor::AddCommissioningParameter, "addCommissioningParameter");
    xgi::bind(pApp,&Supervisor::RemoveLastCommissioningParameter, "removeLastCommissioningParameter");
    xgi::bind(pApp,&Supervisor::ToggleDataVisuSize, "toggleDataVisuSize");
    xgi::bind(pApp,&Supervisor::ResultCommissioningLoop, "resultCommissioningLoop");
    
	// Define FSM
	/*fsm_.addState ('H', "Halted");
	fsm_.addState ('R', "Running");
	fsm_.addState ('C', "Configured");
	fsm_.addStateTransition ('C','R', "start", this, &Streamer::startAction);
	fsm_.addStateTransition ('R','C', "stop", this, &Streamer::stopAction);
	fsm_.addStateTransition ('R','H', "halt", this, &Streamer::haltAction);
	fsm_.addStateTransition ('C','H', "halt", this, &Streamer::haltAction);
	fsm_.addStateTransition ('H','C', "configure", this, &Streamer::configureAction);
	fsm_.setInitialState('H');
	// Failure state setting
	fsm_.setFailedStateTransitionAction( this, &Streamer::failedTransition );
	fsm_.reset();*/
	bRefresh=false;
	bBigSizeVisu=false;
	pWebApp = pApp;
	pMainBoard=pApp->getBoardPointer();
	pAcq=pApp->getAcquisitionPointer();
}


BoardSupervisor::Streamer::~Streamer() {
}

void BoardSupervisor::Streamer::actionPerformed (xdata::Event& e) {
    if (e.type() == "urn:xdaq-event:setDefaultValues") {
        try {
		fileParamHtml=Board::expandEnvironmentVariables(fileParamHtml.toString());
        	if ( ! boost::algorithm::to_lower_copy(fileParamHtml.toString().substr(0,7)).compare("file://"))
        		fileParamHtml=fileParamHtml.toString().substr(7);
            
		fileConditionHtml=Board::expandEnvironmentVariables(fileConditionHtml.toString());
        	if ( ! boost::algorithm::to_lower_copy(fileConditionHtml.toString().substr(0,7)).compare("file://"))
        		fileConditionHtml=fileConditionHtml.toString().substr(7);

//	Default values for streamer parameters from the XDAQ XML configuration file
		pAcq->getStreamerParameterSet().setValue(STREAMER_SHORT_PAUSE, intShortPause);
		pAcq->getStreamerParameterSet().setValue(STREAMER_LONG_PAUSE, intLongPause);
		pAcq->getStreamerParameterSet().setValue(STREAMER_USE_HARDWARE_COUNTER, bHardwareCounter);
		pAcq->getStreamerParameterSet().setValue(STREAMER_SHARED_MEMORY, bSharedMemory);
		pAcq->getStreamerParameterSet().setValue(STREAMER_MEMTOFILE,bMemToFile);
		pAcq->getStreamerParameterSet().setValue(STREAMER_BREAK_TRIGGER,bBreakTrigger);
		pAcq->getStreamerParameterSet().setValue(STREAMER_NB_ACQ,intNbAcq);
		pAcq->getStreamerParameterSet().setValue(STREAMER_DISPLAY_LOG,bDisplayLog);
		pAcq->getStreamerParameterSet().setValue(STREAMER_DISPLAY_FLAGS,bDisplayFlags);
		pAcq->getStreamerParameterSet().setValue(STREAMER_DISPLAY_DATA_FLAGS,bDataFlags);
		pAcq->getStreamerParameterSet().setValue(STREAMER_DISPLAY_COUNTERS,bDisplayCounters);
		pAcq->getStreamerParameterSet().setValue(STREAMER_ACQ_MODE,intAcqMode);
		pAcq->getStreamerParameterSet().setValue(STREAMER_DATA_FILE,strDataFile);
		pAcq->getStreamerParameterSet().setValue(STREAMER_DEST_FILE,strDestFile);
		//pAcq->getStreamerParameterSet().setValue(STREAMER_DEST_MAX_SIZE,intDestMaxSize);
		//pAcq->getStreamerParameterSet().setValue(STREAMER_2ND_DEST_FILE,str2ndDestFile);
		//pAcq->getStreamerParameterSet().setValue(STREAMER_2ND_DEST_RATIO,int2ndDestRatio);
		pAcq->getStreamerParameterSet().setValue(STREAMER_NEW_DAQ_FILE,strNewDaqFile);
		pAcq->getStreamerParameterSet().setValue(STREAMER_ZERO_SUPPRESSED,bZeroSuppressed);
		pAcq->getStreamerParameterSet().setValue(STREAMER_CONDITION_DATA,bConditionData);
		pAcq->getStreamerParameterSet().setValue(STREAMER_COMMISSIONING_LOOP,bCommissionningLoop);
		pAcq->getStreamerParameterSet().setValue(STREAMER_ACF_ACQUISITION,intAcfAcquisition);
		pAcq->getStreamerParameterSet().setValue(STREAMER_READ_NEXT_FILES,bReadNextFiles);
		pAcq->getStreamerParameterSet().setValue(STREAMER_DATA_VISU,intDataVisu);
		pAcq->getStreamerParameterSet().setValue("RUName", strRuName.toString());
		pAcq->getStreamerParameterSet().setValue("RU", intRuInstance.value_);
		pAcq->getStreamerParameterSet().setValue(STREAMER_FAKE_DATA, pWebApp->playbackMode()?1:0);
		
                if (pAcq->getStreamerParameterSet().getValue(STREAMER_SHARED_MEMORY))
                        try{
                                pAcq->setSharedMemory(&frla_, pWebApp->getApplicationContext()->getDefaultZone()->getApplicationDescriptor(strRuName,intRuInstance.value_), pWebApp);
                        }catch(const std::exception& e){
                                LOG4CPLUS_ERROR(pWebApp->getApplicationLogger(), e.what());
                        }
                else if (pAcq->getStreamerParameterSet().getValue(STREAMER_MEMTOFILE))
                        pAcq->setSharedMemory(&frla_, NULL, NULL);
                else
                        pAcq->setSharedMemory(NULL, NULL, NULL);


		//string strDefault = fileConditionHtml.toString()+DEFAULT_VALUES_EXTENSION;
	        //if (access( strDefault.c_str(), F_OK ) != -1 )
	        	//pAcq->getConditionParameterSet().loadParamValuePairsFromFile(strDefault);

		    //if (!strI2cDirectory.toString().empty()) pAcq->getCommissioningLoop().setI2cDirectory(pMainBoard->getI2cDirectory());
        } catch(std::exception& ex) {
            LOG4CPLUS_ERROR(pWebApp->getApplicationLogger(), ex.what());
        }
    }
}
/* Default() hyperDaq method */
void BoardSupervisor::Streamer::Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception) {
    /* Create HTML header */
        bRefresh=false;
	insertDescription(out);
    //xgi::Utils::getPageFooter(*out);
}
/*
void BoardSupervisor::Streamer::createHtmlHeader(xgi::Output *out){
    *out << HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
    *out << html().set("lang", "en").set("dir","ltr") << std::endl;
    if (bRefresh)//Automatic HTML refresh every 2 seconds
        *out << cgicc::meta().set("HTTP-EQUIV", "REFRESH").set("CONTENT", "2")<<endl;
//        .set("URL",getApplicationDescriptor()->getContextDescriptor()->getURL()) << endl;
        
    *out << title("BoardSupervisor::Streamer") << std::endl;
    xgi::Utils::getPageHeader(  out,
                                "BoardSupervisor",
                                pWebApp->getApplicationDescriptor()->getContextDescriptor()->getURL(),
                                pWebApp->getApplicationDescriptor()->getURN(),
                                "/xgi/images/Application.jpg" );
}
*/
void BoardSupervisor::Streamer::insertDescription(xgi::Output *out) {
	*out<<br()<<endl;
	if (pAcq->isRunning()){
		//if (!pAcq->isPauseToggling()) *out<<form(input().set("type","submit").set("value", pAcq->isPaused() ? "Resume" : "Pause")).set("action","pauseAcquisition")<<endl;

		switch (pAcq->getStreamerParameterSet().getValue(STREAMER_ACF_ACQUISITION)){
			case 2:	*out<<h2("ACF acquisition");break;
			case 3: *out<<h2("ACF acquisition in separate thread");break;
			default: 	
				*out<<h2("Complete acquisition");
				*out<<form().set("method", "POST").set("action", "validParam")<<fieldset()<<legend("Acquisition parameters:");

		}
		*out<<"<table>";
		if (pAcq->getStreamerParameterSet().getStrValue(STREAMER_DEST_FILE).length()){
			*out<<"<tr><td>Destination file:</td><td>"<<pAcq->getStreamerParameterSet().getStrValue(STREAMER_DEST_FILE)<<"</td>";
			if (pAcq->isRunning() && !pAcq->isDestFileOpen()) *out<<"<td><strong>(file not open!)</strong></td>";
			*out<<"</tr>"<<endl;
		}

		if (pAcq->getStreamerParameterSet().getValue(STREAMER_FAKE_DATA) == 1 
				&& pAcq->getStreamerParameterSet().getStrValue(STREAMER_DATA_FILE).length()){
			*out<<"<tr><td>Fake data from file:</td><td>"<<pAcq->getStreamerParameterSet().getStrValue(STREAMER_DATA_FILE)<<"</td>";
			if (pAcq->isRunning() && !pAcq->isDataFileOpen()) *out<<"<td><strong>(file not open!)</strong></td>";
			*out<<"</tr>"<<endl;
                }
		if (pAcq->getStreamerParameterSet().getStrValue(STREAMER_NEW_DAQ_FILE).length()){
			*out<<"<tr><td>New DAQ format file:</td><td>"<<pAcq->getStreamerParameterSet().getStrValue(STREAMER_NEW_DAQ_FILE)<<"</td>";
			if (pAcq->isRunning() && !pAcq->isDaqFileOpen()) 
				*out<<"<td><strong>(file not open!)</strong></td>";

			*out<<"</tr>"<<endl;
		}
		if (pAcq->getStreamerParameterSet().getValue(STREAMER_ACF_ACQUISITION) == 1){
			if (pAcq->getStreamerParameterSet().getValue(STREAMER_MEMTOFILE))
				*out<<"<tr><td>Shared memory dumped to file:</td><td>"<<pAcq->getMemDumpFilename()<<"</td></tr>"<<endl;

			*out<<tr(td(input().set("type","submit").set("value",pAcq->isSavingData()?"Stop saving":"Start saving")))<<endl;
		}
		*out<<"</table>";
		if (pAcq->getStreamerParameterSet().getValue(STREAMER_ACF_ACQUISITION) == 1)
			*out<<fieldset()<<form()<<endl; 
	} else {
		*out<<pAcq->getStreamerParameterSet().transformHtmlFileDescription(fileParamHtml.toString(), "StreamerHtmlValues");
	}


	*out<<form(input().set("type","submit").set("value","Send spurious frame")).set("action","sendSpuriousFrame")<<endl;
	//    *out<<form(input().set("type","submit").set("value","Send trigger one shot")).set("action","sendTriggerOneShot")<<endl;
	//*out<<form().set("action","forceStartXgi")<<input().set("type","submit").set("value","Force BG0-START detection").set(strEnable);
	*out<<form().set("action","forceStartXgi")<<input().set("type","submit").set("value","Force BG0-START detection");
	if (pAcq->getLastStartTime().length())
		*out<<" Start forced at: "<<pAcq->getLastStartTime();

	*out<<form()<<endl;
	if (bRefresh)
		*out<<"<a href='StreamerDefault'>stop refresh</a><br/>"<<endl;
	else if (pAcq->isRunning())
		*out<<"<a href='autoRefresh'>auto refresh</a><br/>"<<endl;

	if (pAcq->getStreamerParameterSet().getValue(STREAMER_DISPLAY_FLAGS) 
			|| pAcq->getStreamerParameterSet().getValue(STREAMER_DISPLAY_COUNTERS) 
			|| pAcq->getStreamerParameterSet().getValue(STREAMER_DATA_VISU) 
			|| pAcq->getStreamerParameterSet().getValue(STREAMER_DISPLAY_DATA_FLAGS)) {//Flags and counters if required
		*out<<table()<<tr();
		if (pAcq->getStreamerParameterSet().getValue(STREAMER_DISPLAY_FLAGS))
			*out<<td(statusFlagToHtml(pAcq->getStatusFlags(),"<a href='readFlags'>(read)</a>"));

		if (pAcq->getStreamerParameterSet().getValue(STREAMER_DISPLAY_COUNTERS))
			*out<<td(pAcq->getCountersAsHtml("")).set("valign", "top");

		if (pAcq->getStreamerParameterSet().getValue(STREAMER_DISPLAY_DATA_FLAGS))
			*out<<td(dataFlagToHtml(pAcq->getDataFlags())).set("valign", "top");

		if (pAcq->getStreamerParameterSet().getValue(STREAMER_DATA_VISU))
			*out<<td(dataVisuToHtml(pAcq->getDataVisu(), 0, 0, "")).set("valign", "top");

		*out<<tr()<<table();
	} 
	*out<<"<h2>Acquisition number: "<<pAcq->getNumAcq();
	if (pAcq->getNbCommissioningLoop()>0)
		*out<<" ("<<a("Commissioning loop").set("href",pAcq->isRunning() ? "#" : "resultCommissioningLoop")<<" # "<<pAcq->getNumCommissioningLoop()<<")";

	*out<<"</h2>"<<endl;
	if (pAcq->getTransferRate()>1000000)
		*out<<"Transfer rate: "<<(long)pAcq->getTransferRate()/1000000.0<<" Mbits/s<br>"<<endl;
	else if (pAcq->getTransferRate()>0)
		*out<<"Transfer rate: "<<(long)pAcq->getTransferRate()<<" bits/s<br>"<<endl;

	if (pAcq->getTriggerRate()>0)
		*out<<"Average Trigger rate: "<<pAcq->getTriggerRate()<<" Hz<br>"<<endl;

	if (pAcq->getTriggerRateInstant()>0)
		*out<<"Instantaneous trigger rate: "<<pAcq->getTriggerRateInstant()<<" Hz<br>"<<endl;

	if (!pAcq->getLog().empty())
		*out<<"Acquisition log:<br><pre>"<<pAcq->getLog()<<"</pre><br>"<<endl;
}

void BoardSupervisor::Streamer::startAction()	throw (toolbox::fsm::exception::Exception){
	pAcq->initLog("Acquisition started");
	if (!pAcq->getSharedMemory() && (pAcq->getReadUnitDescriptor() || pAcq->getStreamerParameterSet().getValue(STREAMER_MEMTOFILE)))
	{//Demarrage de l'acquisition sans que l'IHM n'ait valide les parametres
		if (pAcq->getReadUnitDescriptor()) {
	    	try{
		    	pAcq->setSharedMemory(&frla_, pWebApp->getApplicationContext()->getDefaultZone()->getApplicationDescriptor(strRuName,intRuInstance.value_), pWebApp);
	    	}catch(const std::exception& e){
	    		LOG4CPLUS_ERROR (pWebApp->getApplicationLogger(), e.what());
	    	}
		} else if (pAcq->getStreamerParameterSet().getValue(STREAMER_MEMTOFILE))
			pAcq->setSharedMemory(&frla_, NULL, NULL);
	}
	pAcq->run();
	bBigSizeVisu=false;
}

void BoardSupervisor::Streamer::stopAction()	throw (toolbox::fsm::exception::Exception){
	pAcq->stop();
}

void BoardSupervisor::Streamer::destroyAction()	throw (toolbox::fsm::exception::Exception){
	pAcq->stop();
	pAcq->setBoard(NULL, NULL, NULL);
}

void BoardSupervisor::Streamer::initAction()	throw (toolbox::fsm::exception::Exception){
    //pMainBoard->initialize(strHwDesc, NULL);
    //pAcq->setBoard( pMainBoard->getBeBoardInterface(),pMainBoard->getCbcInterface(), pMainBoard->getBeBoard());
    if (pAcq->getStreamerParameterSet().getValue(STREAMER_FAKE_DATA) == 0)
	pAcq->getConfigParameterSet().readAllFromBoard();

    //pAcq->initFeCbcParameterSet();
    //if (pAcq->getStreamerParameterSet().getValue(STREAMER_FAKE_DATA) == 0) pAcq->getFeCbcParameterSet().readAllFromBoard();
    
    pAcq->initConfiguration();//Pour recuperer la taille des donnees (dataSize)
}

void BoardSupervisor::Streamer::failedTransition (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception)
{
		toolbox::fsm::FailedEvent & fe = dynamic_cast<toolbox::fsm::FailedEvent&>(*e);
		LOG4CPLUS_ERROR (pWebApp->getApplicationLogger(), "Failure occurred when performing transition from: "  <<
				fe.getFromState() <<  " to: " << fe.getToState() << " exception: " << fe.getException().what() );
}

void BoardSupervisor::Streamer::AutoRefresh(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception) {
	bRefresh=true;
        pWebApp->setAutoRefresh(2);
        pWebApp->createHtmlHeader(out, VIEW_TYPE_ACQUISITION, "autoRefresh");

	pWebApp->WebShowStateMachine(out);
	insertDescription(out);
    //xgi::Utils::getPageFooter(*out);
}

void BoardSupervisor::Streamer::ValidParam(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception){
	cgicc::Cgicc cgi(in);
	if (pAcq->isRunning()){
		if (pAcq->isSavingData())
			pAcq->stopSaving();
		else{ 
	   		pAcq->getStreamerParameterSet().setFromGui(cgi, STREAMER_DEST_FILE,TEXTFIELD);
			if (pAcq->getStreamerParameterSet().getStrValue(STREAMER_DEST_FILE).empty())
		   		*out<<p("Please enter a file path").set("style","color:red")<<br()<<endl;
		   	else
	   			pAcq->startSaving();
		}
	} else {
		pAcq->getStreamerParameterSet().setFromGui(cgi, STREAMER_DEST_FILE, TEXTFIELD);//setDestinationFile(cgi["destination"]->getValue());
		//pAcq->getStreamerParameterSet().setFromGui(cgi, STREAMER_DEST_MAX_SIZE, INTEGER);
		//pAcq->getStreamerParameterSet().setFromGui(cgi, STREAMER_2ND_DEST_FILE, TEXTFIELD);
		//pAcq->getStreamerParameterSet().setFromGui(cgi, STREAMER_2ND_DEST_RATIO, INTEGER);
		pAcq->getStreamerParameterSet().setFromGui(cgi, STREAMER_LONG_PAUSE, INTEGER);//setLongPause(cgi["longPause"]->getIntegerValue());
	    pAcq->getStreamerParameterSet().setFromGui(cgi, STREAMER_SHORT_PAUSE, INTEGER);//setShortPause(cgi["shortPause"]->getIntegerValue());
	    pAcq->getStreamerParameterSet().setFromGui(cgi, STREAMER_ACQ_MODE,INTEGER);//setAcqMode(cgi["acqMode"]->getIntegerValue());
	    pAcq->getStreamerParameterSet().setFromGui(cgi, STREAMER_NB_ACQ, INTEGER);//setNbAcq(cgi["nbAcq"]->getIntegerValue());
	    pAcq->getStreamerParameterSet().setFromGui(cgi, STREAMER_DISPLAY_LOG, CHECKBOX);//setDisplayLog(cgi.queryCheckbox("log"));
	    pAcq->getStreamerParameterSet().setFromGui(cgi, STREAMER_DISPLAY_FLAGS, CHECKBOX);//setDisplayFlags(cgi.queryCheckbox("flags"));
	    pAcq->getStreamerParameterSet().setFromGui(cgi, STREAMER_DISPLAY_DATA_FLAGS, CHECKBOX);//setDisplayDataFlags(cgi.queryCheckbox("dataflags"));
	    pAcq->getStreamerParameterSet().setFromGui(cgi, STREAMER_DISPLAY_COUNTERS, CHECKBOX);//setDisplayCounters(cgi.queryCheckbox("counters"));
	    pAcq->getStreamerParameterSet().setFromGui(cgi, STREAMER_USE_HARDWARE_COUNTER, CHECKBOX);//setUseHardwareCounter(cgi.queryCheckbox("hardwareCounter"));
	    pAcq->getStreamerParameterSet().setFromGui(cgi, STREAMER_MEMTOFILE, CHECKBOX);//setMemToFile(cgi.queryCheckbox("memToFile"));
		pAcq->getStreamerParameterSet().setFromGui(cgi, STREAMER_DATA_FILE, TEXTFIELD);//setSimulatedDataFile(cgi["dataFile"]->getValue());
		pAcq->getStreamerParameterSet().setFromGui(cgi, STREAMER_READ_NEXT_FILES, CHECKBOX);
		pAcq->getStreamerParameterSet().setFromGui(cgi, STREAMER_BREAK_TRIGGER, CHECKBOX);
		pAcq->getStreamerParameterSet().setFromGui(cgi, STREAMER_NEW_DAQ_FILE, TEXTFIELD);
		pAcq->getStreamerParameterSet().setFromGui(cgi, STREAMER_ZERO_SUPPRESSED, CHECKBOX);
		pAcq->getStreamerParameterSet().setFromGui(cgi, STREAMER_CONDITION_DATA, CHECKBOX);
		pAcq->getStreamerParameterSet().setFromGui(cgi, STREAMER_COMMISSIONING_LOOP, CHECKBOX);
		pAcq->getStreamerParameterSet().setFromGui(cgi, STREAMER_FAKE_DATA, CHECKBOX);
		pAcq->getStreamerParameterSet().setFromGui(cgi, STREAMER_ACF_ACQUISITION, COMBOBOX);
		pAcq->getStreamerParameterSet().setFromGui(cgi, STREAMER_DATA_VISU, INTEGER);
	    if (cgi.queryCheckbox(STREAMER_SHARED_MEMORY))
	    	try{
		    	pAcq->setSharedMemory(&frla_, pWebApp->getApplicationContext()->getDefaultZone()->getApplicationDescriptor(strRuName,intRuInstance.value_), pWebApp);
	    	}catch(const std::exception& e){
	    		*out<<p(e.what()).set("style","color:red")<<br()<<endl;
	    	}
	    else if (pAcq->getStreamerParameterSet().getValue(STREAMER_MEMTOFILE))
			pAcq->setSharedMemory(&frla_, NULL, NULL);
	    else
	    	pAcq->setSharedMemory(NULL, NULL, NULL);
	
	    //if (pAcq->getStreamerParameterSet().getValue(STREAMER_2ND_DEST_RATIO)<=0) pAcq->getStreamerParameterSet().setValue(STREAMER_2ND_DEST_RATIO, 1000);
	}    	
	pWebApp->Default(in, out);	
}

void BoardSupervisor::Streamer::SendSpuriousFrame(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception){
	pAcq->sendSpuriousFrame();
	pWebApp->Default(in, out);	
}

void BoardSupervisor::Streamer::SendTriggerOneShot(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception){
	pAcq->sendTriggerOneShot();
	pWebApp->Default(in, out);	
}

void BoardSupervisor::Streamer::ReadFlags(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception){
	pAcq->loadFlags();
	pWebApp->Default(in, out);
}

void BoardSupervisor::Streamer::EnterData(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception){
	pWebApp->createHtmlHeader(out, VIEW_TYPE_DIAGRAM);
	*out<<h2("Enter simulation data for one CBC")<<endl;
	*out<<form().set("action","validSimulationData").set("method","POST")<<endl;
	*out<<table().set("border","1")<<tr()<<th("Word");
	*out<<th("Byte 0").set("colspan","8")<<th("Byte 1").set("colspan","8")<<th("Byte 2").set("colspan","8")<<th("Byte 3").set("colspan","8")<<tr()<<endl;
	char* pData = pAcq->getSimulationArray();
	for (int iOct=0; iOct<NB_SIMULATION_BYTES; iOct++){
		if (iOct%4==0){
			*out<<tr();
			*out<<td(toolbox::toString("%d", iOct/4)).set("align","center");
		}
//		if (iOct%4==0)			*out<<td(toolbox::toString("%d",iOct/4)).set("rowspan","4");
			
		for (int iBit=0; iBit<8; iBit++){
			bool bChecked = (pData[iOct]&(1<<(iBit^7)))!=0;
			*out<<td(input().set("type","checkbox").set("name",toolbox::toString("chk%d_%d",iOct, iBit^7))
					.set(bChecked ? "checked":"alt").set("title",toolbox::toString("Byte %d, bit %d", iOct, iBit^7)));
		}
		*out<<endl;
		if (iOct%4==3)	
			*out<<tr()<<endl;
	}
	*out<<table();
	*out<<input().set("type","submit").set("value","OK")<<form()<<endl;
	
	//xgi::Utils::getPageFooter(*out);
}
/// Show XDAQ screen to let the user enter the condition data
void BoardSupervisor::Streamer::EnterConditionData(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception){
	pWebApp->createHtmlHeader(out, VIEW_TYPE_DIAGRAM);
	if (pMainBoard->getBeBoard()){
		setConditionValuesFromVector();
		*out<<pAcq->getConditionParameterSet().transformHtmlFileDescription(fileConditionHtml.toString(), "ConditionData");
	} else 
		*out<<h3("Board not yet configured!")<<endl;
	//xgi::Utils::getPageFooter(*out);
}

void BoardSupervisor::Streamer::ValidSimulationData(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception){
	char* pData = pAcq->getSimulationArray();
	cgicc::Cgicc cgi(in);
	
	for (int iLig=0; iLig<NB_SIMULATION_BYTES; iLig++){
		pData[iLig]=0;
		for (int iBit=7; iBit>=0; iBit--){
			if (cgi.queryCheckbox(toolbox::toString("chk%d_%d",iLig,iBit)))
				pData[iLig]|=(1<<iBit);
		}
	}	
	pWebApp->Default(in, out);	
}

void BoardSupervisor::Streamer::ValidConditionData(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception){
	cgicc::Cgicc cgi(in);
	uint32_t numFE, numCBC, numPage, numReg;

	if (pMainBoard->getBeBoard()){
		for (int iCond=0; iCond<NB_CONDITION_DATA; iCond++){
			if (cgi.getElement((boost::format(CONDITION_DATA_CBC)%iCond).str()) != cgi.getElements().end()){
				pAcq->getConditionParameterSet().setFromGui(cgi, (boost::format(CONDITION_DATA_ENABLED)%iCond).str(), CHECKBOX);
				numFE=pAcq->getConditionParameterSet().setFromGui(cgi, (boost::format(CONDITION_DATA_FE_ID)%iCond).str(), COMBOBOX);
				numCBC=pAcq->getConditionParameterSet().setFromGui(cgi, (boost::format(CONDITION_DATA_CBC)%iCond).str(), INTEGER);
				numPage=pAcq->getConditionParameterSet().setFromGui(cgi, (boost::format(CONDITION_DATA_PAGE)%iCond).str(), INTEGER);
				numReg=pAcq->getConditionParameterSet().setFromGui(cgi, (boost::format(CONDITION_DATA_REGISTER)%iCond).str(), HEXA);
				pAcq->getConditionParameterSet().setFromGui(cgi, (boost::format(CONDITION_DATA_TYPE)%iCond).str(), INTEGER);
				pAcq->getConditionParameterSet().setFromGui(cgi, (boost::format(CONDITION_DATA_VALUE)%iCond).str(), INTEGER);
				Ph2_HwDescription::Module* module= pMainBoard->getBeBoard()->getModule(numFE);
				if (module){
					Ph2_HwDescription::Cbc*    pCbc = module->getCbc(numCBC);
					if (pCbc){
						bool bFound=false;
						for (Ph2_HwDescription::CbcRegMap::iterator cIt = pCbc->getRegMap().begin(); cIt!=pCbc->getRegMap().end(); cIt++)
							if (cIt->second.fPage == numPage && cIt->second.fAddress==numReg){
								pAcq->getConditionParameterSet().setValue((boost::format(CONDITION_DATA_NAME)%iCond).str(), cIt->first);
								bFound=true;
								break;
							}

						if (!bFound)
							LOG4CPLUS_ERROR(pWebApp->getApplicationLogger(), toolbox::toString("No I2C register found at page:address %d:%d for FE %d / CBC %d", numPage, numReg, numFE, numCBC)); 
					}
				}
			}
		}
		setConditionVectorFromValues();
		pWebApp->Default(in, out);	
	} else 
		*out<<h3("Board not yet configured!")<<endl;
//	pAcq->getConditionParameterSet().readAllValuesAndWriteIntoBoard(&cgi,false);
}

/// Show XDAQ screen to let the user enter the commissioning loops parameters
void BoardSupervisor::Streamer::EnterCommissioningLoop(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception){
	pWebApp->createHtmlHeader(out, VIEW_TYPE_DIAGRAM);
	pAcq->getCommissioningLoop().insertHtmlForm(out, pMainBoard, pAcq->getFeCbcParameterSet());
	//xgi::Utils::getPageFooter(*out);
}

void BoardSupervisor::Streamer::ValidCommissioningLoop(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception){
	cgicc::Cgicc cgi(in);
	pAcq->getCommissioningLoop().setValuesFromGui(cgi);
	pWebApp->Default(in, out);	
}

void BoardSupervisor::Streamer::AddCommissioningParameter(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception){
	pWebApp->createHtmlHeader(out, VIEW_TYPE_ACQUISITION);
	pAcq->getCommissioningLoop().addOneParameter();
	pAcq->getCommissioningLoop().insertHtmlForm(out, pMainBoard, pAcq->getFeCbcParameterSet());
	//xgi::Utils::getPageFooter(*out);
	
}

void BoardSupervisor::Streamer::RemoveLastCommissioningParameter(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception){
	pWebApp->createHtmlHeader(out, VIEW_TYPE_ACQUISITION);
	pAcq->getCommissioningLoop().removeLastParameter();
	pAcq->getCommissioningLoop().insertHtmlForm(out, pMainBoard, pAcq->getFeCbcParameterSet());
	//xgi::Utils::getPageFooter(*out);
	
}
/*
void BoardSupervisor::Streamer::resetAction() throw (xcept::Exception)
{
	if (  fsm_.getCurrentState() != 'F' )
		XCEPT_RAISE(xcept::Exception, "cannot reset, streamer not in failed state");

	try 
	{
		// reset state machine to initial state
		fsm_.reset();
	}
	catch (toolbox::fsm::exception::Exception & e)
	{
		XCEPT_RETHROW(xcept::Exception, "reset failed", e);
	}
}
*/
xoap::MessageReference BoardSupervisor::Streamer::forceStart(xoap::MessageReference msg) throw (xoap::exception::Exception)
{
	pAcq->forceStart();
	
	xoap::MessageReference reply = xoap::createMessage();
	xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
	xoap::SOAPName responseName = envelope.createName("resetResponse", "xdaq", XDAQ_NS_URI);
	envelope.getBody().addBodyElement ( responseName );
	return reply;
}

void BoardSupervisor::Streamer::ForceStartBg0(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception){
	pAcq->forceStart();
	pWebApp->Default(in, out);
}

void BoardSupervisor::Streamer::PauseAcquisition(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception){
	pAcq->togglePause();	
	pWebApp->Default(in, out);
}

void BoardSupervisor::Streamer::LoadFromFileHtml(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception) {
    cgicc::Cgicc cgi(in);
    cgicc::form_iterator iteFile = cgi.getElement("htmlValuesFile");
    if (iteFile != cgi.getElements().end()) {
    	if (pAcq->getStreamerParameterSet().loadParamValuePairsFromFile(**iteFile))
    		pWebApp->Default(in, out);
    	else
     		LOG4CPLUS_ERROR(pWebApp->getApplicationLogger(), toolbox::toString("Error while opening Streamer parameters HTML file: %s", (**iteFile).c_str()));
    }
}

void BoardSupervisor::Streamer::SaveToFileHtml(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception) {
	out->getHTTPResponseHeader().addHeader("type", "application/x-download");
	out->getHTTPResponseHeader().addHeader("content-disposition", "attachment;filename=streamerValues.txt");
	string strHtmlValues = pAcq->getStreamerParameterSet().nameAndValuePairs(); 
	std::ostringstream ostream;
	ostream<<(strHtmlValues.length());
	out->getHTTPResponseHeader().addHeader("content_length",ostream.str());
    *out<<strHtmlValues;
}

void BoardSupervisor::Streamer::LoadFromFileHtmlCondition(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception) {
    cgicc::Cgicc cgi(in);
    cgicc::form_iterator iteFile = cgi.getElement("htmlValuesFile");
    if (iteFile != cgi.getElements().end()) {
    	if (pAcq->getConditionParameterSet().loadParamValuePairsFromFile(**iteFile))
    		EnterConditionData(in, out);
    	else
     		LOG4CPLUS_ERROR(pWebApp->getApplicationLogger(), toolbox::toString("Error while opening Condition Data parameters HTML file: %s", (**iteFile).c_str()));
    }
}

void BoardSupervisor::Streamer::SaveToFileHtmlCondition(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception) {
	out->getHTTPResponseHeader().addHeader("type", "application/x-download");
	out->getHTTPResponseHeader().addHeader("content-disposition", "attachment;filename=streamerConditionData.txt");
	string strHtmlValues = pAcq->getConditionParameterSet().nameAndValuePairs(); 
	setConditionVectorFromValues();
	std::ostringstream ostream;
	ostream<<(strHtmlValues.length());
	out->getHTTPResponseHeader().addHeader("content_length",ostream.str());
    *out<<strHtmlValues;
}

void BoardSupervisor::Streamer::LoadFromFileCommissioningLoop(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception) {
    cgicc::Cgicc cgi(in);
    cgicc::form_iterator iteFile = cgi.getElement("htmlValuesFile");
    if (iteFile != cgi.getElements().end()) {
    	if (pAcq->getCommissioningLoop().getParameterSet().loadParamValuePairsFromFile(**iteFile))
    		EnterCommissioningLoop(in, out);
    	else
     		LOG4CPLUS_ERROR(pWebApp->getApplicationLogger(), toolbox::toString("Error while opening Condition Data parameters HTML file: %s", (**iteFile).c_str()));
    }
}

void BoardSupervisor::Streamer::SaveToFileCommissioningLoop(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception) {
	out->getHTTPResponseHeader().addHeader("type", "application/x-download");
	out->getHTTPResponseHeader().addHeader("content-disposition", "attachment;filename=streamerCommissioningLoop.txt");
	string strHtmlValues = pAcq->getCommissioningLoop().getParameterSet().nameAndValuePairs(); 
	std::ostringstream ostream;
	ostream<<(strHtmlValues.length());
	out->getHTTPResponseHeader().addHeader("content_length",ostream.str());
    *out<<strHtmlValues;
}

void BoardSupervisor::Streamer::ResultCommissioningLoop(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception){
	pWebApp->createHtmlHeader(out, VIEW_TYPE_DIAGRAM);
	for (uint32_t iCom=0; iCom<pAcq->getNbCommissioningLoop(); iCom++){
		*out<<span()<<dataVisuToHtml(pAcq->getCommissioningLoop().getDataVisuCommLoop(iCom), iCom, 
						pAcq->getCommissioningLoop().getCommLoopNbEvent(iCom), pAcq->getCommissioningLoop().getDataVisuCommLoopString(iCom));
		*out<<span()<<endl;
	}
	uint32_t* puSum=pAcq->getCommissioningLoop().createSumArray();//new uint32_t[nbBin];
	*out<<span()<<dataVisuToHtml(puSum, pAcq->getNbCommissioningLoop(), 0, "Sum");
	*out<<span()<<endl;
	delete puSum;
}

std::string BoardSupervisor::Streamer::statusFlagToHtml(uint32_t uVal, const char* small) {
    ostringstream ostream;

    ostream<<"<table border='1'><tr><th colspan='3'>Status flags <small>"<<small<<"</small></th></tr>";
    if (pMainBoard->getBeBoard()->getBoardType()=="GLIB"){
	    ostream<<"<tr><td>sTTS_code</td><td bgcolor='#C0C0C0' align='right'>"<<getBits(uVal, 3, 0) <<"</td><td>"<<sTTScodeMeaning(uVal&0xF)<<"</td></tr>"<<endl;
	    ostream<<"<tr><td>Fifo1_full (CBC)</td><td bgcolor='#C0C0C0' align='right'>"<<((uVal>>4)&1)<<"</td><td>"<<(((uVal>>4)&1)?"Full":"not full")<<"</td></tr>"<<endl;
	    ostream<<"<tr><td>Fifo2_full (L1A)</td><td bgcolor='#C0C0C0' align='right'>"<<((uVal>>5)&1)<<"</td><td>"<<(((uVal>>5)&1)?"Full":"not full")<<"</td></tr>"<<endl;
	    ostream<<"<tr><td>cdr_lol</td><td bgcolor='#C0C0C0' align='right'>"<<((uVal>>7)&1)<<"</td><td><img height='20' src='/img/"
		    <<(((uVal>>7)&1)?"red.jpg' alt='OFF'/>" : "green.jpg' alt='ON'/>")<<"</td></tr>"<<endl;
	    ostream<<"<tr><td>cdr_los</td><td bgcolor='#C0C0C0' align='right'>"<<((uVal>>8)&1)<<"</td><td><img height='20' src='/img/"
		    <<(((uVal>>8)&1)?"red.jpg' alt='OFF'/>" : "green.jpg' alt='ON'/>")<<"</td></tr>"<<endl;
	    ostream<<"<tr><td>cdr_clk_locked</td><td bgcolor='#C0C0C0' align='right'>"<<((uVal>>6)&1)<<"</td><td><img height='20' src='/img/"
		    <<(((uVal>>6)&1)?"green.jpg' alt='ON'/>" : "red.jpg' alt='OFF'/>")<<"</td></tr>"<<endl;
	    ostream<<"<tr><td>FSM_sTTS_ctrl</td><td bgcolor='#C0C0C0' align='right'>"<<getBits(uVal,12,9)<<"</td><td/>"<<endl;
	    ostream<<"<tr><td>Spurious frame</td><td bgcolor='#C0C0C0' align='right'>"<<((uVal>>13)&1)<<"</td><td"<<(((uVal>>13)&1)?"Detected":"")<<"</td></tr>"<<endl;
    } else {
	    ostream<<"<tr><td>Start valid</td><td bgcolor='#C0C0C0' align='right'>"<<((uVal>>30)&1)<<"</td><td>"<<"</td></tr>"<<endl;
	    ostream<<"<tr><td>Evt buffer empty</td><td bgcolor='#C0C0C0' align='right'>"<<((uVal>>31)&1)<<"</td><td>"<<"</td></tr>"<<endl;
	    ostream<<"<tr><td>sram active</td><td bgcolor='#C0C0C0' align='right'>"<<getBits(uVal,26,25)<<"</td><td>"<<((uVal>>26)&3)<<"</td></tr>"<<endl;
	    ostream<<"<tr><td>Evt counter cleared</td><td bgcolor='#C0C0C0' align='right'>0x"<<hex<<(uVal&0x1FFFFF)<<dec<<"</td><td>"<<(uVal&0x1FFFFF)<<"</td></tr>"<<endl;
    }
    ostream<<"</table>";
    return ostream.str();
}

std::string BoardSupervisor::Streamer::dataFlagToHtml(uint32_t dataFlags) {
    ostringstream ostream;

    ostream<<"<table border='1'><tr><th colspan='3'>Data flags</th></tr>"<<endl;
    ostream<<"<tr><td>sTTS_code</td><td bgcolor='#C0C0C0' align='right'>"<<getBits(dataFlags, 13, 10)
    <<"</td><td>"<<sTTScodeMeaning((dataFlags&0x0001E000)>>10)<<"</td></tr>"<<endl;
    ostream<<"<tr><td>Spurious_frame_detect</td><td bgcolor='#C0C0C0' align='right'>"<<((dataFlags>>14)&1)<<"</td><td"
    	<<(((dataFlags>>14)&1)?"Detected":"")<<"</td></tr>"<<endl;
    ostream<<"<tr><td>cdr_clk_locked_detect</td><td bgcolor='#C0C0C0' align='right'>"<<((dataFlags>>15)&1)<<"</td><td><img height='20' src='/img/"
    <<(((dataFlags>>15)&1)?"green.jpg' alt='ON'/>" : "red.jpg' alt='OFF'/>")<<"</td></tr>"<<endl;
    ostream<<"<tr><td>cdr_los_detect</td><td bgcolor='#C0C0C0' align='right'>"<<((dataFlags>>16)&1)<<"</td><td><img height='20' src='/img/"
    <<(((dataFlags>>16)&1)?"red.jpg' alt='OFF'/>" : "green.jpg' alt='ON'/>")<<"</td></tr>"<<endl;
    ostream<<"<tr><td>cdr_lol_detect</td><td bgcolor='#C0C0C0' align='right'>"<<((dataFlags>>17)&1)<<"</td><td><img height='20' src='/img/"
    <<(((dataFlags>>17)&1)?"red.jpg' alt='OFF'/>" : "green.jpg' alt='ON'/>")<<"</td></tr>"<<endl;
    ostream<<"</table>";
    return ostream.str();
}

std::string BoardSupervisor::Streamer::dataVisuToHtml(uint32_t *puData, uint32_t uNum, uint32_t nbEvt, const std::string& strLabel) {
    ostringstream ostream;
    uint32_t uVal, uMax=0, colMax=0;
    for (uVal=0; puData!=NULL && uVal<NB_STRIPS_CBC2; uVal++){
	if (uMax<puData[uVal]) uMax=puData[uVal];
	if (puData[uVal]>0) colMax=uVal+1;
    }
    ostream<<"<table><tr><td>"<<strLabel;
    if (nbEvt>0)
	ostream<<"<br>"<<nbEvt<<" events";

    ostream<<"</td><td valign='top'>"<<uMax<<"</td><td>"<<endl;
    ostream<<"<canvas id='cnvDataVisu"<<uNum<<"' width='"<<(bBigSizeVisu?1024:300)<<"' height='"<<(bBigSizeVisu?682:200)<<"' title='"<<dataVisuTooltip();
    ostream<<"' style='border:1px solid black;' onclick='window.location=\"toggleDataVisuSize\";'> Your browser does not support the HTML5 canvas tag.</canvas>"<<endl;
    ostream<<"<script> var data=[";
    for (uVal=0; uVal<colMax; uVal++)
	ostream<<puData[uVal]<<",";

    ostream<<"];"<<endl<<"displayData();"<<endl;
    ostream<<"function displayData(){"<<endl;
    ostream<<"var c = document.getElementById('cnvDataVisu"<<uNum<<"');"<<endl;
    ostream<<"var ctx = c.getContext('2d');"<<endl;
    ostream<<"ctx.fillStyle='blue';"<<endl;
    ostream<<"for (col=0; col<data.length; col++)"<<endl;
    ostream<<"  ctx.fillRect(col*c.width/data.length,("<<uMax<<"-data[col])*c.height/"<<uMax<<", c.width/data.length-1,c.height);}"<<endl;
    ostream<<"</script>"<<endl;
    ostream<<"</td></tr>"<<endl;
    if (colMax>0) 
	ostream<<"<tr><td colspan='3' align='right'>"<<colMax-1<<"</td></tr>";

    ostream<<"</table>"<<endl;
    return ostream.str();
}

std::string BoardSupervisor::Streamer::dataVisuTooltip(){
	switch (pAcq->getStreamerParameterSet().getValue(STREAMER_DATA_VISU)){
	case 1:return "TDC (Time to Digit Converter) phases";
	case 2:return "Error bits : 2 by CBC: A0 at 0 and 1, B0 at 10 and 11, A1 at 20 and 21, ...";
	case 3:return "Bad counters: bin 0 is the number of events where L1A counter is different than software counter,\n bin 1 is for CBC counter, \n bin 2 is when an event bunch/orbit/lumisection counter is less than in the previous event";
	case 4:return "Non identical events: number of events whose value is different than in the first event. \nTo be used with fake readout data";
	case 5:return "Non identical events every 2: number of odd and even event whose value is different than in the first event or second event according to event number parity. \nTo be used with internally generated data";
	case 100:return "CBC A0 strips";
	case 101:return "CBC B0 strips";
	case 102:return "CBC A1 strips";
	case 103:return "CBC B1 strips";
	case 104:return "CBC A2 strips";
	case 105:return "CBC B2 strips";
	case 106:return "CBC A3 strips";
	case 107:return "CBC B3 strips";
	case 108:return "CBC A4 strips";
	case 109:return "CBC B4 strips";
	case 110:return "CBC A5 strips";
	case 111:return "CBC B5 strips";
	case 112:return "CBC A6 strips";
	case 113:return "CBC B6 strips";
	case 114:return "CBC A7 strips";
	case 115:return "CBC B7 strips";
	default: return "Unknown ?!";
	}
}
void BoardSupervisor::Streamer::ToggleDataVisuSize(xgi::Input * in, xgi::Output * out )  throw (xgi::exception::Exception){
	bBigSizeVisu = !bBigSizeVisu;
	pWebApp->Default(in, out);
}

std::string BoardSupervisor::Streamer::getBits(uint32_t uVal, short begin, short end) {
    ostringstream ostream;
    for (short sBit=begin; sBit>=end; sBit--)
        ostream<< ((uVal>>sBit)&1);

    return ostream.str();
}


/* Reverse ingeenered Value/Meaning map */
const char* BoardSupervisor::Streamer::sTTScodeMeaning(uint32_t uVal) {
    switch (uVal) {
    case 1 :
        return "Warn";
    case 2 :
        return "Out Of Sync";
    case 7 :
        return "Ready";
    case 8 :
        return "Busy";
    case 12 :
        return "Not Connected";
    case 15:
        return "Error";
    default:
        return "Unknown";
    }
}

void BoardSupervisor::Streamer::setConditionValuesFromVector(){
		for (uint32_t iCond=0; iCond<NB_CONDITION_DATA && iCond<vecConditionKey.size(); iCond++){
/*		acquisition.getConditionParameterSet().mapToSerializable(this, (boost::format(CONDITION_DATA_ENABLED)%iCond).str(), 	&pbConditionEnabled[iCond]);
		acquisition.getConditionParameterSet().mapToSerializable(this, (boost::format(CONDITION_DATA_FE_ID)%iCond).str(), 	&pintConditionFeId[iCond]);
		acquisition.getConditionParameterSet().mapToSerializable(this, (boost::format(CONDITION_DATA_CBC)%iCond).str(), 	&pintConditionCbc[iCond]);
		acquisition.getConditionParameterSet().mapToSerializable(this, (boost::format(CONDITION_DATA_PAGE)%iCond).str(), 	&pintConditionPage[iCond]);
		acquisition.getConditionParameterSet().mapToSerializable(this, (boost::format(CONDITION_DATA_REGISTER)%iCond).str(), 	&pintConditionRegister[iCond]);
		acquisition.getConditionParameterSet().mapToSerializable(this, (boost::format(CONDITION_DATA_TYPE)%iCond).str(), 	&pintConditionType[iCond]);
		acquisition.getConditionParameterSet().mapToSerializable(this, (boost::format(CONDITION_DATA_VALUE)%iCond).str(), 	&pintConditionValue[iCond]);
		acquisition.getConditionParameterSet().mapToSerializable(this, (boost::format(CONDITION_DATA_NAME)%iCond).str(), 	&pstrConditionName[iCond]); 
*/ 
			pAcq->getConditionParameterSet().setValue((boost::format(CONDITION_DATA_ENABLED)%iCond).str(),vecConditionKey[iCond]&0xFF000000 ? 1 : 0);
			pAcq->getConditionParameterSet().setValue((boost::format(CONDITION_DATA_FE_ID)%iCond).str(),vecConditionKey[iCond]&0xFF);
			pAcq->getConditionParameterSet().setValue((boost::format(CONDITION_DATA_CBC)%iCond).str(),(vecConditionKey[iCond]>>8)&0xF);
			pAcq->getConditionParameterSet().setValue((boost::format(CONDITION_DATA_PAGE)%iCond).str(),(vecConditionKey[iCond]>>12)&0xF);
			pAcq->getConditionParameterSet().setValue((boost::format(CONDITION_DATA_REGISTER)%iCond).str(),(vecConditionKey[iCond]>>16)&0xFF);
			pAcq->getConditionParameterSet().setValue((boost::format(CONDITION_DATA_TYPE)%iCond).str(),(vecConditionKey[iCond]>>24)&0xFF);
			pAcq->getConditionParameterSet().setValue((boost::format(CONDITION_DATA_VALUE)%iCond).str(),vecConditionValue[iCond]);
		}
}

void BoardSupervisor::Streamer::setConditionVectorFromValues(){
	ParameterSetUhal& parCond= pAcq->getConditionParameterSet();
	for (uint32_t iCond=0; iCond<NB_CONDITION_DATA && iCond<vecConditionKey.size(); iCond++){
		vecConditionKey[iCond]= (parCond.getValue((boost::format(CONDITION_DATA_FE_ID)%iCond).str())&0xFF)
			| (parCond.getValue((boost::format(CONDITION_DATA_CBC)%iCond).str())&0xF)<<8
			| (parCond.getValue((boost::format(CONDITION_DATA_PAGE)%iCond).str())&0xF)<<12
			| (parCond.getValue((boost::format(CONDITION_DATA_REGISTER)%iCond).str())&0xFF)<<16
			| ( parCond.getValue((boost::format(CONDITION_DATA_ENABLED)%iCond).str()) ? (parCond.getValue((boost::format(CONDITION_DATA_TYPE)%iCond).str())&0xFF)<<24 : 0);
		
		vecConditionValue[iCond] = parCond.getValue((boost::format(CONDITION_DATA_VALUE)%iCond).str());

	}
}

