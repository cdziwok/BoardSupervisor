/* Copyright 2012 Institut Pluridisciplinaire Hubert Curien
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

   FileName : 		I2cCommunication.cc
   Used in : 		CBCDAQ
   Programmer : 	Christian Bonnin
   Version : 		
   Date of creation : 30/07/2012
   Support : 		mail to : christian.bonnin@iphc.cnrs.fr
*/
#include <unistd.h>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include "toolbox/string.h"
#include "I2cCommunication.h"
#include "Board.h"
#include "HWInterface/BeBoardInterface.h"
#include "HWInterface/CbcInterface.h"
#include "HWDescription/BeBoard.h"
#include "HWDescription/Module.h"
#include "HWDescription/Cbc.h"
#include "HWDescription/CbcRegItem.h"

#define I2C_CTRL_ENABLE 	0x000009F4
#define I2C_CTRL_DISABLE	0
#define I2C_STROBE			1
#define I2C_M16B			0
#define I2C_MEM				1
//#define I2C_SLAVE			0x40
#define I2C_WRITE_ADDR		0x09
#define I2C_READ_ADDR		0x06


using namespace std;

I2cCommunication::I2cCommunication(const std::string& strSet, const std::string& strCmd, const std::string& strRep, uint32_t iSlave){
	 strSettings = strSet;
	 strCommand = strCmd;
	 strReply = strRep;
	 iI2cSlave = iSlave;
}

void I2cCommunication::init(){
strLog.clear();
}

bool I2cCommunication::reset(Ph2_HwInterface::BeBoardInterface* pBoardInterface, Ph2_HwDescription::BeBoard* pBoard){
	enable(pBoardInterface, pBoard);
	bool bSuccess = checkRead(pBoardInterface, pBoard);
	if (bSuccess){
		bSuccess = write(pBoardInterface, pBoard, I2C_WRITE_ADDR, 0x20);
	}		
	if (bSuccess){
		bSuccess = checkRead(pBoardInterface, pBoard);
	}
	if (bSuccess){
		bSuccess = write(pBoardInterface, pBoard, I2C_WRITE_ADDR, 0);
	}		
	if (bSuccess){
		bSuccess = checkRead(pBoardInterface, pBoard); 
	}
	disable(pBoardInterface, pBoard);		
	return bSuccess;	
}

bool I2cCommunication::write3values(Ph2_HwInterface::BeBoardInterface* pBoardInterface, Ph2_HwDescription::BeBoard* pBoard){
	enable(pBoardInterface, pBoard);
	bool bSuccess = write(pBoardInterface, pBoard, 0x08, 0xC2);
	if (bSuccess){
		bSuccess = write(pBoardInterface, pBoard, 0x09, 0);
	}		
	if (bSuccess){
		bSuccess = write(pBoardInterface, pBoard, 0x11, 0);
	}		
	disable(pBoardInterface, pBoard);		
	return bSuccess;	
}

// "-> enabling sys i2c controller  "
void I2cCommunication::enable(Ph2_HwInterface::BeBoardInterface* pBoardInterface, Ph2_HwDescription::BeBoard* pBoard){
	strLog="Enabling I2C controller\n";
	pBoardInterface->WriteBoardReg( pBoard, strSettings, I2C_CTRL_ENABLE);
	usleep(100000);
}

//	disabling usr i2c controller 
void I2cCommunication::disable(Ph2_HwInterface::BeBoardInterface* pBoardInterface, Ph2_HwDescription::BeBoard* pBoard){
	pBoardInterface->WriteBoardReg( pBoard,strSettings, I2C_CTRL_DISABLE);
	strLog.append("Disabling I2C controller\n");
}

uint32_t I2cCommunication::send(Ph2_HwInterface::BeBoardInterface* pBoardInterface, Ph2_HwDescription::BeBoard* pBoard, uint16_t addr, uint16_t data, bool bWrite){
	uint32_t fmc_vtrx_comm = I2C_STROBE<<31 | I2C_M16B<<25 | I2C_MEM<<24 | (bWrite?1:0)<<23 | iI2cSlave<<16 | addr<<8 | data;
	pBoardInterface->WriteBoardReg( pBoard,strCommand, fmc_vtrx_comm);
//	strLog.append(toolbox::toString("Write %X at %s\n", fmc_vtrx_comm, strCommand.c_str()));
//	usleep(100000);
	uhal::ValWord<uint32_t> reply = pBoardInterface->ReadBoardReg(pBoard,strReply);
//	usleep(100000);
	return reply & 0xff;	
}

uint32_t I2cCommunication::read(Ph2_HwInterface::BeBoardInterface* pBoardInterface, Ph2_HwDescription::BeBoard* pBoard, uint16_t addr){
	uint32_t uRet=send(pBoardInterface, pBoard, addr, 0, false);
	std::ostringstream ostream;
	ostream<<"Read value: "<<std::hex<<std::uppercase<<std::setw(2)<<std::setfill('0')<<uRet<<" at address: "<<addr<<" slave: "<<iI2cSlave<<"\n";
	strLog.append(ostream.str());
	return uRet;
}

bool I2cCommunication::checkRead(Ph2_HwInterface::BeBoardInterface* pBoardInterface, Ph2_HwDescription::BeBoard* pBoard){
	return read(pBoardInterface, pBoard, I2C_READ_ADDR)<0x10000000;
}

bool I2cCommunication::write(Ph2_HwInterface::BeBoardInterface* pBoardInterface, Ph2_HwDescription::BeBoard* pBoard, uint16_t addr, uint16_t data){
	uint32_t reply = send(pBoardInterface, pBoard, addr, data, true);
	std::ostringstream ostream;
	ostream<<"Write data: "<<std::hex<<std::uppercase<<std::setw(2)<<std::setfill('0')<<data<<" at address: "<<addr<<" (reply: "<<reply<<")\n";
	strLog.append(ostream.str());
	return reply<0x10000000;
}

std::string I2cCommunication::showValuesFromI2cAndFile( Ph2_HwInterface::CbcInterface *cbcInterface,  Ph2_HwDescription::BeBoard *beBoard, bool bRead){
	std::ostringstream ostream, oLast;
	string strLine, strName;//, strPath = strValuesDir+"/"+strValuesFile;
	string strBackColor(bRead?"lime":"cyan");
	uint32_t uPage, uAddr, uDef, uRead,  uLine=0;// uWrite, uNum=0;
	vector<uint32_t> vecReq;
	uint32_t uFE=0, uCBC=0;
	
	if (strValuesFile.empty()) return "";
	extractFeAndCbcNumbers(strValuesFile, &uFE, &uCBC);
	Module* module= beBoard->getModule(uFE);
 	Cbc*    pCbc = module->getCbc(uCBC);
	if (bRead){
		std::vector<std::string> vecReg;

		for ( auto& it : pCbc->getRegMap() )
			vecReg.push_back( it.first );

		cbcInterface->ReadCbcMultReg(pCbc, vecReg); // -> CtaFWInterface::ReadCbcBlockReg -> CtaFWInterface::ReadI2C
	}
		
	ostream<<"<strong>"<<strValuesFile<<"</strong>"<<endl;
        ostream<<"<small><a title='Set default values into memory values' href='i2cDefaultToMem?cbc="<<strValuesFile<<"'>default to memory</a></small><br/><br/>"<<endl;
	ostream<<"<table border='1'>"<<endl;
	ostream<<"<tr><th>Name</th><th>Page</th><th>Register</th><th>Default value</th>";
	ostream<<"<th style='background-color:"<<strBackColor<<"'>"<<(bRead?"Read":"Write")<<" value</th></tr>"<<endl;
	oLast<<"<form action='i2cWriteOneValue' name='frmWriteOne'><tr><td><select name='regName'>";
//	filValues.open(strPath.c_str());
	//uint32_t uFirstVal=0, uData;
	for (CbcRegMap::iterator cIt = pCbc->getRegMap().begin(); cIt != pCbc->getRegMap().end() ; cIt++){
		uLine++;uDef=0;uRead=0;//uWrite=0;
		uDef=cIt->second.fDefValue;
		uRead=cIt->second.fValue;
		uPage=cIt->second.fPage;
		uAddr=cIt->second.fAddress;
			//if (valuesFromLine(strLine, false, strName, &uPage, &uAddr, &uDef, &uWrite)){
		ostream<<"<tr><td";
		//if (((uData>>21)&0xF)!=uFE || ((uData>>17)&0xF)!=uCBC || ((uData>>16)&0x1)!=uPage || ((uData>>8)&0xFF)!=uAddr)
					//ostream<<" style='background-color:red' title='"<<"FE="<<((uData>>21)&0xF)<<", CBC=0x"<<hex<<((uData>>17)&0xF)<<", page="<<((uData>>16)&0x1)
							//<<", addr=0x"<<((uData>>8)&0xFF)<<", data=0x"<<uData<<"'";
					
		ostream<<">"<<cIt->first<<"</td><td>"<<hex<<uPage<<"</td><td>0x"<<setfill('0')<<setw(2)<<uAddr<<"</td>";
		if (uDef == uRead){
					ostream<<"<td colspan='2' align='center' style='background-color:"<<strBackColor<<"'>0x"<<setw(2)<<uDef;
					ostream<<"</td><tr>"<<endl;
		}else{
					ostream<<"<td>0x"<<uDef<<"</td>";
					ostream<<"<td style='background-color:"<<strBackColor<<"'>0x"<<setw(2)<<uRead<<"</td></tr>"<<endl;
		}
		oLast<<"<option>"<<cIt->first<<"</option>";
	}//for
	oLast<<"</select></td>"<<endl;
	oLast<<"<td>Value:</td><td>0x<input name='regValue' type='text' size='3'></td>"<<endl;
	oLast<<"<td colspan='2'><input type='hidden' value='' name='checkedBox'><input type='submit' value='Write one value' title='Write one value into the checked CBCs or: ";
	oLast<<strValuesFile<<" if none is checked' onclick='document.frmWriteOne.checkedBox.value=getCheckedBoxes();'></td></tr></form>";
	//filValues.close();
	//Work around of a VHDL bug that fails to read more than once the first value: the first value is written again.
//	if (vecReq.size()>1){
//		vecReq.erase(vecReq.begin()+1, vecReq.begin()+vecReq.size()-1);
//		cout<<"rewrite first value: "<<hex<<uFirstVal<<dec<<endl;
//		vecReq[0]=vecReq[0] & 0xFFF0 | uFirstVal;
//		sleep(1);
//		blockSendRequest(lBoard, strSram, strSramUserLogic, vecReq, true);		
//	}
//	
	ostream<<oLast.str()<<endl;
	ostream<<"<form action='i2cSaveToFile'><tr border='0'><td colspan='5' align='center'><input type='submit' value='Save to file'></input></td></tr></form>";
	ostream<<"</table>"<<endl;
	//ostream<<"File: "<<strPath<<"<br/>"<<endl;
	return ostream.str();	
}

void I2cCommunication::setDefaultValuesIntoMem(Ph2_HwDescription::BeBoard *beBoard){
	uint32_t uFE=0, uCBC=0;
	
	if (strValuesFile.empty()) return ;
	extractFeAndCbcNumbers(strValuesFile, &uFE, &uCBC);
	Module* module= beBoard->getModule(uFE);
 	Cbc*    pCbc = module->getCbc(uCBC);
        for ( auto& it : pCbc->getRegMap() )
	        it.second.fValue=it.second.fDefValue;
}
/** Write one hexadecimal I2C value into the selected CBCs or the current one if none is selected
 * \param	strName	CBC register name
 * \param	strValue Value to be written
 * \param 	CBC names (like FE0CBC0) separated by spaces
 */
void I2cCommunication::writeOneCbcValue(Ph2_HwInterface::CbcInterface *cbcInterface, Ph2_HwDescription::BeBoard *beBoard, 
					const std::string& strName, const std::string& strValue, const std::string& strChecked){
	uint32_t uFE=0, uCBC=0, uVal=(uint32_t)strtol(strValue.c_str(), NULL, 16);
	
	init();
	if (!strChecked.empty()){
		istringstream iss(strChecked);
		while(iss){
			string strCbc;
			iss >> strCbc;
			if (extractFeAndCbcNumbers(strCbc, &uFE, &uCBC)){
				strLog.append(toolbox::toString("FE %d, CBC %d, %s &lt;- 0x%02X: ", uFE, uCBC, strName.c_str(), uVal));
				Module* module= beBoard->getModule(uFE);
				Cbc*    pCbc = module->getCbc(uCBC);
				if (cbcInterface->WriteCbcReg(pCbc, strName, uVal, true))
					strLog.append("OK\n");
				else
					strLog.append("Error!\n");
			}
		}
	} else if (!strValuesFile.empty()){
		extractFeAndCbcNumbers(strValuesFile, &uFE, &uCBC);
		strLog.append(toolbox::toString("FE %d, CBC %d, %s &lt;- 0x%02X: ", uFE, uCBC, strName.c_str(), uVal));
		Module* module= beBoard->getModule(uFE);
		Cbc*    pCbc = module->getCbc(uCBC);
		if (cbcInterface->WriteCbcReg(pCbc, strName, uVal, true))
			strLog.append("OK\n");
		else
			strLog.append("Error!\n");
	}
}

bool I2cCommunication::writeFileValues(Ph2_HwInterface::CbcInterface *cbcInterface,  Ph2_HwDescription::BeBoard *beBoard, bool bCheck){
	uint32_t uFE=0, uCBC=0;
	/*uint32_t uPage, uAddr, uDef, uRead, uWrite, uLine=0;
	string strName, strLine, strPath = strValuesDir+"/"+strValuesFile;;
	vector<uint32_t> vecReq ;
	
	std::ifstream filValues(strPath.c_str());
	if (!filValues.good()){
		return false;
	}*/
	init();
	extractFeAndCbcNumbers(strValuesFile, &uFE, &uCBC);
	Module* module= beBoard->getModule(uFE);
 	Cbc*    pCbc = module->getCbc(uCBC);
	try{
		cbcInterface->ConfigureCbc(pCbc, bCheck);
	} catch(std::exception& e) {
		strLog.append(toolbox::toString("FE %d, CBC %d: ERROR: %s\n", uFE, uCBC, e.what()));
		return false;
	}
/*	
	init();
	blockInit(lBoard);
	while (filValues.good()){//!filValues.eof()){
		getline(filValues, strLine);
		uLine++;uDef=0;uRead=0;uWrite=0;
		if (strLine[0]!='#' && strLine[0]!='*' && strLine.length()>0){
			if (valuesFromLine(strLine, true, strName, &uPage, &uAddr, &uDef, &uWrite)){
				blockAdd(vecReq, uFE, uCBC, uPage, uAddr, uWrite);
			}
		}
	}//while
	filValues.close();
			
	blockSendRequest(lBoard, vecReq, true);		
	blockWaitAndReadResult(lBoard, vecReq.size());//Values are read but not used*/
	return true;
}

void I2cCommunication::setValuesFile(const std::string& strFilename){
	strValuesFile = strFilename;
}

void I2cCommunication::setValuesDirectory(const std::string& strDir){
	strValuesDir = strDir;
}
/** Extract the FE and CBC numbers from a CBC name 
 * \return true if numbers could be extracted */
bool I2cCommunication::extractFeAndCbcNumbers(const std::string& strCbcName, uint32_t *puFe, uint32_t *puCbc){
	if (strCbcName.substr(0,2).compare("FE")==0){
		*puFe=(uint32_t)strtol(strCbcName.substr(2,1).c_str(),NULL,10);
		if (strCbcName.substr(3,3).compare("CBC")==0){
			*puCbc=(uint32_t)strtol(strCbcName.substr(6,strCbcName.length()-6).c_str(),NULL,10);
			return true;
		}
	}
	return false;
}

std::string I2cCommunication::getValuesAsText(Ph2_HwInterface::CbcInterface *cbcInterface, Ph2_HwDescription::BeBoard *beBoard, const std::string& strCbc){
	std::ostringstream ostream;
	uint32_t uFE=0, uCBC=0;
	
	if (strCbc.empty()) return "";
	
	ostream<<"* Regname\tPage\tRegAddr\tDefVal\tWriteValue\n";
	extractFeAndCbcNumbers(strCbc, &uFE, &uCBC);
	Module* module= beBoard->getModule(uFE);
 	Cbc*    pCbc = module->getCbc(uCBC);
	for ( auto& it : pCbc->getRegMap() ){
		ostream<<it.first<<"\t0x"<<hex<<setw(2)<<setfill('0')<<(uint32_t)it.second.fPage;
		ostream<<"\t0x"<<setw(2)<<setfill('0')<<(uint32_t)it.second.fAddress<<"\t0x"<<setw(2)<<setfill('0')<<(uint32_t)it.second.fDefValue;
		ostream<<"\t0x"<<setw(2)<<setfill('0')<<(uint32_t)it.second.fValue<<endl;
	}
	return ostream.str();
}

void I2cCommunication::checkAllValues(Ph2_HwInterface::CbcInterface *cbcInterface, Ph2_HwDescription::BeBoard *beBoard, const std::string& strChecked){
	uint32_t uFE=0, uCBC=0;
	
	init();
	if (!strChecked.empty()){
		istringstream iss(strChecked);
		while(iss){
			string strCbc;
			iss >> strCbc;
			if (extractFeAndCbcNumbers(strCbc, &uFE, &uCBC)){
				checkAllValuesForOneCbc(cbcInterface, beBoard, uFE, uCBC);
			}
		}
	} else if (!strValuesFile.empty()){
		extractFeAndCbcNumbers(strValuesFile, &uFE, &uCBC);
		checkAllValuesForOneCbc(cbcInterface, beBoard, uFE, uCBC);
	} 
}

void I2cCommunication::checkAllValuesForOneCbc(Ph2_HwInterface::CbcInterface *cbcInterface, Ph2_HwDescription::BeBoard *beBoard, uint32_t uFE, uint32_t uCBC){
	bool bOk=true;
	map <string, uint8_t> mapRegVal;
	std::vector<std::string> vecReg;

	Module* module= beBoard->getModule(uFE);
	Cbc*    pCbc = module->getCbc(uCBC);
	vecReg.reserve(pCbc->getRegMap().size());
	for ( auto& it : pCbc->getRegMap() ){
		mapRegVal[it.first] = it.second.fValue;
		vecReg.push_back( it.first ); 
	}
	try{
	cbcInterface->ReadCbcMultReg(pCbc, vecReg);
	for ( auto& it : pCbc->getRegMap() )
		if (it.second.fValue != mapRegVal[it.first]){
			bOk=false;
			strLog.append(toolbox::toString("FE %d, CBC %d, %s: expected 0x%02X but read 0x%02X\n", uFE, uCBC, it.first.c_str(), (uint32_t)mapRegVal[it.first], it.second.fValue));
		}

	} catch(std::exception& e) {
		strLog.append(toolbox::toString("FE %d, CBC %d: ERROR: %s\n", uFE, uCBC, e.what()));
		bOk=false;
	}
	if (bOk)
		strLog.append(toolbox::toString("FE %d, CBC %d: OK\n", uFE, uCBC));
}

