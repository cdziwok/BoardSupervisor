//Copied from TrackerDAQ sources. Original link: ../../../../TrackerOnline/2005/TrackerXdaq/src/common/TSem.cc
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "TSem.h"

# include <sys/types.h>
# include <sys/ipc.h>
# include <sys/sem.h>
# include <sys/shm.h>
# include <errno.h>
#if defined(__GNU_LIBRARY__) && !defined(_SEM_SEMUN_UNDEFINED)
/* union semun is defined by including <sys/sem.h> */
#else
/* according to X/OPEN we have to define it ourselves */
union semun {
        int val;                    /* value for SETVAL */
        struct semid_ds *buf;       /* buffer for IPC_STAT, IPC_SET */
        unsigned short int *array;  /* array for GETALL, SETALL */
        struct seminfo *__buf;      /* buffer for IPC_INFO */
};
#endif

void rmsem(int ie,void *arg)
{
    int *c = (int*) arg;int ic=*c;
    printf("Rem Sem I got an exit %d with %d as arg \n",ie,ic);
      struct semid_ds *shm_ds;
      if (semctl(ic, 0,IPC_RMID, 0) <0)
        {
          printf("Y a un problem \n");
          perror("semctl");
        }
}

#undef TSEMUSED

TSem::TSem(char *cdev,int type)  throw (std::string)
{
#ifdef TSEMUSED
  memset(fName,0,256);
  strncpy(fName,cdev,strlen(cdev));

  isServer = (type == TSem_CREATE);
  printf("Semaphore name name %s \n",fName);
  printf("Semaphore type %d %d %d \n",type,TSem_CREATE,isServer);
  if (isServer)
   {
     printf("File Creation \n");
     char cmd[256];
     sprintf(cmd,"touch %s.sem",fName);
     system(cmd);
   }
  sprintf(fSemName,"%s.sem",fName);
  /* Get a key from the file shm_devname */
  if((fSemkey = ftok(fSemName, '\x01')) <0)
    {
      perror("TSem: ftok problem");
      throw std::string("TSem: ftok problem");
    }
  int flag = (isServer)?IPC_CREAT:0;
  
  flag |=384;
  if((fSemid = semget( fSemkey, 1, flag)) <0)
    {
      printf("TSem: semget problem : %d %d\n", errno,flag);
      perror("TSem: semget");
      throw std::string("TSem: semget problem");
    }
  printf("Semaphore Id %d %d %d \n",fSemkey,fSemid,flag);
//   if (isServer) 
//     {
//       printf("TSem : lock the semaphore\n");
//       (fSembuf[0]).sem_num = 0;
//       (fSembuf[0]).sem_op = 0;
//       (fSembuf[0]).sem_flg = 0;

//       if((semop( fSemid, fSembuf, 1) <0))
//         {
//           perror("TSem: semop problem");
//           exit(-1);
//         }

//       //  on_exit(rmsem,(void*)&fSemid);
//     }
#else
  ptlock_ = new PTLock();
#endif
}
TSem::~TSem()  throw (std::string)
{
#ifdef TSEMUSED
  if (isServer)
    {
      struct semid_ds *sem_ds;
      if (semctl(fSemid, 0,IPC_RMID, 0) <0)
        {
          perror("semctl");
	  throw std::string("TSem~ : semctl problem");
        }
    }

#else
  delete ptlock_;
#endif
}

void TSem::Lock()
{
#ifdef TSEMUSED
  (fSembuf[0]).sem_num = 0;
  (fSembuf[0]).sem_op = 0;
  (fSembuf[0]).sem_flg = SEM_UNDO;

  (fSembuf[1]).sem_num = 0;
  (fSembuf[1]).sem_op = 1;
  (fSembuf[1]).sem_flg = SEM_UNDO | IPC_NOWAIT;
  
  if((semop( fSemid, fSembuf, 2) <0))
    {
      perror("TSem: Lock problem");
    }
#else
  ptlock_->lock();
#endif 
}
void TSem::Unlock()
{
#ifdef TSEMUSED
  union semun arg;
  (fSembuf[0]).sem_num = 0;
  (fSembuf[0]).sem_op = -1;
  //(fSembuf[0]).sem_flg = SEM_UNDO| IPC_NOWAIT; //LM suppress NOWAIT
  (fSembuf[0]).sem_flg = SEM_UNDO;

 
  if((semop( fSemid, fSembuf, 1) <0))
    {
      perror("TSem: Unlock problem");
      /*
     printf("Error code : %d \n",errno);
     semctl(fSemid,0,GETALL,arg);
     printf("Semaphore value %d \n",arg.array[0]);
     arg.array[0] =0;
     semctl(fSemid,0,SETALL,arg);
      */

    }
  usleep(1);
#else
  ptlock_->unlock();
#endif
}
