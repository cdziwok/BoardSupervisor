<!--HTML presentation of XDAQ XML configuration. Show only XdaqExecutive tags-->
<xsl:stylesheet version = '1.0' 
     xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>

<xsl:template match="/"> 
<html><body>
<H1>Tasks table</H1>
	<table border="1">	
	<tr><th>Hostname</th><th>Port</th><th>Config</th></tr>
		<xsl:apply-templates/>
	</table>
</body></html> 
</xsl:template>

<xsl:template match="XdaqApplication">
	<!--tr><td><xsl:value-of select="@className"/></td><td><xsl:value-of select="@hostname"/></td><td><xsl:value-of select="@port"/></td></tr-->
</xsl:template>

<xsl:template match="XdaqExecutive">
	<tr>
	<td>&lt;XdaqExecutive hostname="<xsl:value-of select="@hostname"/>"</td><td> port="<xsl:value-of select="@port"/>"&gt;</td>
	<td> <xsl:value-of select="configFile"/>&lt;/XdaqExecutive&gt;</td>
	</tr>
</xsl:template>

</xsl:stylesheet>

