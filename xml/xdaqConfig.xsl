<!--HTML presentation of XDAQ XML configuration file with XdaqApplication and XdaqExecutive tags-->
<xsl:stylesheet version = '1.0' 
     xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>

<xsl:template match="/"> 
<html><body>
<H1>Tasks table</H1>
	<table border="1">	
	<tr><th>Classname</th><th>Hostname</th><th>Port</th></tr>
		<xsl:apply-templates select="//XdaqApplication">
			<xsl:sort select="@hostname"/>
			<xsl:sort select="@className"/>
		</xsl:apply-templates>
	</table>
</body></html> 
</xsl:template>

<xsl:template match="XdaqApplication">
	<tr><td><xsl:value-of select="@className"/></td><td><xsl:value-of select="@hostname"/></td><td><xsl:value-of select="@port"/></td></tr>
</xsl:template>

<xsl:template match="XdaqExecutive">
	<tr><td align="center"><i>XdaqExecutive</i></td><td><xsl:value-of select="@hostname"/></td><td><xsl:value-of select="@port"/></td></tr>
</xsl:template>

</xsl:stylesheet>

