<!--Conversion of uHal XML address table to HTML form -->
<xsl:stylesheet version = '1.0' 
     xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
<xsl:output method="html"/>

<xsl:template match="/"> 
<h2>Parameters</h2>

<form method="POST" action="saveParameters" name="frm">
          <xsl:apply-templates select="/node/node"/>
	<input value="Save" type="submit"/>
</form>
</xsl:template>

<xsl:template match="node[node]" priority="1"><!--First level nodes-->
        <h3><xsl:value-of select="@id"/></h3>
	<div style="border:1px solid">
	<xsl:apply-templates />
	</div>
</xsl:template>

<!--Final leaves without bit table-->
<xsl:template match="node[@address and not(node) and not(@mode='block') ]" >
	<xsl:value-of select="@id"/>:<xsl:if test="@tags='hexa'"> 0x</xsl:if>
	<input type='text' size='8'>
		<xsl:attribute name="name"><xsl:apply-templates mode="uhal_path" select="."/></xsl:attribute>
		<xsl:attribute name="title"><xsl:value-of select="@description"/></xsl:attribute>
		<xsl:if test="not(contains(ancestor-or-self::*/@permission, 'w'))"><xsl:attribute name="readonly"/></xsl:if>
	</input>
	<br/>
</xsl:template>

<xsl:template match="node[node and not(node/@address) ]" priority="2"><!-- Bit table header (nodes with children that have no address)-->
        <h3><xsl:value-of select="@id"/></h3>
	<div style="border:1px solid;">
	<table border="0">	
		<xsl:apply-templates/>
	</table>
	</div>
</xsl:template>

<xsl:template match="node[not(@address) and ancestor::node]"><!-- Bit table row (node without address and with ancestor)-->
<tr><td><xsl:value-of select="@id"/></td>
<td><xsl:if test="@tags='hexa'"> 0x</xsl:if>
	<xsl:choose>
		<xsl:when test="@tags='select'">
			<select><xsl:attribute name="name"><xsl:apply-templates mode="uhal_path" select="."/></xsl:attribute></select>
                </xsl:when>
		<xsl:when test="@tags='checkbox'">
			<input size="8" type="checkbox">
				<xsl:attribute name="name"><xsl:apply-templates mode="uhal_path" select="."/></xsl:attribute>
				<xsl:attribute name="title"><xsl:value-of select="@description"/></xsl:attribute>
				<xsl:if test="not(contains(ancestor-or-self::*/@permission, 'w'))"><xsl:attribute name="readonly"/></xsl:if>
			</input>
                </xsl:when>
		<xsl:otherwise>
			<input size="8" type="text">
				<xsl:attribute name="name"><xsl:apply-templates mode="uhal_path" select="."/></xsl:attribute>
				<xsl:attribute name="title"><xsl:value-of select="@description"/></xsl:attribute>
				<xsl:if test="not(contains(ancestor-or-self::*/@permission, 'w'))"><xsl:attribute name="readonly"/></xsl:if>
			</input>
		</xsl:otherwise>			
	</xsl:choose>
</td>
</tr>
</xsl:template>

<xsl:template match="node" mode="uhal_path"><!-- Recursive template to retrieve the path of node 'id' attributes -->
	<xsl:if test="parent::*">
		<xsl:apply-templates mode="uhal_path" select="parent::*"/>
		<xsl:if test="parent::*/parent::*"><xsl:text>.</xsl:text></xsl:if>
		<xsl:value-of select="@id"/>
	</xsl:if>
</xsl:template>
</xsl:stylesheet>

