import FWCore.ParameterSet.python.Config as cms

# process declaration
process = cms.Process("HLT")


# message logger
print "load MessageLogger_cfi"
process.load("FWCore.MessageLogger.python.MessageLogger_cfi")
process.MessageLogger.cerr.FwkReport.reportEvery = 1000
process.MessageLogger.suppressWarning = cms.untracked.vstring('siStripClusters')

process.MessageLogger = cms.Service("MessageLogger",
  destinations = cms.untracked.vstring('cout','log4cplus'),
  cout = cms.untracked.PSet(threshold = cms.untracked.string('WARNING')),
  log4cplus = cms.untracked.PSet(INFO = cms.untracked.PSet(reportEvery = cms.untracked.int32(1000)),threshold = cms.untracked.string('WARNING'))
)

print "import PrescaleService_cfi"
from FWCore.PrescaleService.PrescaleService_cfi import *
process.PrescaleService = cms.Service("PrescaleService",
    prescaleTable = cms.VPSet(),
    lvl1Labels = cms.vstring('default'),
    lvl1DefaultLabel = cms.untracked.string('default')
)


process.source = cms.Source("DaqSource",
    readerPluginName = cms.untracked.string('FUShmReader'),
    evtsPerLS = cms.untracked.uint32(1000000)
)


## Load Event Setup Records
print "load StandardSetup_cfi"
process.load("XtalDAQ.Configuration.python.StandardSetup_cfi")
process.SiStripXtalNoisesGenerator.NoisesFile = cms.FileInPath('XtalDAQ/SiStripCommon/data/Pedestals.dat')
process.SiStripApvGainGenerator.file = cms.FileInPath("XtalDAQ/SiStripCommon/data/SiStripDetInfo.dat")


## Produce SiStrip Digis
print "load SiStripDigis_cfi"
process.load("EventFilter.SiStripRawToDigi.python.SiStripDigis_cfi")
process.siStripDigis.ProductLabel = cms.InputTag('source')
process.siStripDigis.UseDaqRegister = cms.bool(True)
process.siStripDigis.UnpackBadChannels = cms.bool(True)



#process.anal = cms.EDAnalyzer("EventContentAnalyzer")
#process.p1 = cms.Path(process.anal)

process.online = cms.Path( process.siStripDigis )

process.consumer = cms.OutputModule("ShmStreamConsumer",
    outputCommands = cms.untracked.vstring(
        'drop *', 
        'keep *_siStripDigis_*_*',
        'keep SiStripEventSummary_*_*_*'
    ),
    compression_level = cms.untracked.int32(1),
    use_compression = cms.untracked.bool(True)
)
process.outpath = cms.EndPath( process.consumer )
print "script end"
