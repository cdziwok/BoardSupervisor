import FWCore.ParameterSet.python.Config as cms 

# process declaration
process = cms.Process("HLT") 


# message logger
process.load("FWCore.MessageLogger.python.MessageLogger_cfi") 
process.MessageLogger.cerr.FwkReport.reportEvery = 1000 
process.MessageLogger = cms.Service("MessageLogger", 
  destinations = cms.untracked.vstring('cout','log4cplus'), 
  cout = cms.untracked.PSet(threshold = cms.untracked.string('WARNING')), 
  log4cplus = cms.untracked.PSet(INFO = cms.untracked.PSet(reportEvery = cms.untracked.int32(1000)),threshold = cms.untracked.string('WARNING')) 
)

 

from FWCore.PrescaleService.PrescaleService_cfi import *
process.PrescaleService = cms.Service("PrescaleService",
    prescaleTable = cms.VPSet(),
    lvl1Labels = cms.vstring('default'),
    #lvl1DefaultLabel = cms.untracked.string('default')
)

 
process.source = cms.Source("DaqSource",
    readerPluginName = cms.untracked.string('FUShmReader'),
    evtsPerLS = cms.untracked.uint32(1000000)
)

process.ana = cms.EDAnalyzer("EventContentAnalyzer")
process.p1 = cms.Path(process.ana)
 

process.consumer = cms.OutputModule("ShmStreamConsumer",
    compression_level = cms.untracked.int32(1),
    use_compression = cms.untracked.bool(True)
)

process.outpath = cms.EndPath( process.consumer )

