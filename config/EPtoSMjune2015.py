import FWCore.ParameterSet.Config as cms 

# process declaration
process = cms.Process("HLT") 


# message logger
process.load("FWCore.MessageLogger.MessageLogger_cfi") 
process.MessageLogger.cerr.FwkReport.reportEvery = 1000 
process.MessageLogger = cms.Service("MessageLogger", 
  destinations = cms.untracked.vstring('cout','log4cplus'), 
  cout = cms.untracked.PSet(threshold = cms.untracked.string('WARNING')), 
  log4cplus = cms.untracked.PSet(INFO = cms.untracked.PSet(reportEvery = cms.untracked.int32(1000)),threshold = cms.untracked.string('WARNING')) 
)

 

process.PrescaleService = cms.Service("PrescaleService",
    prescaleTable = cms.VPSet(),
    lvl1Labels = cms.vstring('default'),
    #lvl1DefaultLabel = cms.untracked.string('default')
)

 
process.source = cms.Source("DaqSource",
    readerPluginName = cms.untracked.string('FUShmReader'),
    evtsPerLS = cms.untracked.uint32(1000000)
)


#process.load('Phase2TrackerDAQ.SiStripRawToDigi.SiStripDigi_test_producer_cfi')
#process.load('Phase2TrackerDAQ.SiStripRawToDigi.SiStripDigi_CondData_producer_cfi')

process.load('Phase2TrackerAnalysis.OnlineAnalysis.TBeamTest_cfi')

process.load("DQMServices.Components.DQMFileSaver_cfi")
process.load("DQMServices.Components.DQMEventInfo_cfi")
process.dqmEnv.subSystemFolder    = "TkPhase2"
process.dqmSaver.convention = cms.untracked.string('Online')
process.dqmSaver.producer = cms.untracked.string('DQM')
process.dqmSaver.dirName = cms.untracked.string('/opt/cmssw/Data/online')
process.dqmSaver.saveByEvent = cms.untracked.int32(20000)

#process.ana = cms.EDAnalyzer("EventContentAnalyzer")

#process.p1 = cms.Path( process.ana )

process.p1 = cms.Path()
#process.SiStripDigitestproducer*
#			process.SiStripDigiCondDataproducer*
#			process.tbeamTest*process.dqmEnv*
#			process.dqmSaver)
 

process.consumer = cms.OutputModule("ShmStreamConsumer",
    compression_level = cms.untracked.int32(1),
    use_compression = cms.untracked.bool(True)
)

process.outpath = cms.EndPath( process.consumer )

