#FileName : 		Makefile
#Content :		Makefile for BoardSupervisor module
#Version : 		1.0
#Last modification date	10/10/2012
#Support : 		mail to : christian.bonnin@iphc.cnrs.fr

#BUILD_HOME environement variable must point to /...../DiagSystem

BUILD_HOME:=$(HOME)/
CACTUS_BUILD_ROOT = /opt/cactus/
ACF_BUILD_ROOT = $(HOME)/Ph2_ACF

#include DIAGSYSTEM configuration file
#include $(BUILD_HOME)/DiagHeader.linux

#include xdaq configuration files
include $(XDAQ_ROOT)/config/mfAutoconf.rules
include $(XDAQ_ROOT)/config/mfDefs.linux

#define project path - relative to DiagSystem root
Project = CBCDAQ

#define package path - relative to DiagSystem/project root
Package = BoardSupervisor

#Define package source files
Sources= FedStateMachine.cc BoardSupervisor.cc BoardSupervisorV.cc \
		Acquisition.cc I2cCommunication.cc FedEvent.cc ParameterSet.cc Board.cc FedEvent2020.cc CommissioningLoop.cc TSem.cc Streamer.cc

IncludeDirs = \
	$(XDAQ_ROOT)/include \
	$(XDAQ_ROOT)/include/linux \
	$(ENV_CMS_TK_DIAG_ROOT)/tools/xmldiagappender/include \
    	$(ENV_CMS_TK_DIAG_ROOT)/tools/diagbag/include \
	$(ENV_CMS_TK_DIAG_ROOT)/TrackerOnline/2005/TrackerXdaq/include \
	$(ENV_CMS_TK_DIAG_ROOT) \
	${FECSOFTWARE_ROOT}/FecPciDeviceDriver/include \
	${FECSOFTWARE_ROOT}/generic/include \
	${ORACLE_INCLUDE} \
	${FED_INCLUDE} \
	${TRACKERDAQ_INCLUDE} \
	${CACTUS_BUILD_ROOT}/include \
	${ACF_BUILD_ROOT} \
	${ENV_CMS_TK_XDAQ}/include	#added by LG 31/10/2013 to include FRLA.h


LibraryDirs = \
	$(XDAQ_ROOT)/x86/lib \
	$(XDAQ_ROOT)/lib \
        $(ENV_CMS_TK_DIAG_ROOT)/tools/soapdiagappender/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM) \
	$(ENV_CMS_TK_DIAG_ROOT)/tools/xmldiagappender/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM) \
        $(ENV_CMS_TK_DIAG_ROOT)/tools/diagbag/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM) \
	${FECSOFTWARE_ROOT}/generic/lib/${XDAQ_OS}/${XDAQ_PLATFORM} \
	${ORACLE_LDDFLAGS} \
	${FED_LIBDIR} \
	${CACTUS_BUILD_ROOT}/lib \
	${ACF_BUILD_ROOT}/lib

UserCFlags =
#UserCCFlags = -g 

UserCCFlags = -g -fPIC -std=c++11 -Wcpp -Wno-unused-local-typedefs -O0

UserDynamicLinkFlags =
UserStaticLinkFlags =
UserExecutableLinkFlags =



#ExternalObjects = $(LibraryPaths) -lxmldiagappender -lTkDiagBagWizard -lsoapdiagappender
ExternalObjects = $(LibraryPaths) -lpthread -lboost_thread -lboost_filesystem -lboost_regex -lboost_system -lboost_thread -lcactus_extern_pugixml -lcactus_uhal_log -lcactus_uhal_grammars -lcactus_uhal_uhal -lxdaq2rc -lPh2_Description -lPh2_Interface -lPh2_Utils -lPh2_System -lPh2_Tracker


DynamicLibrary=BoardSupervisor
StaticLibrary=
Executables=

include $(XDAQ_ROOT)/config/Makefile.rules
